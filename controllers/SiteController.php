<?php

namespace app\controllers;

use app\job\MailSendJob;
use app\models\About;
use app\models\AboutImages;
use app\models\MainSub;
use app\models\Banner;
use app\models\BannerAdvantages;
use app\models\Partners;
use app\models\Products;
use app\models\Telephone;
use app\models\Contact;
use app\models\Subscribe;
use app\models\Feedback;
use app\models\Menu;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class SiteController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@app/web/fonts/Century Gothic.ttf',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Menu::find()->where('url = "/"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $mainSub = MainSub::find()->all();
        $banner = Banner::find()->all();
        $bannerAdvantages = BannerAdvantages::find()->all();
        $about = About::find()->one();
        $aboutImages = AboutImages::find()->all();
        $partners = Partners::find()->all();
        $phone = Telephone::find()->all();
        $contact = Contact::find()->one();
        $hitProduct = Products::find()->where('status = 1 AND (isHit != 0 OR isHit != NULL)')->all();

        return $this->render('index', compact('model', 'banner', 'bannerAdvantages', 'mainSub', 'partners',
                            'phone', 'contact', 'hitProduct', 'about', 'aboutImages'));
    }


    public function actionContact()
    {
        $model = Menu::find()->where('url = "site/contact"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $contact = Contact::find()->one();
        $phone = Telephone::find()->all();

        return $this->render('contact', compact('model','phone', 'contact'));
    }


    public function actionComment()
    {
        $model = new Feedback();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                if ($model->save()) {
                    return 1;
                }
            } else {

                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }

    public function actionSubscribe()
    {
        $model = new Subscribe();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    $model->sendEmail();
                    return 1;
                }
            } else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }


    public function actionTest()
    {
        $email = 'kkokoneor@gmail.com';
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => 'AL2KA'])
            ->setTo($email)
            ->setSubject('Пользователь подписался на рассылку')
            ->setHtmlBody("<p>Email: assadsadasdasdasd</p>
                             ");

        return $emailSend->send();

//        var_dump(Yii::$app->formatter->asDate(1578908209, 'Y-M-d')); die;

//        Yii::$app->queue->delay(80)->push(new MailSendJob([
//            'to'    => 'qweqwrqrewew',
//            'subject' => 'dsawqdfzxvc',
//            'body' => "gqwdasasasdasdqwewq",
//        ]));
    }

}
