<?php

namespace app\controllers;

use app\models\Menu;
use app\models\ProductOwn;
use app\models\Products;
use app\models\Orders;
use app\models\User;
use app\models\UserAddress;
use app\models\UserFavorites;
use app\models\UserProfile;
use app\models\LoginForm;
use app\models\LoginUser;
use app\models\PasswordUpdate;
use app\models\SignupForm;
use app\models\ForgotYourPassword;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;


class AccountController extends FrontendController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['profile'],
                'rules' => [

                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        if(!Yii::$app->user->isGuest){
            $model = Menu::findOne(['url'=>'/account']);
            $metaName   = "metaName".Yii::$app->session["lang"];
            $metaKey    = "metaKey".Yii::$app->session["lang"];
            $metaDesc   = "metaDesc".Yii::$app->session["lang"];
            $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

            $user = User::findOne(Yii::$app->user->id);
            $profile = UserProfile::findOne(['user_id'=>Yii::$app->user->id]);
            $address = UserAddress::findAll(['user_id'=>Yii::$app->user->id]);
            $userAddress = UserAddress::findOne(['user_id' => Yii::$app->user->id]);
            $orders = Orders::getAll(5);

            $favorites = UserFavorites::findAll(['user_id'=>Yii::$app->user->id]);
            if($favorites != null){
                $arr = '(';
                foreach ($favorites as $v){
                    $arr.=$v->product_id.',';
                }
                $arr = substr($arr,0,strlen($arr)-1);
                $arr.= ')';
                $fav = Products::find()->where('id in '.$arr)->all();
            }

            $ownProduct = ProductOwn::findAll(['user_id' => Yii::$app->user->identity->id]);


            return $this->render('profile', compact('model','user','profile','address', 'menu', 'orders', 'fav', 'userAddress', 'ownProduct'));
        }else{
            return $this->render('login');
        }

    }

    public function actionSignIn(){

        return $this->render('login');
    }


    public function actionSignUp(){
        $model = new SignupForm();
        return $this->render('register',compact('model'));
    }

    public function actionLogin()
    {
        $model = new LoginUser();

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->login()) {
                return 1;
            }else{
                $error = "";
                $errors = $model->getErrors();

                foreach($errors as $v){
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }


    public function actionRegister()
    {
        $model = new SignupForm();
        $profile = new UserProfile();

        if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $profile->validate()) {
                $password = $model->password;
                if ($user = $model->signUp()) {
                    $profile->user_id = $user->id;
                    if($profile->save()){
                        $this->sendInformationAboutRegistration($profile->fio, $user->email, $user->username, $password);
                        Yii::$app->getUser()->login($user);
                        return 1;
                    }
                }else{
                    $user_error = "";
                    $user_errors = $model->getErrors();
                    foreach($user_errors as $v){
                        $user_error .= $v[0];
                        break;
                    }
                    return $user_error;
                }
        }else{
            $profile_error = "";
            $profile_errors = $profile->getErrors();
            foreach($profile_errors as $v){
                $profile_error .= $v[0];
                break;
            }

            return $profile_error;

        }
    }



    public function actionUpdateAccountData()
    {
        $user = User::findOne(Yii::$app->user->id);
        $user->scenario = User::update_all_data;
        $profile = UserProfile::findOne(['user_id'=>Yii::$app->user->id]);
        $address = new UserAddress();

        if ($user->load(Yii::$app->request->post()) && $address->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $profile->validate()) {

            if($user->password != null){
                $user->scenario = User::update_all_data;
            }else{
                $user->scenario = User::update_data;
            }

            if($address['address'][0] != null){
                if($user->validate()){
                    if($user->password != null) {
                        $user->setPassword($user->password);
                    }
                    if($profile->save() && $user->save()) {
                        UserAddress::deleteAll(['user_id'=>Yii::$app->user->id]);
                        foreach ($address['address'] as $v){
                            $newAddress = new UserAddress();
                            $newAddress->user_id = Yii::$app->user->id;
                            $newAddress->address = $v;
                            $newAddress->save();
                        }
                        return 1;
                    }
                }else{
                    $user_error = "";
                    $user_errors = $user->getErrors();
                    foreach($user_errors as $v){
                        $user_error .= $v[0];
                        break;
                    }
                    return $user_error;
                }


            }else{
                return 'Необходимо заполнить «Адрес».';
            }

        }else{
            $profile_error = "";
            $profile_errors = $profile->getErrors();
            foreach($profile_errors as $v){
                $profile_error .= $v[0];
                break;
            }
            return $profile_error;
        }
    }

    public function actionUpdatePassword()
    {
        $user = new PasswordUpdate();

        if($user->load(Yii::$app->request->get()) && $user->validate()){
            if($user->signUp()) {
                return 1;
            }
        }else{
            $error = "";
            $errors = $user->getErrors();
            foreach ($errors as $v) {
                $error .= $v[0];
                break;
            }
            return $error;
        }

    }



    public function actionForgotPassword()
    {
        $user = new ForgotYourPassword();
        if ($user->load(Yii::$app->request->get()) && $user->validate()) {

            $check = User::findOne(['email'=>$user->email]);
            if($check) {
                $password = $check->getRandomPassword();
                $check->setPassword($password);
                if($check->save(false)){
                    if($this->sendInformationAboutNewPassword($user->email, $password)){
                        return 1;
                    }
                }
            }else{
                return 'Пользователь с таким e-mail отсутствует в базе!';
            }
        }else{
            $user_error = "";
            $user_errors = $user->getErrors();
            foreach($user_errors as $v){
                $user_error .= $v[0];
                break;
            }
            return $user_error;
        }
    }


    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }



    public function actionAddToFavorite()
    {
        $user_id = $_GET['user_id'];
        $product_id = $_GET['product_id'];
        $favorite = UserFavorites::find()->where('user_id='.$user_id.' AND product_id='.$product_id)->one();

        $text = "";
        if($favorite == null){
            $fav = new UserFavorites();
            $fav->user_id = $user_id;
            $fav->product_id = $product_id;
            if($fav->save()){
                $text = 'Товар добавлен в избранное!';
            }

        }else{
            $favorite->delete();
            $text = 'Товар удален с избранного!';
        }


        $array = ['text' => $text];
        return json_encode($array);

    }

    public function actionDeleteFromFavorite()
    {
        $favorite = UserFavorites::find()->where('user_id='.Yii::$app->user->id.' AND product_id='.$_GET['id'])->one();

        if($favorite->delete()){
            $favorites = UserFavorites::findAll(['user_id'=>Yii::$app->user->id]);
            if($favorites != null){
                $arr = '(';
                foreach ($favorites as $v){
                    $arr.=$v->product_id.',';
                }
                $arr = substr($arr,0,strlen($arr)-1);
                $arr.= ')';
                $fav = Product::find()->where('id in '.$arr)->all();
            }
            return $this->renderAjax('favorite', compact('fav','error'));
        }else{
            return 0;
        }
    }

    

}
