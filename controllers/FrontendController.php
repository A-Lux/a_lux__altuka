<?php

namespace app\controllers;

use app\models\Logo;
use app\models\Offer;
use app\models\Orders;
use app\models\Telephone;
use app\models\Catalog;
use app\models\Contact;
use app\models\Emailforrequest;
use app\models\Feedback;
use app\models\Menu;
use app\models\Translation;
use app\models\User;
use app\models\Products;
use app\models\SignupForm;
use app\models\UserProfile;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{

    public function init()
    {
        $main               = Menu::find()->where('url = "/"')->one();
        $text               = "text" . Yii::$app->session["lang"];
        $menu               = Menu::find()->where(['status' => 1])->orderBy('sort asc')->limit(4)->all();
        $menuAll            = Menu::find()->all();
        $phone              = Telephone::find()->all();
        $logo               = Logo::find()->all();
        $contact            = Contact::find()->one();
        $catalog            = Catalog::find()->where('status = 1 AND level=1')->all();
        $catalogLevel2      = Catalog::find()->where('status = 1 AND level=2')->all();
        $translation        = Translation::find()->all();
        $offer              = Offer::findOne(['id' => 1]);

        $yourMagnet         = Catalog::findOne(['name' => 'Индивидуальный заказ']);

        Yii::$app->view->params['yourMagnet']       = $yourMagnet->name;

        Yii::$app->view->params['main']             = $main->$text;
        Yii::$app->view->params['menu']             = $menu;
        Yii::$app->view->params['menuAll']          = $menuAll;
        Yii::$app->view->params['phone']            = $phone;
        Yii::$app->view->params['logo']             = $logo;
        Yii::$app->view->params['contact']          = $contact;
        Yii::$app->view->params['catalog']          = $catalog;
        Yii::$app->view->params['catalogLevel2']    = $catalogLevel2;
        Yii::$app->view->params['translation']      = $translation;
        Yii::$app->view->params['offer']            = $offer;

        if (!isset($_SESSION['basket'])) {
            session_start();
            $_SESSION['basket'] = array();
        }

        if (!isset($_SESSION['basket-product-own'])) {
            session_start();
            $_SESSION['basket-product-own'] = array();
        }

        if (Yii::$app->session['lang'] == '')
            Yii::$app->language = 'ru';
        elseif (Yii::$app->session['lang'] == '_kz')
            Yii::$app->language = 'kz';
        elseif (Yii::$app->session['lang'] == '_en')
            Yii::$app->language = 'en';

    }

    protected function setMeta($metaName = null, $metaKey = null, $metaDesc = null)
    {
        $this->view->title = $metaName;
        $this->view->registerMetaTag(['name' => 'metaKey', 'content' => "$metaKey"]);
        $this->view->registerMetaTag(['name' => 'metaDesc', 'content' => "$metaDesc"]);
    }

    public function Catalog_products($id)
    {
        $catalogs = Catalog::find()->where('status = 1')->all();
        $result = $this->get_cat($catalogs);

        $result = $this->get_level3($result, $id);


        //echo '<pre>'.var_dump($result, true).'</pre>';die;
        return $result;
    }

    protected function drawCatalogItems($menuItems, $parent_id = null)
    {
        $html = "";
        foreach ($menuItems as $item) {
            if ($item->parent_id !== $parent_id) {
                continue;
            }

            $parentItems = mb_substr($this->parentItems($menuItems, $item), 0, -1);
            $parentItems = array_reverse(explode('-', $parentItems));
            $parentItems = implode('-', $parentItems);

            $html .= '<li><img src="/public/images/no-active.png">';
            $html .= '<a class="' . $this->classLevel($item->level) . ' category_filter" href="#' . $parentItems . '" data-id="' . $item->id . '">';
            $html .= $item->name . '</a>';
            if (!empty($item->childs)) {
                $html .= '<ul id="' . $parentItems . '" style="display: none"">' . $this->drawCatalogItems($menuItems, $item->id) . '</ul>';
            }
        }

        return $html;
    }

    public function parentItems($menuItems, $item)
    {
        $tmp = $item->id . '-';

        if (!empty($menuItems[$item->parent_id])) {
            $tmp .= $this->parentItems($menuItems, $menuItems[$item->parent_id]);
        }

        return $tmp;
    }

    protected function getCatalogItems($categories)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            $menuItems[$category->id] = $category;
        }

        foreach ($categories as $category) {
            if (isset($menuItems[$category->parent_id])) {
                $menuItems[$category->parent_id]->childs[] = $category;
            }
        }

        return $menuItems;
    }

    public function classLevel($level)
    {
        if ($level == 1) {
            $class = 'firts-tabs';
        } elseif ($level == 2) {
            $class = 'second-tabs';
        } else {
            $class = 'third-city';
        }

        return $class;
    }

    public function get_cat($catalogs)
    {
        $arr_cat = array();
        if (count($catalogs)) {
            //В цикле формируем массив
            foreach ($catalogs as $v) {
                //Формируем массив, где ключами являются адишники на родительские категории
                if (empty($arr_cat[$v['parent_id']])) {
                    $arr_cat[$v['parent_id']] = [];
                }
                $arr_cat[$v['parent_id']][] = $v;
            }
            //возвращаем массив
            return $arr_cat;
        }
    }

    public function get_level3($result, $id)
    {
        $arr = [];
        foreach ($result as $val1) {
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    if (!empty($result[$val2->id]))
                        foreach ($result[$val2->id] as $val3) {
                            if (!empty($result[$val3->id]))
                                foreach ($result[$val3->id] as $val4) {
                                    if (!empty($result[$val4->id]))
                                        foreach ($result[$val4->id] as $val5) {
                                            if (!empty($result[$val5->id]))
                                                foreach ($result[$val5->id] as $val6) {
                                                    $arr[] = $val6->id;
                                                }
                                            $arr[] = $val5->id;
                                        }
                                    $arr[] = $val4->id;
                                }
                            $arr[] = $val3->id;
                        }
                    $arr[] = $val2->id;
                }
            }
        }

        if ($arr)
            $arr = Products::find()->where('category_id in (' . implode(',', $arr) . ')')->all();

        return $arr;
    }

    public function get_level_filter($result, $id, $sql, $order, $price)
    {
        $arr = [];
        foreach ($result as $val1) {
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    if (!empty($result[$val2->id]))
                        foreach ($result[$val2->id] as $val3) {
                            if (!empty($result[$val3->id]))
                                foreach ($result[$val3->id] as $val4) {
                                    if (!empty($result[$val4->id]))
                                        foreach ($result[$val4->id] as $val5) {
                                            if (!empty($result[$val5->id]))
                                                foreach ($result[$val5->id] as $val6) {
                                                    $arr[] = $val6->id;
                                                }
                                            $arr[] = $val5->id;
                                        }
                                    $arr[] = $val4->id;
                                }
                            $arr[] = $val3->id;
                        }
                    $arr[] = $val2->id;
                }
            }
        }

        if ($arr)
            $arr = Products::find()->where('category_id in (' . implode(',', $arr) . ') ' . $sql . $price)->orderBy($order)->all();

        return $arr;
    }


    protected function sendNewPassword($email, $code)
    {
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom('sdulife.kz@gmail.com')
            ->setTo($email)
//            ->setSubject('Update password')

            ->setHtmlBody("<p> Код для изменение пароля : $code</p>");
        return $emailSend->send();

    }

    protected function sendRequest($name, $email, $content)
    {


        $emailSend = Yii::$app->mailer->compose()
            ->setFrom('sdulife.kz@gmail.com')
            ->setTo(Yii::$app->view->params['email']->email)
            ->setSubject('Клиент хочет связаться с вами')
            ->setHtmlBody("<p>Имя: $name</p>
                                 <p>Номер телефона: $email</p>
                                 <p>Сообщение: $content</p>");
        return $emailSend->send();

    }


    protected function sendInformationAboutRegistration($fio, $email, $login, $password)
    {
        $host = Yii::$app->request->hostInfo;

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => '«AL2KA»'])
            ->setTo($email)
            ->setSubject('Регистрация на сайте компании «AL2KA»')
            ->setHtmlBody("<p>$fio, вы получили данное письмо, т.к. зарегистрировались на сайте компании «AL2KA».</p> </br>
                                 </br>
                                 <p>Ваши данные для авторизации:</p> </br></br>
                                 <p>E-mail: $email</p>
                                 <p>Логин: $login</p>
                                 <p>Пароль: $password</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«AL2KA»</a></p>");

        return $emailSend->send();

    }


    protected function sendInformationAboutNewPassword($email, $password)
    {
        $host = Yii::$app->request->hostInfo;

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => 'ТОО «AL2KA»'])
            ->setTo($email)
            ->setSubject('Восстановление доступа')
            ->setHtmlBody("<p>Вы получили данное письмо, т.к. на ваш e-mail был полуен запрос на восстановление пароля.</p> </br>
                                 </br>
                                 <p>Ваши данные для авторизации:</p> </br></br>
                                 <p>E-mail: $email</p>
                                 <p>Ваш новый пароль: $password</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«AL2KA»</a></p>");

        return $emailSend->send();

    }

    protected function sendInformationAboutPayment($fio, $email, $id)
    {
        $host = Yii::$app->request->hostInfo;
        $orderedProduct = "<table>
			<colgroup><col width=\"72\">
			<col>
			<col width=\"100\">
			<col width=\"75\">
			<col width=\"125\"></colgroup>
			<tbody>
			    <tr>
                    <th></th>
                    <th>Наименование</th>
                    <th>Код товара</th>
                    <th>Размер</th>
                    <th>Цена за ед.</th>
                    <th>Кол-во</th>
                    <th>Сумма</th>
                </tr>";

        $orderedProductOwn = "<table>
			<colgroup><col width=\"72\">
			<col>
			<col width=\"100\">
			<col width=\"75\">
			<col width=\"125\"></colgroup>
			<tbody>
			    <tr>
                    <th></th>
                    <th>Наименование</th>
                    <th>Код товара</th>
                    <th>Размер</th>
                    <th>Цена за ед.</th>
                    <th>Кол-во</th>
                    <th>Сумма</th>
                </tr>";

        $allPrice = 0;
        foreach ($_SESSION['basket'] as $v) {
            $price = $v->price;

            $productPrice = $v->getSumProduct($v->id);
            $image = $v->getImage();
            $orderedProduct .= "<tr>
                    <td>
                        <img src=\"$host$image\" style=\"float: left;margin-right: 50px;\" alt=\"cart-img\" class=\"CToWUd\" width=\"100\">
                    </td>
                    <td><b> $v->name</b><br>
                    </td>
                    <td align=\"center\">$v->articul</td>
                    <td align=\"center\">$v->size</td>
                    <td align=\"center\">$price тг</td>
                    <td align=\"center\">$v->count</td>
                    <td align=\"center\">$productPrice тг</td>
                </tr></tbody></table>";

            $allPrice += $productPrice;
        }

        foreach ($_SESSION['basket-product-own'] as $v) {
            $price  = $v->product->price;
            $name   = $v->product->name;
            $size   = $v->product->size;
            $articul    = $v->product->articul;

            $productPrice = $v->product->getSumProductOwn($v->id);
            $image = $v->getImage();
            $orderedProductOwn .= "<tr>
                    <td>
                        <img src=\"$host$image\" style=\"float: left;margin-right: 50px;\" alt=\"cart-img\" class=\"CToWUd\" width=\"100\">
                    </td>
                    <td><b> $name</b><br>
                    </td>
                    <td align=\"center\">$articul</td>
                    <td align=\"center\">$size</td>
                    <td align=\"center\">$price тг</td>
                    <td align=\"center\">$v->count</td>
                    <td align=\"center\">$productPrice тг</td>
                </tr></tbody></table>";

            $allPrice += $productPrice;
        }

        $messagePrice = "<table><tbody><tr><td colspan=\"2\"></td>
				<td colspan=\"2\">
					Итого товара на сумму:
				</td>
				<td align=\"center\">
					$allPrice тг
				</td>
			</tr>
			</tbody></table>
			<br><br>
            <p>---</p></br>";

        $messageOwn = $_SESSION['basket-product-own'] == null ? "<br>" :
            "<p>Индивидуальный заказ:</p><br>
                 $orderedProductOwn
                 <br><br>
             <p>---</p></br>";

        $message    = $_SESSION['basket'] == null ? "<br>" :
            "<p>Обычный заказ:</p><br>
                 $orderedProduct
                 </br></br>
             <p>---</p></br>";

        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => 'ТОО «AL2KA»'])
            ->setTo($email)
            ->setSubject('Поступил новый заказ №' . $id . ' от ' . date('d-m-Y h:i', time()))
            ->setHtmlBody("<p>$fio, благодарим Вас за оформление заказа на нашем сайте «AL2KA»</p></br>
                                 <p>Ниже, предоставляем Вам информацию о заказах:</p></br></br>
                                 $messageOwn
                                 $message
                                 $messagePrice
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«AL2KA»</a></p>");

        return $emailSend->send();

    }

    protected function sendOrderOperator($order_id, $order_comment, $fio, $phone, $mail, $address, $status)
    {
        $host = Yii::$app->request->hostInfo;
        $email = Emailforrequest::find()->all();

        $orderedProduct = "<table>
			<colgroup><col width=\"72\">
			<col>
			<col width=\"100\">
			<col width=\"75\">
			<col width=\"125\"></colgroup>
			<tbody>
			    <tr>
                    <th></th>
                    <th>Наименование</th>
                    <th>Код товара</th>
                    <th>Размер</th>
                    <th>Цена за ед.</th>
                    <th>Кол-во</th>
                    <th>Сумма</th>
                </tr>";

        $orderedProductOwn = "<table>
			<colgroup><col width=\"72\">
			<col>
			<col width=\"100\">
			<col width=\"75\">
			<col width=\"125\"></colgroup>
			<tbody>
			    <tr>
                    <th></th>
                    <th>Наименование</th>
                    <th>Код товара</th>
                    <th>Размер</th>
                    <th>Цена за ед.</th>
                    <th>Кол-во</th>
                    <th>Сумма</th>
                </tr>";

        $allPrice = 0;
        foreach ($_SESSION['basket'] as $v) {
            $price = $v->price;

            $productPrice = $v->count * $price;
            $orderedProduct .= "<tr>
                    <td>
                    </td>
                    <td><b> $v->name</b><br>
                    </td>
                    <td align=\"center\">$v->articul</td>
                    <td align=\"center\">$v->size</td>
                    <td align=\"center\">$price тг</td>
                    <td align=\"center\">$v->count</td>
                    <td align=\"center\">$productPrice тг</td>
                </tr></tbody></table>";

            $allPrice += $productPrice;
        }
        foreach ($_SESSION['basket-product-own'] as $v) {
            $price = $v->product->price;
            $name   = $v->product->name;
            $size   = $v->product->size;
            $articul    = $v->product->articul;

            $productPrice = $v->product->getSumProductOwn($v->id);
            $image = $v->getImage();
            $orderedProductOwn .= "<tr>
                    <td>
                        <img src=\"$host$image\" style=\"float: left;margin-right: 50px;\" alt=\"cart-img\" class=\"CToWUd\" width=\"100\">
                    </td>
                    <td><b> $name</b><br>
                    </td>
                    <td align=\"center\">$articul</td>
                    <td align=\"center\">$size</td>
                    <td align=\"center\">$price тг</td>
                    <td align=\"center\">$v->count</td>
                    <td align=\"center\">$productPrice тг</td>
                </tr></tbody></table>";

            $allPrice += $productPrice;
        }

        $messagePrice = "<table><tbody><tr><td colspan=\"2\"></td>
				<td colspan=\"2\">
					Итого товара на сумму:
				</td>
				<td align=\"center\">
					$allPrice тг
				</td>
			</tr>
			</tbody></table>
			";

        $orderStatus = "<br>";
        if($status == 2){
            $status = Orders::getStatusPay($status);
            $orderStatus = "<p>Статус заказа: ". $status . "</p>";
        }

        $messageOwn = $_SESSION['basket-product-own'] == null ? "<br>" :
            "<p>Индивидуальный заказ:</p><br>
                 $orderedProductOwn
             <br><br>
             <p>---</p></br>";

        $message    = $_SESSION['basket'] == null ? "<br>" :
            "<p>Обычный заказ:</p><br>
                $orderedProduct
             </br></br>";

        foreach ($email as $item) {
            $emailSend = Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['adminEmail'] => '"AL2KA"'])
                ->setTo($item->email)
                ->setSubject('Поступил новый заказ №' . $order_id)
                ->setHtmlBody("<p>ФИО клиента: $fio</p></br>
						 <p>Телефон клиента: $phone</p></br>
						 <p>Email клиента: $mail</p></br>
						 <p>Адрес клиента: $address</p></br>
						 <p>Комментарий к заказу: $order_comment</p></br>
						    $orderStatus
						 <p>Заказы:</p></br>
						 $messageOwn
                         $message
                         $messagePrice");

                foreach($_SESSION['basket-product-own'] as $v){
                    $emailSend->attach($v->getImagePathName());
                }

            $emailSend->send();
        }
        return 1;
    }

}
