<?php

namespace app\controllers;

use app\models\ProductOwn;
use app\models\Products;

class ProductOwnController extends FrontendController
{
    public function actionAddProductOwn()
    {
        $id = $_GET['id'];

        $card = [];
        foreach ($_SESSION['basket-product-own'] as $value){
            $card[] = $value->id;
        }

        if(!in_array($id, $card)){
            $basket = ProductOwn::findOne(['id' => $id]);
            $basket->count = 1;

            $check = true;
            foreach ($_SESSION['basket-product-own'] as $v) {
                if ($v->id == $id) {
                    $check = false;
                    break;
                }
            }
            if ($check) {
                array_push($_SESSION['basket-product-own'], $basket);
            }
            $count = count($_SESSION['basket-product-own']);
            $array = ['status' => 1, 'count' => $count];
        }else {
            $array = ['status' => 2];
        }

        return json_encode($array);
    }

    public function actionDeleteProductOwn($id)
    {
        $m=0;
        foreach ($_SESSION['basket-product-own'] as $v){
            if($v->id == $id){
                unset($_SESSION['basket-product-own'][$m]);
                $_SESSION['basket-product-own'] = array_values($_SESSION['basket-product-own']);
                break;
            }
            $m++;
        }
        return $this->redirect('/card/index');
    }

    public function actionDownClickedOwn()
    {
        $count = 1;
        foreach ($_SESSION['basket-product-own'] as $k=>$v){
            if($v->id == $_GET['id'] && $_SESSION['basket-product-own'][$k]->count > 1){
                $_SESSION['basket-product-own'][$k]['count']-=1;
                $count = $_SESSION['basket-product-own'][$k]['count'];
                break;
            }
        }

        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        $sumProduct = Products::getSumProduct($_GET['id']);
        $array = ['countProduct' => $count, 'sum' => $sum,'sumProduct'=>$sumProduct];
        return json_encode($array);
    }


    public function actionUpClickedOwn()
    {
        $count = 1;
        foreach ($_SESSION['basket-product-own'] as $k=>$v){
            if($v->id == $_GET['id']){
                $_SESSION['basket-product-own'][$k]['count']+=1;
                $count = $_SESSION['basket-product-own'][$k]['count'];
                break;
            }
        }

        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        $sumProduct = Products::getSumProductOwn($_GET['id']);
        $array = ['countProduct' => $count, 'sum' => $sum,'sumProduct'=>$sumProduct];
        return json_encode($array);
    }

    public function actionCountChangedOwn()
    {
        foreach ($_SESSION['basket-product-own'] as $k=>$v){
            if($v->id == $_GET['id']){
                if($_GET['v']>0){
                    $_SESSION['basket-product-own'][$k]['count'] = $_GET['v'];
                }else{
                    $_SESSION['basket-product-own'][$k]['count']=1;
                }
            }
        }

        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        $sumProduct = Products::getSumProductOwn($_GET['id']);
        $array = ['sum' => $sum,'sumProduct'=>$sumProduct];
        return json_encode($array);
    }

    public function actionDeleteOwn($id)
    {
        $model = ProductOwn::findOne(['id' => $id]);

        if($model->delete()){
            \Yii::$app->session->setFlash('successDelete', \Yii::t('main', 'Вы успешно удалили данный продукт!'));
            return $this->redirect('/account#v-pills-ownCabinet');
        }else{
            \Yii::$app->session->setFlash('errorDelete', \Yii::t('main', 'Неожиданная ошибка сервера. Попробуйте позднее!'));
            return $this->redirect('/account#v-pills-ownCabinet');
        }
    }

//    public function actionTest()
//    {
//        $card = [];
//        foreach ($_SESSION['basket-product-own'] as $value){
//            $card[$value->id] = $value->product->name;
//        }
//
//        $price = [];
//
//        foreach ($_SESSION['basket-product-own'] as $v) {
//            $price[] = $v->product->getSumProductOwn($v->id);
//        }
//
//        print_r("<pre>");
//        print_r($price);die;
//    }
}