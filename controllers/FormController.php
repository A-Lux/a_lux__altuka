<?php

namespace app\controllers;

use app\models\ProductOwn;
use yii\web\UploadedFile;

class FormController extends FrontendController
{
    public function actionUpload()
    {
        $model  = new ProductOwn();

        $model->user_id = \Yii::$app->user->identity->id;
        $model->date    = \Yii::$app->formatter->asDate(time(), "Y-MM-dd H:i:s");
        $extension  = ['png', 'jpg', 'jpeg', 'bmp'];

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $model->image = UploadedFile::getInstance($model, 'image');

            if(!in_array($model->image->extension, $extension)){
                \Yii::$app->session->setFlash('errorUpload', \Yii::t('main', 'Изображение должно соответсвовать формату "png, jpeg, jpg"'));
                return $this->redirect(['/catalog/products/' . $model->product->url]);
            }

            if($model->image != null) {
                $time = time();
                $model->image->saveAs($model->path . $time . '_' . $model->image->baseName . '.' . $model->image->extension);
                $model->image = $time . '_' . $model->image->baseName . '.' . $model->image->extension;
            }

            if($model->save()){
                \Yii::$app->session->setFlash('successUpload', \Yii::t('main', 'Вы успешно создали свой магнитик! Можете его добавить в корзину в личном кабинете и заказать!'));

                return $this->redirect('/account#v-pills-ownCabinet');
            }
        }else{
            \Yii::$app->session->setFlash('errorUpload', \Yii::t('main', 'Ошибка сервера! Попробуйте позднее загрузить данные!'));

            return $this->goBack();
        }
    }
}