<?php
namespace app\controllers;

use app\models\Currency;
use app\models\Menu;
use app\models\Catalog;
use app\models\MainSub;
use app\models\Products;
use app\models\ProductsImage;
use app\models\Recommend;
use app\models\Subjects;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class CatalogController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    private $countProductPerPage = 5;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = Menu::find()->where('url = "/catalog"')->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $mainSub = MainSub::find()->all();
        $subjects = Subjects::find()->all();

        $catalog = Catalog::find()->where('status=1')->all();
        if(!$catalog)
            throw new NotFoundHttpException();

        $result = $this->getCatalogItems($catalog);


        $tree = $this->drawCatalogItems($result);

        return $this->render('index', compact('catalog', 'model', 'mainSub', 'tree',
            'subjects'));
    }

    public function actionCategory()
    {
        $id = $_GET['id'];
        $category_id = $_GET['category_id'];
        $catalogs = Catalog::find()->where('status = 1')->all();
        $result = $this->get_cat($catalogs);
        $products = $this->get_level3($result, $id);


        $catalog = Catalog::findOne(['id' => $id]);
        if($catalog->name == "Индивидуальный заказ"){
            $message    = Yii::t('main', 'Пожалуйста, выберите размер магнитика');
        }else{
            $message    = '';
        }

        // echo '<pre>';
        // print_r($products);
        // echo '</pre>'; die;

        return $this->renderAjax('category',compact('products', 'category_id', 'message'));
    }

    public function actionProduct($url)
    {
        $product = Products::findOne(['url' => $url]);
        $this->setMeta($product->metaName, $product->metaKey, $product->metaDesc);
        $mainSub = MainSub::find()->all();

        $image = ProductsImage::find()->where(['product_id' => $product->id])->all();

        $catalog = Catalog::findOne(['url' => $product->category->url]);

        if(!$product){
            throw new NotFoundHttpException();
        }

        $recommendProduct = Recommend::findAll(['product1' => $product->id]);
        $in = [];
        $sql = 0;
        foreach ($recommendProduct as $v){
            $in[] = $v->product2;
        }

        if($in)
            $sql = 'id in ('.implode(',', $in).')';

        $recommendProduct = Products::find()->where($sql)->all();

        return $this->render('product', compact('product', 'catalog', 'mainSub',
            'recommendProduct', 'image'));
    }

    public function actionFilter()
    {
        $category_id = $_GET['category_id'];

        $sql = '';

        if(empty($category_id)) {

            $querySql = $this->querySql($sql);
            $queryOrder = $this->queryOrder($sql);
            $queryPrice = $this->priceFilter($sql, $querySql, $category_id);

            $products = Products::find()->where($querySql.$queryPrice)->orderBy($queryOrder)->all();

            return $this->renderAjax('filter', compact('products'));
        }else{

            $querySql = $this->querySqlCategory($sql);
            $queryOrder = $this->queryOrder($sql);
            $queryPrice = $this->priceFilter($sql, $querySql, $category_id);

            $catalogs = Catalog::find()->where('status = 1')->all();
            $result = $this->get_cat($catalogs);
            $products = $this->get_level_filter($result, $category_id, $querySql, $queryOrder, $queryPrice);


            return $this->renderAjax('filterCategory', compact('products', 'category_id'));
        }
    }

    public function PriceFilter($price, $querySql, $category_id)
    {
        if(!empty($querySql)) {
            if (empty($_GET['from']) && empty($_GET['to']))
                $price .= '';

            elseif (!empty($_GET['from']) && empty($_GET['to']))
                $price .= ' AND price >= ' . $_GET['from'];

            elseif (empty($_GET['from']) && !empty($_GET['to']))
                $price .= ' AND price <= ' . $_GET['to'];

            elseif (!empty($_GET['from']) && !empty($_GET['to']))
                $price .= ' AND price BETWEEN ' . $_GET['from'] . ' AND ' . $_GET['to'];
        }elseif(!empty($category_id)){
            if (empty($_GET['from']) && empty($_GET['to']))
                $price .= '';

            elseif (!empty($_GET['from']) && empty($_GET['to']))
                $price .= ' AND price >= ' . $_GET['from'];

            elseif (empty($_GET['from']) && !empty($_GET['to']))
                $price .= ' AND price <= ' . $_GET['to'];

            elseif (!empty($_GET['from']) && !empty($_GET['to']))
                $price .= ' AND price BETWEEN ' . $_GET['from'] . ' AND ' . $_GET['to'];
        }else{
            if (empty($_GET['from']) && empty($_GET['to']))
                $price .= '';

            elseif (!empty($_GET['from']) && empty($_GET['to']))
                $price .= ' price >= ' . $_GET['from'];

            elseif (empty($_GET['from']) && !empty($_GET['to']))
                $price .= ' price <= ' . $_GET['to'];

            elseif (!empty($_GET['from']) && !empty($_GET['to']))
                $price .= ' price BETWEEN ' . $_GET['from'] . ' AND ' . $_GET['to'];
        }

        return $price;
    }

    public function QuerySubject($subject, $sql)
    {
        $subjectName = Subjects::find()->all();
        foreach($subjectName as $value) {
            $filter = 'subjects'.$value->id;
            if ($_GET['subjects'] == $filter) {
                if(isset($sql) && $sql !== '') {
                    $subject .= ' AND subject_id = ' . $value->id . '';
                }else{
                    $subject .= ' subject_id = ' . $value->id . '';
                }
            }
        }

        return $subject;
    }

    public function QuerySubjectCategory($subject)
    {
        $subjectName = Subjects::find()->all();
        foreach($subjectName as $value) {
            $filter = 'subjects'.$value->id;
            if ($_GET['subjects'] == $filter) {
                $subject .= ' AND subject_id = '.$value->id.'';
            }
        }

        return $subject;
    }


    public function QuerySql($sql)
    {
        if ($_GET['select'] == 'available') {
            $sql .= 'status = 1';
        }

        if ($_GET['select'] == 'isDiscount') {
            $sql .= '(isDiscount != 0 OR isDiscount != NULL)';
        }

        if ($_GET['select'] == 'isNew') {
            $sql .= '';
        }

        if ($_GET['select'] == 'isHit') {
            $sql .= '(isHit != 0 OR isHit != NULL)';
        }

        if ($_GET['select'] == 'top_month') {
            $sql .= '(top_month != 0 OR top_month != NULL)';
        }

        return $sql;
    }

    public function QuerySqlCategory($sql)
    {
        if ($_GET['select'] == 'available') {
            $sql .= ' AND status = 1 ';
        }

        if ($_GET['select'] == 'isDiscount') {
            $sql .= ' AND (isDiscount != 0 OR isDiscount != NULL) ';
        }

        if ($_GET['select'] == 'isNew') {
            $sql .= '';
        }

        if ($_GET['select'] == 'isHit') {
            $sql .= ' AND (isHit != 0 OR isHit != NULL) ';
        }

        if ($_GET['select'] == 'top_month') {
            $sql .= ' AND (top_month != 0 OR top_month != NULL)';
        }

        return $sql;
    }

    public function QueryOrder($order)
    {
        if ($_GET['select'] == 'available') {
            $order .= '';
        }

        if ($_GET['select'] == 'isDiscount') {
            $order .= '';
        }

        if ($_GET['select'] == 'isNew') {
            $order .= 'isHit DESC';
        }

        if ($_GET['select'] == 'isHit') {
            $order .= '';
        }

        if ($_GET['select'] == 'top_month') {
            $order .= '';
        }

        return $order;
    }

}
