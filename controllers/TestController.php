<?php

namespace app\controllers;

use app\models\Catalog;
use app\models\Currency;
use yii\web\Controller;
use  Dompdf\Dompdf ;

class TestController extends Controller
{
    public function actionTest()
    {
        $dir = \Yii::$app->basePath.'/sync';

        $products = file_get_contents($dir . "/products.json");

        $products = json_decode($products, true);

        $array = [];

        foreach($products as $_product) {
            foreach($_product as $k => $value) {
                if ($k == "Рабочее наименование"){
                    $array[] = $value;

//                    var_dump($k . ' => ' . $value); die;
                }
            }
        }

        print_r("<pre>");
        print_r($array); die;
    }

    public function actionPdf()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;

        $dompdf = new Dompdf();

        $dompdf->loadHtml('hello world!');

        $dompdf->setPaper('A4', 'landscape');

        $dompdf->render();

        $output = $dompdf->output();

        $file = file_get_contents(\Yii::$app->basePath . '/web/uploads/files/order/order.pdf', $output);


        return $file;
//        $dompdf->stream('/uploads/file/order/order.pdf');
    }

    public function actionSync()
    {
        $dir = \Yii::$app->basePath.'/sync';

        $products = file_get_contents($dir . "/products.json");

        $products = json_decode($products, true);

        $array = [];
        foreach($products as $_product) {
            foreach($_product as $k => $value) {
                if ($k != "ФайлКартинки") {
                    $array[$k] = $value;
                }

                $parent = explode('/', $array['Родитель']);
                array_shift($parent);

                $level = [];
                foreach($parent as $item){
                    $level[] = explode('. ', $item);
                }
            }
            print_r("<pre>");
            print_r($array['Родитель']);
            die;
        }

//        print_r("<pre>");
//        print_r($array);
    }

//    public function actionTest()
//    {
//        $target = $this->code();    // коды валюты
//
//        $test = self::currency();   // вызов конвертера по api
//
//        $result = [];
//
//        foreach($test->rates as $key => $value){
//            if(in_array($key, $target ))
//                $result[$key] = $value;
//        }
//
//        $update = $this->updateCurrency($result);
//
//        if($update){
//            return true;
//        }else{
//            return false;
//        }
//    }

    public static function currency()
    {
        $currency = simplexml_load_file('https://nationalbank.kz/rss/rates_all.xml');

        $array = [];
        foreach ($currency->channel as $value){
            foreach ($value->item as $item){
                $array[] = $item;
            }
        }

        return $array;
    }

    public function actionCons()
    {
        $currency = self::currency();

        $target = $this->code();

        $result = [];
        foreach ($currency as $item) {
            if(in_array($item->title, $target)){
                $result[] = $item;
            }
        }

        $update = $this->update($result);

        print_r("<pre>");
        print_r($update);
    }

    public function updateCurrency($result)
    {
        foreach ($result as $key => $value){
            $model = Currency::find()->where(['code' => $key])->one();
            $values = (int)1/$value;

            $model->value = $values;
            $model->save(false);
        }

        return 1;
    }

    public function update($result)
    {
        foreach ($result as $value){
            $model = Currency::find()->where(['code' => $value->title])->one();
            $values = $value->description;

            $model->value = $values;
            $model->save(false);
        }

        return 1;
    }

    public function code()
    {
        $model = Currency::find()->all();
        $code = [];
        foreach ($model as $item){
            $code[] = $item->code;
        }

        return $code;
    }

    public function actionConversion()
    {
        $price = 500;

        $currency = Currency::find()->all();

        $array = [];

        foreach($currency as $item){

            $conversion = (int)$price / (int)$item->value;
            $key       = $item->name;
            $conversion    = floor($conversion * 100)/100;

            if($item->code !== "KZT") {
                $array[] = $conversion . ' ' . $key;
            }

        }


        $text = implode(' или ', $array);

        echo "<pre>";
        print_r($text);
    }

    public function actionTree()
    {
        $catalog = Catalog::find()->all();

        $result = $this->getCatalogItems($catalog);

        $test = $this->drawCatalogItems($result);

        echo "<pre>";
        print_r($test); die;
    }

    private function drawCatalogItems($menuItems, $parent_id = null)
    {
        $html = "";
        foreach ($menuItems as $item) {
            if($item->parent_id !== $parent_id) {
                continue;
            }

            $parentItems = mb_substr($this->parentItems($menuItems, $item), 0, -1);
            $parentItems = array_reverse(explode('-', $parentItems));
            $parentItems = implode('-', $parentItems);

            $html .= '<li><img src="/public/images/no-active.png">';
            $html .= '<a class="firts-tabs category_filter" href="#'.$item->id .'" data-id="'.$item->id.'">';
            $html .= $item->name . '</a>';
            $html .= '<br>'.$parentItems.'</br>';
            if(!empty($item->childs)) {
                $html .= '<ul id="'.$item->id.'">'.$this->drawCatalogItems($menuItems, $item->id).'</ul>';
            }
        }

        return $html;
    }

    public function parentItems($menuItems, $item)
    {
        $tmp = $item->id.'-';

        if(!empty($menuItems[$item->parent_id])) {
            $tmp .= $this->parentItems($menuItems, $menuItems[$item->parent_id]);
        }

        return $tmp;
    }

    private function getCatalogItems($categories)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            $menuItems[$category->id] = $category;
        }

        foreach ($categories as $category) {
            if(isset($menuItems[$category->parent_id])) {
                $menuItems[$category->parent_id]->childs[] = $category;
            }
        }

        return $menuItems;
    }

    private function drawMenuItems($menuItems)
    {
        $html = "";
        foreach ($menuItems as $item){
            $html .= '<li><img src="/public/images/no-active.png">';
            $html .= '<a class="'.$this->classLevel($item['level']).' category_filter" href="#'.$item['active'] .'" data-id="'.$item['active'].'">';
            $html .= $item['label'] . '</a>';
            if(!empty($item['items'])){
                $html .= '<ul id="'.$item['active'].'" style="display: none">'.$this->drawMenuItems($item['items']).'</ul>';
            }
        }

        return $html;
    }

    public function classLevel($level)
    {
        if($level == 1){
            $class = 'firts-tabs';
        }elseif($level == 2){
            $class = 'second-tabs';
        }else{
            $class = 'third-city';
        }

        return $class;
    }

    private function getMenuItems($categories, $activeId = null, $parent = null)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            if ($category->parent_id == $parent) {
                $menuItems[$category->id] = [
                    'active'    => $category->id,
                    'label'     => $category->name,
                    'level'     => $category->level,
                    'items'     => $this->getMenuItems($categories, $activeId, $category->id),
                ];
            }
        }

        return $menuItems;
    }

//    public function actionConversion()
//    {
//        $target = $this->code();    // коды валюты
//
//        $test = self::currency();   // вызов конвертера по api
//
//        $result = [];
//
//        foreach($test->rates as $key => $value){
//            if(in_array($key, $target ))
//                $result[$key] = $value;
//        }
//
//        $update = $this->updateCurrency($result);   //  изменение значение валюты в бд
//
//        if($update){
//            return true;
//        }else{
//            return false;
//        }
//    }
//
//    public static function currency()
//    {
//        $target = "KZT";
//
//        $currency = @file_get_contents('https://api.exchangerate-api.com/v4/latest/'.$target);
//
//        $currency = json_decode($currency);
//
//        return $currency;
//    }
//
//    public function updateCurrency($result)
//    {
//        foreach ($result as $key => $value){
//            $model  = Currency::find()->where(['code' => $key])->one();
//            $values = (int)1/$value;
//
//            $model->value = $values;
//            $model->save(false);
//        }
//
//        return 1;
//    }
//
//    public function code()
//    {
//        $model = Currency::find()->all();
//        $code = [];
//        foreach ($model as $item){
//            $code[] = $item->code;
//        }
//
//        return $code;
//    }

//if(isset($categories[1])) {
//$second_category = $this->findCatalog($categories[1][0], $first_category->id, $categories[1][1] ?? '', $categories[1][2] ?? '');
//$catalogs[] = $second_category;
//}
//if(isset($categories[2])) {
//    $third_category = $this->findCatalog($categories[2][0], $second_category->id, $categories[2][1] ?? '', $categories[2][2] ?? '');
//    $catalogs[] = $third_category;
//}
//if(isset($categories[3])) {
//    $four_category = $this->findCatalog($categories[3][0], $third_category->id, $categories[3][1] ?? '', $categories[3][2] ?? '');
//    $catalogs[] = $four_category;
//}
//
//if(isset($categories[4])) {
//    $five_category = $this->findCatalog($categories[4][0], $third_category->id, $categories[4][1] ?? '', $categories[4][2] ?? '');
//    $catalogs[] = $five_category;
//}
//    public function findParent($name)
//    {
//        if(!$catalog = Catalog::find()->where(['name' => $name[0]])->one()){
//            $catalog = new Catalog();
//            $catalog->name      = $name[0];
//            $catalog->name_kz   = $name[1];
//            $catalog->name_en   = $name[2];
//            $catalog->status    = 1;
//            $catalog->parent_id = NULL;
//            $catalog->level     = 1;
//            $catalog->save(false);
//        }
//
//        return $catalog;
//    }
//    public function findCatalog($name, $parent_id, $name_kz, $name_en) {
//        $attr = [
//            'name' => $name,
//            'parent_id' => $parent_id
//        ];
//
//        if(!($catalog = Catalog::find()->where($attr)->one())) {
//            $catalog = new Catalog();
//            $catalog->name = $attr['name'];
//            $catalog->name_kz = $name_kz;
//            $catalog->name_en = $name_en;
//            $catalog->parent_id = $attr['parent_id'];
//            $catalog->level = $catalog->parent->level + 1;
//            $catalog->status = 1;
//            $catalog->save(false);
//        }
//
//        return $catalog;
//    }
//
//    public function catalog($catalog)
//    {
//        $explode = explode('/', $catalog);
//        array_shift($explode);
//
//        $result = [];
//        foreach($explode as $item){
//            $result[] = explode('. ', $item);
//        }
//
//        return $result;
//    }

//    public function actionImage()
//    {
//        \Tinify\setKey("YVGpZ02htDylhmLBMk3dzwZfNMS6TXg9");
//
//
//        $source = \Tinify\fromFile("asd");
//        $source->toFile("asd");
//        \Tinify\validate();
//    }
//
//    public function actionTest()
//    {
//        $dir = Yii::$app->basePath.'/sync';
//
//        $products = file_get_contents($dir . "/products.json");
//
//        $products = json_decode($products, true);
//
//        $array = [];
//        foreach($products as $_product) {
//            $array[] = $_product;
//            //base64_decode($_product['ФайлКартинки']);
//            //$array[] = $_product;
//            foreach($_product as $k => $value) {
//                if ($k != "ФайлКартинки"){
//                    print_r($k . ' => ' . $value . "\n");
//                }
//            }
//        }
//
//        //print_r("<pre>");
//        //print_r($array);
//    }
//
//    public function actionWatermark()
//    {
//        $dir = Yii::$app->basePath . '/web/uploads/images/products/';
//        $file = file_get_contents($dir . "1565328738_KeizersgrachtReguliersgrachtAmsterdam.jpg");
//
//        $imagine = new \Imagine\Gd\Imagine();
//        $watermark = $imagine->open(Yii::$app->basePath . '/web/public/images/water_mark.png');
//        $image     = $imagine->open($file);
//
//        $bottomRight = new \Imagine\Image\Point(50, 50);
//
//        $image->paste($watermark, $bottomRight)->save($file);
//    }

}