<?php

namespace app\controllers;

use app\models\Orders;
use pantera\yii2\pay\alfabank\components\Alfabank;
use pantera\yii2\pay\alfabank\models\Invoice;
use Yii;
use yii\web\Controller;

class PayController extends FrontendController
{
    const USERNAME      = 'altuka_kz-api';
    const PASSWORD      = 'E?dme333';
    const GATEWAY_URL   = 'https://pay.alfabank.kz/payment/rest/';
    const RETURN_URL    = 'https://al2ka.kz/pay/success';

    const PASSWORD_TEST         = 'altuka_kz*?1';
    const GATEWAY_URL_TEST      = 'https://web.rbsuat.com/ab/rest/';

    public function actionPay()
    {
        $id = $_GET['id'];

        if(!empty($id)) {

            $order = Orders::findOne(['id' => $id]);
            $orderId = $order->order_id;
            $amount  = (int)$order->sum;

            $data = [
                'userName'      => self::USERNAME,
                'password'      => self::PASSWORD,
                'orderNumber'   => urlencode($orderId),
                'amount'        => urlencode($amount.'00'),
                'returnUrl'     => self::RETURN_URL
            ];

            $response = $this->gateway('register.do', $data);


            if (isset($response['errorCode'])) { // В случае ошибки вывести ее
                Yii::$app->getSession()->setFlash('error', $response['errorMessage']);
                return $this->redirect(['/site/index']);
//                echo 'Ошибка #' . $response['errorCode'] . ': ' . $response['errorMessage'];
            } else { // В случае успеха перенаправить пользователя на платежную форму
                header('Location: ' . $response['formUrl']);
                die();
            }
        }else{
            Yii::$app->getSession()->setFlash('error', 'Ошибка!');
            return $this->redirect(['/site/index']);
        }
    }

    public function gateway($method, $data)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL             => self::GATEWAY_URL.$method,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS      => http_build_query($data)
        ]);
        $response = curl_exec($curl);

        $response = json_decode($response, true);
        curl_close($curl);

        return $response;
    }

    public function actionError()
    {
        return "Ошибка";
    }

    public function actionSuccess()
    {
        $orderId = $_GET['orderId'];


        if(isset($orderId)){
            $data = [
                'userName'  => self::USERNAME,
                'password'  => self::PASSWORD,
                'orderId'   => $orderId
            ];

            $response = $this->gateway('getOrderStatus.do', $data);

            if($response['OrderStatus'] == 2){
                $order = $this->updateOrder($response['OrderNumber']);

                Yii::$app->sync->orderExport($order->order_id);
                Yii::$app->getSession()->setFlash('paySuccess', 'Спасибо, скоро мы свяжемся с вами!');
                return $this->redirect(['/account/index']);

            }elseif ($response['OrderStatus'] == 3){
                Yii::$app->getSession()->setFlash('payError3', 'Ошибка! Авторизация отменена');
                return $this->redirect(['/site/index']);
            }elseif ($response['OrderStatus'] == 0){
                Yii::$app->getSession()->setFlash('payError0', 'Ошибка! 	Заказ зарегистрирован, но не оплачен');
                return $this->redirect(['/site/index']);
            }else{
                Yii::$app->getSession()->setFlash('error', 'Ошибка!' . $response['errorMessage']);
                return $this->redirect(['/site/index']);
            }
        }

    }

    public function updateOrder($order_id)
    {
        $order = Orders::findOne(['order_id' => $order_id]);

        $order->statusPay = 1;

        $order->save(false);

        return $order;
    }

    public function actionTest()
    {
        $orderId = 152456645654;
        $amount  = (int)10;

        $data = [
            'userName'      => self::USERNAME,
            'password'      => self::PASSWORD,
            'orderNumber'   => urlencode($orderId),
            'amount'        => urlencode($amount.'00'),
            'returnUrl'     => 'https://al2ka.kz/pay/successf',
        ];

        $response = $this->gateway('register.do', $data);


        if (isset($response['errorCode'])) { // В случае ошибки вывести ее
            Yii::$app->getSession()->setFlash('error', $response['errorMessage']);

            echo '<b>Error code:</b> ' . $response['errorMessage'] . '<br />';
//            return $this->redirect(['/site/index']);
//                echo 'Ошибка #' . $response['errorCode'] . ': ' . $response['errorMessage'];
        } else { // В случае успеха перенаправить пользователя на платежную форму
            header('Location: ' . $response['formUrl']);
            die();
        }
    }

    public function actionSuccessf()
    {
        $orderId = $_GET['orderId'];

        $data = [
            'userName'  => self::USERNAME,
            'password'  => self::PASSWORD,
            'orderId'   => $orderId
        ];

        $response = $this->gateway('getOrderStatus.do', $data);

        echo('
            <b>Error code:</b> ' . $response['ErrorCode'] . '<br />
            <b>Order status:</b> ' . $response['OrderStatus'] . '<br />
        ');

        die;


    }


//    public function actionTest()
//    {
//        $order_id = 1573722509;
//
//        $order = $this->updateOrder($order_id);
//
////        $order = Orders::findOne(['order_id' => $order_id]);
//
//        echo "<pre>";
//        var_dump($order);
//        echo "</pre>";
//    }
}