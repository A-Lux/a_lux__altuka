<?php

namespace app\controllers;

use app\models\Products;
use app\models\Menu;
use app\models\Catalog;
use yii\web\Controller;
use Yii;

class SearchController extends FrontendController
{

    public function actionIndex($text)
    {
        $menu = Menu::find()->all();
        $model = Menu::find()->where('url = "/search"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $search = $text;
		if($text){
            $product = $this->AllSearch($text);
            $count = count($product);
        }else{
		    $count = 0;
			return $this->render('index', compact('search', 'count', 'text', 'model', 'menu'));
        }
        
        return $this->render('index', compact('product','count','search', 'model', 'menu'));
    }

    public function AllSearch($text)
    {
        $result = array();
        $products = Products::find()->where("name LIKE '%$text%'")->all();
        $productByCatalog = $this->SearchCatalog($text); 
        foreach($products as $v){
            array_push($result,$v);
        }
        if($productByCatalog){
            foreach($productByCatalog as $val){
                $status = true;
                foreach($products as $v){
                    if($v->id == $val->id){
                        $status = false;
                    }
                }

                if($status){
                    array_push($result,$val);
                }
            }

            $product = $result;
        }else{
            $product = Products::find()->where("name LIKE '%$text%'")->all();
        }

        return $product;
    }

    public function SearchCatalog($text)
    {
        $catalogs = Catalog::find()->where("name LIKE '%$text%'")->all();
        if($catalogs){
            $ins = [];
            foreach($catalogs as $level1){
                if($level1->childs){
                    foreach ($level1->childs as $level2){
                        if($level2->childs){
                            foreach($level2->childs as $level3){
                                if($level3->childs){
                                    foreach($level3->childs as $level4){
                                        if($level4->childs){
                                            foreach($level4->childs as $level5){
                                                $ins[] = $level5->id;
                                            }
                                        }
                                        $ins[] = $level4->id;
                                    }
                                }
                                $ins[] = $level3->id;
                            }
                        }
                        $ins[] = $level2->id;
                    }
                }
                $ins[] = $level1->id;
            }
                
            $products = Products::find()->where('category_id in ('.implode(',', $ins).') AND status = 1')->all();
        }

        return $products;
    }

    public function actionTest()
    {
        $text = "магниты";
        $result = array();

        $products = Products::find()->where("name LIKE '%$text%'")->all();
        $productByCatalog = $this->SearchCatalog($text); 
        foreach($products as $v){
                array_push($result,$v);
        }
        if($productByCatalog){
            foreach($productByCatalog as $val){
                $status = true;
                foreach($products as $v){
                    if($v->id == $val->id){
                        $status = false;
                    }
                }

                if($status){
                    array_push($result,$val);
                }
            }
            $product = $result;
        }else{
            $product = Products::find()->where("name LIKE '%$text%'")->all();
        }

        //var_dump($product);die;
    }
}