<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class LangController extends Controller
{
    public function actionIndex($url)
    {
        if($url == 'ru'){
            Yii::$app->session->set('lang', '');
        }elseif ($url == 'kz'){
            Yii::$app->session->set('lang', '_kz');
        }elseif ($url == 'en'){
            Yii::$app->session->set('lang', '_en');
        }else{
            throw new ForbiddenHttpException();
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}