<?php

namespace app\controllers;


use app\components\Sync;
use app\models\Catalog;
use app\models\Currency;
use app\models\InfoBasket;
use app\models\Menu;
use app\models\MainSub;
use app\models\Offer;
use app\models\Orderedproduct;
use app\models\Orders;
use app\models\PriceMinimum;
use app\models\Products;
use app\models\Requisites;
use app\models\User;
use app\models\UserAddress;
use app\models\UserProfile;
use Dompdf\Dompdf;
use Dompdf\Options;
use kartik\mpdf\Pdf;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class CardController extends FrontendController
{

    public function actionIndex()
    {
        $model = Menu::find()->where('url = "/card"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $minPrice = PriceMinimum::find()->orderBy('id ASC')->one();
        $info = InfoBasket::find()->orderBy('id ASC')->one();
        $offer = Offer::findOne(['id' => 1]);
        $requisites = Requisites::find()->orderBy('id ASC')->one();

        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        if(!Yii::$app->user->isGuest){
            $user = User::findOne(Yii::$app->user->identity->id);
            $profile = UserProfile::findOne(['user_id'=>Yii::$app->user->identity->id]);
            $address = UserAddress::findOne(['user_id' => Yii::$app->user->identity->id]);
        }
        
        return $this->render('basket',compact('sum','model',
            'user', 'profile', 'minPrice', 'info', 'address', 'offer', 'requisites'));
    }


    public function actionDeleteProduct($id)
    {
        $m=0;
//        die(debug($_SESSION['basket']));
        foreach ($_SESSION['basket'] as $v){
            if($v->id == $id){
                unset($_SESSION['basket'][$m]);
                $_SESSION['basket'] = array_values($_SESSION['basket']);
                break;
            }
            $m++;
        }
        return $this->redirect(['index']);
    }

    public function actionDeleteAll()
    {
        unset($_SESSION['basket']);

        unset($_SESSION['basket-product-own']);

        return $this->redirect(['index']);
    }

    public function actionAddProduct()
    {
        $id = $_GET['id'];
        $amount = $_GET['amount'];

        $card = [];
        foreach ($_SESSION['basket'] as $value){
            $card[] = $value->id;
        }
        if(!in_array($id, $card)){
            $basket = Products::findOne($id);
            if (isset($amount)) {
                $basket->count = $amount;
            } else {
                $basket->count = 1;
            }
            $check = true;
            foreach ($_SESSION['basket'] as $v) {
                if ($v->id == $id) {
                    $check = false;
                    break;
                }
            }
            if ($check) {
                array_push($_SESSION['basket'], $basket);
            }
            $count = count($_SESSION['basket']);
            $array = ['status' => 1, 'count' => $count];
        }else {
            $array = ['status' => 2];
        }

        return json_encode($array);

    }

    public function actionDownClicked()
    {
        $count = 1;
        foreach ($_SESSION['basket'] as $k=>$v){
            if($v->id == $_GET['id'] && $_SESSION['basket'][$k]->count > 1){
                $_SESSION['basket'][$k]['count']-=1;
                $count = $_SESSION['basket'][$k]['count'];
                break;
            }
        }

        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        $sumProduct = Products::getSumProduct($_GET['id']);
        $array = ['countProduct' => $count, 'sum' => $sum,'sumProduct'=>$sumProduct];
        return json_encode($array);
    }


    public function actionUpClicked()
    {
        $count = 1;
        foreach ($_SESSION['basket'] as $k=>$v){
            if($v->id == $_GET['id']){
                $_SESSION['basket'][$k]['count']+=1;
                $count = $_SESSION['basket'][$k]['count'];
                break;
            }
        }

        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        $sumProduct = Products::getSumProduct($_GET['id']);
        $array = ['countProduct' => $count, 'sum' => $sum,'sumProduct'=>$sumProduct];
        return json_encode($array);
    }

    public function actionCountChanged()
    {
        foreach ($_SESSION['basket'] as $k=>$v){
            if($v->id == $_GET['id']){
                if($_GET['v']>0){
                    $_SESSION['basket'][$k]['count'] = $_GET['v'];
                }else{
                    $_SESSION['basket'][$k]['count']=1;
                }
            }
        }

        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        $sumProduct = Products::getSumProduct($_GET['id']);
        $array = ['sum' => $sum,'sumProduct'=>$sumProduct];
        return json_encode($array);
    }

    public function actionOrderPay()
    {
        $id = time();
        $sum = (int)Products::getSum() + (int)Products::getSumOwn();
        $order = new Orders();
        $order->user_id         = Yii::$app->user->isGuest ? 0 : Yii::$app->user->identity->id;
        $order->order_id        = $id;
        $order->order_own       = count($_SESSION['basket-product-own']) == 0 ? null : 1 ;
        $order->statusPay       = 0;
        $order->statusProgress  = 0;
        $order->type            = 2;
        $order->sum             = $sum;

        if ($order->load(Yii::$app->request->post()) && $order->validate()) {
            if ($order->save()) {
                foreach ($_SESSION['basket'] as $v) {
                    $orderedProduct = new OrderedProduct();
                    $orderedProduct->order_id       = $order->order_id;
                    $orderedProduct->product_id     = $v->id;
                    $orderedProduct->count          = $v->count;
                    $orderedProduct->save();
                }

                foreach ($_SESSION['basket-product-own'] as $v) {
                    $orderedProduct = new OrderedProduct();
                    $orderedProduct->order_id       = $order->order_id;
                    $orderedProduct->own_id         = $v->id;
                    $orderedProduct->product_id     = $v->product->id;
                    $orderedProduct->count          = $v->count;
                    $orderedProduct->save();
                }

                Yii::$app->sync->orderExport($order->order_id);
                $response = ['status' => 1, 'order_id' => $order->id];

                return json_encode($response);
            }else {
                $response = ['status' => 0];
                return json_encode($response);
            }
        }else{
            $errors = "";
            $error = $order->getErrors();
            foreach($error as $v){
                $errors .= $v[0];
                break;
            }
            return $errors;
        }
    }

    public function actionOnlinePayment()
    {
        $id = $_GET['id'];

        if($order = Orders::findOne(['id' => $id])){
             $this->sendOrderOperator($order->order_id, $order->comment, $order->fullname, $order->phone, $order->email, $order->address, $order->statusPay);
             $this->sendInformationAboutPayment($order->fullname, $order->email, $order->order_id);
            $response = ['status' => 1];

            unset($_SESSION['basket']);
            unset($_SESSION['basket-product-own']);
            return json_encode($response);
        }else{
            $response = ['status' => 0];

            return json_encode($response);
        }

    }


    public function actionTransactionPayment()
    {
        $id = $_GET['id'];

        if($order = Orders::findOne(['id' => $id])){
            $order->statusPay = 2;
            $order->save();
             $this->sendOrderOperator($order->order_id, $order->comment, $order->fullname, $order->phone, $order->email, $order->address, $order->statusPay);
             $this->sendInformationAboutPayment($order->fullname, $order->email, $order->order_id);

            $product = $this->getOrderProduct($order->order_id);

             Yii::$app->sync->orderExport($order->order_id);
            unset($_SESSION['basket']);
            unset($_SESSION['basket-product-own']);
            $_SESSION['order'] = $order;
            $response = [
              'status' => 1, 'order_id' => $order->order_id,
              'order_date' => $order->created, 'order_sum' => $order->sum,
              'order_fullname' => $order->fullname, 'product' => $product
            ];

            return json_encode($response);
        }else{
            $response = ['status' => 0];

            return json_encode($response);
        }
    }

    protected function getOrderProduct($order_id)
    {
        $orderProduct = Orderedproduct::find()->where(['order_id' => $order_id])->all();

        $product = [];

        foreach ($orderProduct as $key => $item){
            $product[$key]['name'] = $item->product->name;
            $product[$key]['name_en'] = $item->product->name_en;
            $product[$key]['image'] = $item->product->getImage();
            $product[$key]['price'] = $item->product->price;
            $product[$key]['count'] = $item->count;
        }

        return $product;
    }

    public function actionPdf()
    {
        if(!isset($_SESSION['order'])) throw new NotFoundHttpException();
        $order = $_SESSION['order'];
        $products = Orderedproduct::find()->where(['order_id' => $order->order_id])->all();
        $currency = Currency::getList();
        $content = $this->renderPartial('pdf',compact('order','products','currency'));


        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,

            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting

            // set mPDF properties on the fly
            'methods' => [
            'SetTitle' => 'Microsoft Word - Al2ka Proforma Invoice Sample',]

        ]);


        unset($_SESSION['order']);

        return $pdf->render();


    }


}

