<?php

namespace app\controllers;

use app\models\About;
use app\models\Agreement;
use app\models\Contact;
use app\models\Faq;
use app\models\Feedback;
use app\models\MainSub;
use app\models\Menu;
use app\models\News;
use app\models\Telephone;
use app\models\UserProfile;
use Yii;

class ContentController extends FrontendController
{

    public static function cutStr($str, $length=50, $postfix=' ...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }

    public function actionAbout()
    {
        $model = Menu::find()->where('url = "/content/about"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $mainSub = MainSub::find()->all();

        $about = About::find()->one();
        $contact = Contact::find()->one();
        $phone = Telephone::find()->all();

        return $this->render('about', compact('model','about', 'mainSub',
            'contact', 'phone', 'about'));
    }

    public function actionAgreement()
    {
        $model = Menu::find()->where('url = "/content/agreement"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $agreement = Agreement::find()->one();


        return $this->render('agreement', compact('model', 'agreement'));
    }

    public function actionNews()
    {
        $model = Menu::find()->where('url = "/content/news"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $news = News::find()->orderBy('date DESC')->all();

        return $this->render('news', compact('model', 'news'));
    }

    public function actionNewsCard($id)
    {
        $news = News::findOne(['id' => $id]);
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($news->$metaName, $news->$metaKey, $news->$metaDesc);

        return $this->render('news-card', compact('news'));
    }

    public function actionQuestion()
    {
        $model = Menu::find()->where('url = "/content/question"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);

        $mainSub = MainSub::find()->all();

        $faq = Faq::find()->all();
        return $this->render('questions', compact('model', 'faq', 'mainSub'));
    }

    public function actionReview()
    {
        $model = Menu::find()->where('url = "/content/review"')->one();
        $metaName   = "metaName".Yii::$app->session["lang"];
        $metaKey    = "metaKey".Yii::$app->session["lang"];
        $metaDesc   = "metaDesc".Yii::$app->session["lang"];
        $this->setMeta($model->$metaName, $model->$metaKey, $model->$metaDesc);
        if(!Yii::$app->user->isGuest) $profile = UserProfile::findOne(['user_id' => Yii::$app->user->id]);



        $feedback = Feedback::getAll();

        return $this->render('feedback', compact('model', 'feedback', 'menu', 'profile'));
    }

}
