<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $status
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property string $date
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */
class News extends \yii\db\ActiveRecord
{

    public $path = 'uploads/images/news/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],

            [[
                'content', 'content_en', 'content_kz',
                'metaDesc', 'metaDesc_en', 'metaDesc_kz',
                'metaKey', 'metaKey_en', 'metaKey_kz'
            ], 'string'],

            [['date'], 'safe'],

            [[
                'name', 'name_en', 'name_kz',
                'metaName', 'metaName_en', 'metaName_kz', 'image'
                ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Название',
            'name_en'       => 'Название на английском',
            'name_kz'       => 'Название на казахском',
            'image'         => 'Картинка',
            'content'       => 'Содержание',
            'content_en'    => 'Содержание на английском',
            'content_kz'    => 'Содержание на казахском',
            'date'          => 'Дата',
            'metaName'      => 'Мета Названия',
            'metaName_en'   => 'Мета Названия на английском',
            'metaName_kz'   => 'Мета Названия на казахском',
            'metaDesc'      => 'Мета Описание',
            'metaDesc_en'   => 'Мета Описание на английском',
            'metaDesc_kz'   => 'Мета Описание на казахском',
            'metaKey'       => 'Ключевые слова',
            'metaKey_en'    => 'Ключевые слова на английском',
            'metaKey_kz'    => 'Ключевые слова на казахском',
            'status'        => 'Статус рассылки',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/news/' . $this->image : '/no-image.png';
    }

    public function beforeDelete()
    {
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date, 'long');
    }

    public static function getAll($pageSize=5)
    {
        $query = News::find()->orderBy('date desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
