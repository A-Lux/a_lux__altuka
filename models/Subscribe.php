<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subscribe".
 *
 * @property int $id
 * @property string $email
 */
class Subscribe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscribe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],

            [['email'], 'required'],
            [['email'], 'email'],

            [['email'], 'unique', 'targetClass'=>'app\models\Subscribe'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'email' => 'Эл. адрес',
        ];
    }

    public function sendEmail()
    {
        $email = Emailforrequest::find()->all();

        foreach ($email as $item) {
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([Yii::$app->params['adminEmail'] => 'AL2KA'])
            ->setTo($item->email)
            ->setSubject('Пользователь подписался на рассылку')
            ->setHtmlBody("<p>Email: $this->email</p>
                             ");
            $emailSend->send();
        }

        return 1;

    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'email');
    }
}
