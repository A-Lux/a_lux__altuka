<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $description
 * @property string $description_en
 * @property string $description_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'content'], 'required'],
            [['description', 'description_en', 'description_kz',
                'content', 'content_en', 'content_kz'], 'string'],
            [['title', 'title_en', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'title'             => 'Заголовок',
            'title_en'          => 'Заголовок на английском',
            'title_kz'          => 'Заголовок на казахском',
            'description'       => 'Краткое описание',
            'description_en'    => 'Краткое описание на английском',
            'description_kz'    => 'Краткое описание на казахском',
            'content'           => 'Контент',
            'content_en'        => 'Контент на английском',
            'content_kz'        => 'Контент на казахском',
        ];
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'title');
    }
}
