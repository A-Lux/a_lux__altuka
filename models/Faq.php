<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [[
                'name', 'name_en', 'name_kz',
                'content', 'content_en', 'content_kz'
            ], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Вопрос',
            'name_en'       => 'Вопрос на английском',
            'name_kz'       => 'Вопрос на казахском',
            'content'       => 'Ответ',
            'content_en'    => 'Ответ на английском',
            'content_kz'    => 'Ответ на казахском',
        ];
    }
}
