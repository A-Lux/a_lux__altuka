<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "price_minimum".
 *
 * @property int $id
 * @property int $price
 */
class PriceMinimum extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_minimum';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price'], 'required'],
            [['price'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'price'     => 'Цена',
        ];
    }
}
