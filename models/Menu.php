<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 * @property int $status
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'status', 'metaName', 'metaDesc', 'metaKey'], 'required'],
            [['status'], 'integer'],
            [[
                'metaDesc', 'metaDesc_en', 'metaDesc_kz',
                'metaKey', 'metaKey_en', 'metaKey_kz'
            ], 'string'],
            [[
                'text', 'text_en', 'text_kz',
                'metaName', 'metaName_en', 'metaName_kz'
            ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'text'          => 'Заголовок',
            'text_en'       => 'Заголовок на английском',
            'text_kz'       => 'Заголовок на казахском',
            'status'        => 'Статус',
            'metaName'      => 'Мета Названия',
            'metaName_en'   => 'Мета Названия на английском',
            'metaName_kz'   => 'Мета Названия на казахском',
            'metaDesc'      => 'Мета Описание',
            'metaDesc_en'   => 'Мета Описание на английском',
            'metaDesc_kz'   => 'Мета Описание на казахском',
            'metaKey'       => 'Ключевые слова',
            'metaKey_en'    => 'Ключевые слова на английском',
            'metaKey_kz'    => 'Ключевые слова на казахском',
        ];
    }

}
