<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requisites".
 *
 * @property int $id
 * @property string $file
 */
class Requisites extends \yii\db\ActiveRecord
{
    public $path = 'uploads/files/requisites/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'requisites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'file'  => 'Файл',
        ];
    }

    public function getFile()
    {
        return (isset($this->file)) ? '/uploads/files/requisites/' . $this->file : '';
    }
}
