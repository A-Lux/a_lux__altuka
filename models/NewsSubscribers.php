<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_subscribers".
 *
 * @property int $id
 * @property int $news_id
 * @property int $subscriber_id
 * @property int $status
 *
 * @property News $news
 * @property Subscribe $subscriber
 */
class NewsSubscribers extends \yii\db\ActiveRecord
{
    const STATUS_SEND = 1;
    const STATUS_WAIT = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_subscribers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id', 'subscriber_id'], 'required'],
            [['news_id', 'subscriber_id', 'status'], 'integer'],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subscribe::className(), 'targetAttribute' => ['subscriber_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'news_id'       => 'Пользователь',
            'subscriber_id' => 'Рассылка',
            'status'        => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(Subscribe::className(), ['id' => 'subscriber_id']);
    }
}
