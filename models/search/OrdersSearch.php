<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Orders;

class OrdersSearch extends Orders
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'order_id', 'statusPay','statusProgress', 'sum', 'type'], 'integer'],
            [['created', 'fullname', 'email', 'phone', 'address', 'comment'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'order_id' => $this->order_id,
            'statusPay' => $this->statusPay,
            'statusProgress' => $this->statusProgress,
            'sum' => $this->sum,
            'type' => $this->type,
            'created' => $this->created,
        ]);
        $query->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address]);
        $query->orderBy('id DESC');

        return $dataProvider;
    }
}
