<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $url
 * @property string $image
 * @property int $level
 * @property int $sort
 * @property int $status
 * @property string $created_at
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 */

class Catalog extends \yii\db\ActiveRecord
{
    public $path = 'uploads/';

    public static function tableName()
    {
        return 'catalog';
    }

    public function rules()
    {
        return [
            [['name', 'url', 'sort', 'status', 'level'], 'required'],

            [['parent_id', 'sort', 'status', 'level'], 'integer'],

            [['created_at',
                'metaDesc', 'metaDesc_en', 'metaDesc_kz',
                'metaKey', 'metaKey_en', 'metaKey_kz'
            ], 'string'],

            [[
                'name', 'name_en', 'name_kz',
                'url', 'image',
                'metaName', 'metaName_en', 'metaName_kz'
            ], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'parent_id'     => 'Родительский каталог',
            'name'          => 'Название',
            'name_en'       => 'Название на английском',
            'name_kz'       => 'Название на казахском',
            'url'           => 'Url',
            'image'         => 'Изображение',
            'sort'          => 'Сортировка',
            'status'        => 'Статус',
            'created_at'    => 'Дата создания',
            'metaName'      => 'Мета Названия',
            'metaName_en'   => 'Мета Названия на английском',
            'metaName_kz'   => 'Мета Названия на казахском',
            'metaDesc'      => 'Мета Описание',
            'metaDesc_en'   => 'Мета Описание на английском',
            'metaDesc_kz'   => 'Мета Описание на казахском',
            'metaKey'       => 'Ключевые слова',
            'metaKey_en'    => 'Ключевые слова на английском',
            'metaKey_kz'    => 'Ключевые слова на казахском',
        ];
    }

    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }

    public static function slug($string, $replacement = '-', $lowercase = true)
    {
        $parts = explode($replacement, static::transliterate($string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }


    public function upload()
    {
        $time = time();
        $this->image->saveAs($this->path. $time . $this->image->baseName . '.' . $this->image->extension);
        return $time . $this->image->baseName . '.' . $this->image->extension;
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Catalog::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
        }
        return parent::beforeSave($insert);
    }

    public function getParent()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'parent_id']);
    }

    public function getParentName(){
        return (isset($this->parent))? $this->parent->name:'Не задано';
    }

    public function getChilds()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id']);
    }

    public function getChildstop()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id'])->limit(24);
    }

    public function getChildsmore()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id'])->offset(23);
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id']);
    }

    public function getProductsactive()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id'])->where('status = 1');
    }


    public function getAllProducts()
    {
        $id = $this->id;
        $catalogs = Catalog::find()->where('status = 1 AND level=1')->all();
        $result = $this->get_cat($catalogs);
        $result = $this->get_level3($result, $id);

        return $result;
    }

    public function get_cat($catalogs)
    {
        $arr_cat = array();
        if(count($catalogs)) {
            //В цикле формируем массив
            foreach($catalogs as $v) {
                //Формируем массив, где ключами являются адишники на родительские категории
                if(empty($arr_cat[$v['parent_id']])) {
                    $arr_cat[$v['parent_id']] = [];
                }
                $arr_cat[$v['parent_id']][] = $v;
            }
            //возвращаем массив
            return $arr_cat;
        }
    }

    public function get_level3($result, $id)
    {
        $arr = [];
        foreach ($result as $val1){
            foreach ($val1 as $val2) {
                if ($val2->id == $id) {
                    if($val2->childs)
                        foreach ($val2->childs as $val3) {
                            if($val3->childs)
                                foreach ($val3->childs as $val4){
                                    if($val4->childs)
                                        foreach ($val4->childs as $val5){
                                            if($val5->childs)
                                                foreach ($val5->childs as $val6){
                                                    $arr[] = $val6->id;
                                                }
                                            $arr[] = $val5->id;
                                        }
                                    $arr[] = $val4->id;
                                }
                            $arr[] = $val3->id;
                        }
                }
            }

        }

        if($arr)
            $arr = Products::find()->where('category_id in ('.implode(',', $arr).')')->all();

        return $arr;
    }
}
