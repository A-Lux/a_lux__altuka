<?php

namespace app\models;

use Yii;
use app\models\Discounts;

/**
 * This is the model class for table "product".
 *
 * @property int    $id
 * @property int    $category_id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $articul
 * @property string $image
 * @property int    $price
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 * @property int    $status
 * @property int    $size
 * @property int    $url
 * @property int    $isNew
 * @property int    $isDiscount
 * @property int    $isHit
 * @property int    $top_month
 * @property int    $newPrice
 * @property string $metaName
 * @property string $metaName_en
 * @property string $metaName_kz
 * @property string $metaDesc
 * @property string $metaDesc_en
 * @property string $metaDesc_kz
 * @property string $metaKey
 * @property string $metaKey_en
 * @property string $metaKey_kz
 * @property string $created_at
 * @property int    $amount
 * @property int    $sort
 */
class Products extends \yii\db\ActiveRecord
{
    public $count;
    public $path = 'uploads/images/products/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'articul', 'url', 'status', 'price', 'category_id'], 'required'],
            [['status', 'isNew', 'isDiscount', 'isHit', 'top_month', 'category_id'], 'integer'],
            [['created_at'], 'safe'],

            [[
                'content', 'content_en', 'content_kz',
                'metaDesc', 'metaDesc_en', 'metaDesc_kz',
                'metaKey', 'metaKey_en', 'metaKey_kz'
            ], 'string'],

            [[
                'name', 'name_en', 'name_kz',
                'articul', 'url', 'size',
                'metaName', 'metaName_en', 'metaName_kz'
            ], 'string', 'max' => 255],

            [['price', 'newPrice', 'amount'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Название',
            'name_en'       => 'Название на английском',
            'name_kz'       => 'Название на казахском',
            'articul'       => 'Модель',
            'image'         => 'Изображение',
            'price'         => 'Цена',
            'content'       => 'Описание',
            'content_en'    => 'Описание на английском',
            'content_kz'    => 'Описание на казахском',
            'status'        => 'Статус',
            'url'           => 'URL',
            'size'          => 'Размер продукта',
            'isNew'         => 'Новая',
            'isDiscount'    => 'Скидка',
            'isHit'         => 'Популярная',
            'top_month'     => 'Топ месяца',
            'newPrice'      => 'Новая цена',
            'category_id'   => 'Категория',
            'metaName'      => 'Мета Названия',
            'metaName_en'   => 'Мета Названия на английском',
            'metaName_kz'   => 'Мета Названия на казахском',
            'metaDesc'      => 'Мета Описание',
            'metaDesc_en'   => 'Мета Описание на английском',
            'metaDesc_kz'   => 'Мета Описание на казахском',
            'metaKey'       => 'Ключевые слова',
            'metaKey_en'    => 'Ключевые слова на английском',
            'metaKey_kz'    => 'Ключевые слова на казахском',
            'created_at'    => 'Дата создания',
            'amount'        => 'Количество',
        ];
    }

    public function generateCyrillicToLatin($string, $replacement = '-', $lowercase = true)
    {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya'
        ];
        $parts = explode($replacement, str_replace($cyr,$lat, $string));

        $replaced = array_map(function ($element) use ($replacement) {
            $element = preg_replace('/[^a-zA-Z0-9=\s—–-]+/u', '', $element);
            return preg_replace('/[=\s—–-]+/u', $replacement, $element);
        }, $parts);

        $string = trim(implode($replacement, $replaced), $replacement);

        return $lowercase ? strtolower($string) : $string;
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $model = Products::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at = date("Y-m-d H:i:s", time());
            $this->isDiscount = 1;
        }
        return parent::beforeSave($insert);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/products/' . $this->image : '/no-image.png';
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'name');
    }


    public function getCategory()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'category_id']);
    }

    public function getCatalog()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'category_id']);
    }

    public function getCategoryName(){
        return (isset($this->category))? $this->category->name:'Не задан';
    }

    public function getRecommend()
    {
        return $this->hasOne(Recommend::className(), ['product1' => 'id']);
    }


    public static function getSum()
    {
        $sum = 0;
        $count = 0;
        $discount = Discount::find()->orderBy('id ASC')->one();

        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                $count += $v->count;
                $price = $v->price;

                $sum+=(int)$v->count*(int)$price;
            }

            if($count >= $discount->count){
                $sum = (int)$sum - (int)(($sum * $discount->discount)/100);
            }
        }

        return $sum;
    }


    public static function getSumProduct($id)
    {
        $sum = 0;

        if($_SESSION['basket'] != null){
            foreach ($_SESSION['basket'] as $v){
                if($v->id == $id){
                    $price = $v->price;

                    $sum+=(int)$v->count*(int)$price;
                }
            }
        }
        return $sum;
    }

    public static function getSumOwn()
    {
        $sum = 0;
        $count = 0;
        $discount = Discount::find()->orderBy('id ASC')->one();

        if($_SESSION['basket-product-own'] != null){
            foreach ($_SESSION['basket-product-own'] as $v){
                $count += $v->count;
                $price = $v->product->price;

                $sum+=(int)$v->count*(int)$price;
            }

            if($count >= $discount->count){
                $sum = (int)$sum - (int)(($sum * $discount->discount)/100);
            }
        }

        return $sum;
    }


    public static function getSumProductOwn($id)
    {
        $sum = 0;

        if($_SESSION['basket-product-own'] != null){
            foreach ($_SESSION['basket-product-own'] as $v){
                if($v->id == $id){
                    $price = $v->product->price;

                    $sum+=(int)$v->count*(int)$price;
                }
            }
        }
        return $sum;
    }

    public function getConversion($price)
    {
        $currency = Currency::find()->all();

        $array = [];

        foreach($currency as $item){

            $conversion     = $price / $item->value;
            $key            = $item->name;
            $conversion     = floor($conversion * 100)/100;

            if($item->code !== "KZT") {
                $array[] = $key . ' '.$conversion;
            }

        }

        $text = implode(',  ', $array);

       return $text;
    }

    public function getConversionBasket($price)
    {
        $currency = Currency::find()->all();
        $array = [];

        foreach($currency as $item){

            $conversion     = $price / $item->value;
            $key            = $item->name;
            $conversion     = floor($conversion * 100)/100;

            if($item->code !== "KZT") {
                $array[] = '' . $key . ' '.$conversion;
            }

        }

        $text = implode('<br> ', $array);

        return $text;
    }

    public function lastCatalog($catalog)
    {
        if(!empty($catalog->parent)) {
            return $this->lastCatalog($catalog->parent);
        } else {
            return $catalog->name;
        }
    }

}
