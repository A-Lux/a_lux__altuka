<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aboutImages".
 *
 * @property int $id
 * @property int $about_id
 * @property string $image
 */
class AboutImages extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/aboutImages/';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aboutImages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['about_id', 'image'], 'required'],
            [['about_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'about_id' => 'О компании',
            'image' => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/aboutImages/' . $this->image : '/no-image.png';
    }

    public function getAbout()
    {
        return $this->hasOne(About::className(), ['id' => 'about_id']);
    }

    public function getAboutName(){
        return (isset($this->about))? $this->about->title:'Не задан';
    }
}
