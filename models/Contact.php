<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $address
 * @property string $address_en
 * @property string $address_kz
 * @property string $email
 * @property string $work_time
 * @property string $work_time_en
 * @property string $work_time_kz
 * @property string $facebook
 * @property string $instagram
 * @property string $whatsap
 * @property string $youtube
 * @property string $vk
 * @property string $longitude
 * @property string $latitude
 * @property string $iframe
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'email', 'work_time', 'facebook', 'instagram', 'whatsap', 'youtube', 'vk'], 'required'],
            [['address', 'address_en', 'address_kz', 'iframe'], 'string'],
            [['work_time', 'work_time_en', 'work_time_kz',
                'facebook', 'instagram', 'whatsap', 'youtube', 'vk',
                'longitude', 'latitude'], 'string', 'max' => 255],

            ['email','email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'address'       => 'Адрес',
            'address_en'    => 'Адрес на английском',
            'address_kz'    => 'Адрес на казахском',
            'email'         => 'E-mail',
            'work_time'     => 'График работы',
            'work_time_en'  => 'График работы на английском',
            'work_time_kz'  => 'График работы на казахском',
            'facebook'      => 'Facebook',
            'instagram'     => 'Instagram',
            'whatsap'       => 'Whatsapp',
            'youtube'       => 'Youtube',
            'vk'            => 'VK',
            'longitude'     => 'долгота',
            'latitude'      => 'широта',
            'iframe'        => 'карта',
        ];
    }
}
