<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "source_message".
 *
 * @property int $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'category'  => 'Категория',
            'message'   => 'Текст',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['id' => 'id'])->indexBy('language');
    }

    /**
     * Populating messages to new sourceMessage
     */
    public function initMessages()
    {
        $messages = [];

        foreach (i18n::locales() as $locale){
            if(!isset($this->messages[$locale])) {
                $message            = new Message();
                $message->id        = $this->id;
                $message->language  = $locale;
                $messages[$locale]  = $message;
            } else {
                $messages[$locale]  = $this->messages[$locale];
            }
        }

        $this->populateRelation('messages', $messages);
    }

    /**
     * Linking related records
     */

    public function saveMessage()
    {
        foreach ($this->messages as $message) {
            $this->link('messages', $message);
            $message->save();
        }
    }
}
