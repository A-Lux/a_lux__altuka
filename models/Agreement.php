<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agreement".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $content
 * @property string $content_en
 * @property string $content_kz
 */
class Agreement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['content', 'content_en', 'content_kz'], 'string'],
            [['title', 'title_en', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'title_en'      => 'Заголовок на английском',
            'title_kz'      => 'Заголовок на казахском',
            'content'       => 'Описание',
            'content_en'    => 'Описание на английском',
            'content_kz'    => 'Описание на казахском',
        ];
    }
}
