<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_your".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property string $image
 * @property string $date
 */
class ProductYour extends \yii\db\ActiveRecord
{
    public $path = "uploads/images/product-your/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_your';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id'], 'required'],
            [['user_id', 'product_id'], 'integer'],
            [['date'], 'safe'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'user_id'       => 'Пользователь',
            'product_id'    => 'Продукт',
            'image'         => 'Изображение',
            'date'          => 'Дата создания',
        ];
    }

    public function getImage()
    {
        return isset($this->image) ? '/uploads/images/product-your/' . $this->image : '';
    }
}
