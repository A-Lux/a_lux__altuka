<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $name
 * @property string $telephone
 * @property string $email
 * @property string $comment
 * @property int $rating
 * @property int $status
 * @property string $date
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'comment', 'rating'], 'required'],
            [['comment'], 'string'],
            [['rating', 'status'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            ['email','email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'comment' => 'Отзыв',
            'rating' => 'Рейтинг',
            'status' => 'Статус',
            'date' => 'Дата создание',
        ];
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date, 'long');
    }

    public static function getAll($pageSize=3)
    {
        $query =  Feedback::find()->where('status=1')->orderBy('date DESC');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize, 'pageParam' => 'feedback','pageSizeParam' => false, 'forcePageParam' => false]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
