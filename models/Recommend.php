<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "recommend".
 *
 * @property int $id
 * @property int $product1
 * @property int $product2

 */

class Recommend extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'recommend';
    }

    public function rules()
    {
        return [
            [['product1', 'product2'], 'required'],
            [['product1', 'product2'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product1' => 'Product1',
            'product2' => 'Product2',
        ];
    }
}
