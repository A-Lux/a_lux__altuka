<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $value
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name', 'value'], 'required'],
            [['code', 'name'], 'string', 'max' => 128],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'code'      => 'Уникальное значение',
            'name'      => 'Наименование',
            'value'     => 'Валюта в тг',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Currency::find()->all(),'code','value');
    }
}
