<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * UserProfile model
 *
 * @property integer $id
 * @property string $user_id
 * @property string $name
 * @property string $surname
 * @property string $father
 * @property string $phone
 * @property string $date_of_birth

 */

class UserProfile extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_profile';
    }

    public function rules()
    {
        return [
            [['name', 'surname'], 'required'],
            [['user_id'], 'integer'],
            [['date_of_birth'], 'safe'],
            [['name', 'surname', 'father', 'phone'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'father' => 'Отчество',
            'phone' => 'Телефон',
            'date_of_birth' => 'День рождения',
        ];
    }

    public function fields()
    {
        return [
            'name'
        ];
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date_of_birth, 'long');
    }

    public function getFio(){
        return $this->name.' '.$this->surname;
    }
}
