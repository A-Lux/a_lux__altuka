<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property string $image
 */
class Partners extends \yii\db\ActiveRecord
{

    public $path = 'uploads/images/partners/';

    public static function tableName()
    {
        return 'partners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
        ];
    }


    public function getImage()
    {
        return ($this->image) ? '/uploads/images/partners/' . $this->image : '/no-image.png';
    }
}
