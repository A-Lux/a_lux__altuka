<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productsImage".
 *
 * @property int $id
 * @property int $product_id
 * @property string $image
 */
class ProductsImage extends \yii\db\ActiveRecord
{

    public $path = 'uploads/images/products/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productsImage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id'], 'integer'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Продукт',
            'image' => 'Изображение',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/products/' . $this->image : '/no-image.png';
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getProductName(){
        return (isset($this->product))? $this->product->name:'Не задан';
    }
}
