<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner_advantages".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 */
class BannerAdvantages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner_advantages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'name_en', 'name_kz'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Название',
            'name_en'   => 'Название на английском',
            'name_kz'   => 'Название на казахском',
            'image'     => 'Картинка',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }
}
