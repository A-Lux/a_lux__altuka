<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;


class UserAddress extends ActiveRecord
{

    public static function tableName()
    {
        return 'user_address';
    }

    public function rules()
    {
        return [
            [['address'], 'required'],
            [['user_id'], 'integer'],
            [['address'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'address' => 'Адрес',
        ];
    }
}
