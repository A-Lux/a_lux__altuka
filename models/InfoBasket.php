<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "infoBasket".
 *
 * @property int $id
 * @property string $title
 * @property string $title_en
 * @property string $title_kz
 * @property string $text
 * @property string $text_en
 * @property string $text_kz
 * @property int $status
 */
class InfoBasket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'infoBasket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['text', 'text_en', 'text_kz'], 'string'],
            [['status'], 'integer'],
            [['title', 'title_en', 'title_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'title'     => 'Заголовок',
            'title_en'  => 'Заголовок на английском',
            'title_kz'  => 'Заголовок на казахском',
            'text'      => 'Содержание',
            'text_en'   => 'Содержание на английском',
            'text_kz'   => 'Содержание на казахском',
            'status'    => 'Статус',
        ];
    }
}
