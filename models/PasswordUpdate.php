<?php
namespace app\models;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Password update
 */
class PasswordUpdate extends Model
{
    public $old_password;
    public $password;
    public $password_verify;

    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['old_password', 'password', 'password_verify'], 'required'],
            ['old_password', 'validatePassword'],


            [['old_password', 'password', 'password_verify'], 'string', 'min' => 6],

            [['password_verify'], 'compare', 'compareAttribute' => 'password', 'message'=>"Пароли не совпадают!",],



        ];
    }

    public function attributeLabels()
    {
        return [
            'old_password' => 'Старый пароль',
            'password' => 'Новый пароль',
            'password_verify' => 'Подтверждение нового пароля',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user->validatePassword($this->old_password)) {
                $this->addError($attribute, 'Старый пароль не соответсвует.');
            }
        }
    }

    public function signUp()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = User::findOne(Yii::$app->user->id);
        if($this->password != null) {
            $user->setPassword($this->password);
        }
        return $user->save(false);
    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(Yii::$app->user->id);
        }

        return $this->_user;
    }
}
