<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "orderedproduct".
 *
 * @property int $id
 * @property int $order_id
 * @property int $own_id
 * @property int $product_id
 * @property int $count
 */

class Orderedproduct extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orderedproduct';
    }

    public function rules()
    {
        return [
            [['order_id', 'product_id', 'count'], 'required'],
            [['order_id', 'product_id', 'count', 'own_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'order_id'      => 'Order ID',
            'own_id'        => 'Индивидуальный заказ',
            'product_id'    => 'Product ID',
            'count'         => 'Count',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getOwnProduct()
    {
        return $this->hasOne(ProductOwn::className(), ['id' => 'own_id']);
    }
}
