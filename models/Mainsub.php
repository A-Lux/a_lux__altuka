<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mainsub".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 */
class MainSub extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mainsub';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'name_en', 'name_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Название',
            'name_en'       => 'Название на английском',
            'name_kz'       => 'Название на казахском',
        ];
    }

}
