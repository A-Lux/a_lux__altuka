<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_own".
 *
 * @property int $id
 * @property int $product_id
 * @property int $user_id
 * @property string $image
 * @property string $date
 *
 * @property Products $product
 * @property User $user
 */
class ProductOwn extends \yii\db\ActiveRecord
{
    public $path = "uploads/images/product-own/";

    public $agreement;

    public $agreement_2;

    public $count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_own';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id'], 'required'],
            [['product_id', 'user_id'], 'integer'],
            [['date'], 'safe'],
            [['image'], 'file', 'extensions' => ['png, jpg, jpeg, bmp, gif']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'product_id'    => 'Продукт',
            'user_id'       => 'Пользователь',
            'image'         => 'Изображение',
            'date'          => 'Дата создание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/product-own/' . $this->image : '';
    }

    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    public function getImagePathName()
    {
        $path = \Yii::$app->basePath . '/web/uploads/images/product-own/';

        return ($this->image) ? $path . $this->image : '';
    }
}
