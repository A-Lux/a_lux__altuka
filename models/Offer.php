<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "offer".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $file
 */
class Offer extends \yii\db\ActiveRecord
{
    public $path = "uploads/files/offer/";

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'name_en', 'name_kz', 'file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Название',
            'name_en'   => 'Название на английском',
            'name_kz'   => 'Название казахском',
            'file'      => 'Файл',
        ];
    }

    public function getName()
    {
        $name = "name".Yii::$app->session["lang"];

        return $this->$name;
    }

    public function getFile()
    {
        return (isset($this->file)) ? '/uploads/files/offer/' . $this->file : '';
    }
}
