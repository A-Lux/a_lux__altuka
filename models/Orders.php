<?php
namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $order_id
 * @property int $order_own
 * @property int $user_id
 * @property string $fullname
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $comment
 * @property int $statusPay
 * @property int $statusProgress
 * @property int $sum
 * @property int $type
 * @property int $created
 */

class Orders extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return [
            [['order_id', 'sum', 'fullname', 'email', 'phone', 'address'], 'required'],
            [['user_id', 'order_id', 'statusPay','statusProgress', 'sum', 'type', 'order_own'], 'integer'],
            [['address', 'comment'], 'string'],
            [['fullname', 'email', 'phone'], 'string', 'max' => 255],
            [['created'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'order_id' => 'Номер заказа',
            'order_own' => 'Индивидуальный заказа',
            'fullname' => 'ФИО',
            'email' => 'Электронная почта',
            'phone' => 'Номер телефона',
            'address' => 'Адрес доставки',
            'comment' => 'Комментарий к заказу',
            'statusProgress' => 'Статус подготовки',
            'statusPay' => 'Статус оплаты',
            'sum' => 'Сумма заказа',
            'type' => 'Тип заказа',
            'created' => 'Дата заказа',
        ];
    }

    public static function getAll($pageSize=6)
    {
        $query =  Orders::find()->where(['user_id'=>Yii::$app->user->id])->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $orders = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $orders;
        $data['pagination'] = $pagination;

        return $data;
    }

    public function getUser()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Orderedproduct::className(), ['order_id' => 'order_id']);
    }

    public function getCount()
    {
        $query =  Orders::find()->where(['user_id'=>Yii::$app->user->id]);
        $count = $query->count();

        return $count;
    }

    public static function getStatusPay($status)
    {
        if($status == 1){
            $response = "Заказ оплачен";
        }elseif($status == 2){
            $response = "Заказ будет оплачено через реквизиты";
        }else{
            $response = "Заказ не оплачен";
        }

        return $response;
    }


    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->created, 'long');
    }

    public function getOrderOwn()
    {
        return $this->hasOne(ProductOwn::className(), ['id' => 'order_own']);
    }


    public function getConversion()
    {
        $currency = Currency::find()->all();

        $array = [];

        foreach($currency as $item){

            $conversion     = $this->sum / $item->value;
            $key            = $item->name;
            $conversion     = floor($conversion * 100)/100;

            if($item->code !== "KZT") {
                $array[] = $key . ' '.$conversion;
            }

        }

        $text = implode(',  ', $array);

        return $text;
    }


}
