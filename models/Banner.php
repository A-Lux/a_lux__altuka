<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $image
 */
class Banner extends \yii\db\ActiveRecord
{
    public $path = 'uploads/images/banner/';

    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'name_en', 'name_kz'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => 'Название',
            'name_en'   => 'Название на английском',
            'name_kz'   => 'Название на казахском',
            'image'     => 'Картинка',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/banner/' . $this->image : '/no-image.png';
    }
}
