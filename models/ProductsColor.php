<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productsColor".
 *
 * @property int    $id
 * @property int    $product_id
 * @property string $name
 * @property string $name_en
 * @property string $name_kz
 * @property string $color
 */
class ProductsColor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productsColor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'name', 'color'], 'required'],
            [['product_id'], 'integer'],
            [['name', 'name_en', 'name_kz', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'product_id'    => 'Продукт',
            'name'          => 'Название',
            'name_en'       => 'Название на английском',
            'name_kz'       => 'Название на казахском',
            'color'         => 'Цвет',
        ];
    }

    public function getProducts()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public function getProductsName()
    {
        return (isset($this->products)) ? $this->products->name : 'Не задан';
    }
}
