<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/params_mailer.php';

$config = [
    'id' => 'basic',
    'sourceLanguage' => 'ru',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'queue'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '8FTHIDwCElvGbo2tZ4zipwV41uDJZLDE',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
         'mailer' => $mailer,
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db', // Компонент подключения к БД или его конфиг
            'tableName' => '{{%queue}}', // Имя таблицы
            'channel' => 'default', // Выбранный для очереди канал
            'mutex' => \yii\mutex\MysqlMutex::class, // Мьютекс для синхронизации запросов
        ],

        'i18n' => [
            'translations' =>[
                'main*' => [
                    'class'                 => \yii\i18n\DbMessageSource::className(),
                    'sourceLanguage'        => 'ru',
                    'sourceMessageTable'    => '{{%source_message}}',
                    'messageTable'          => '{{%message}}',
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'catalog/<url1:\w+>/<url:\w+>' => 'catalog/header-category',
                'catalog/products/<url:[\w_\/-]+>' => 'catalog/product'
            ],
        ],

        'formatter' => [
            'timeZone' => 'Asia/Almaty',
            'dateFormat' => 'dd MMMM', //Date format to used here
            'datetimeFormat' => 'php:d-m-Y H:i:s',
            'timeFormat' => 'php:h:i:s A',
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
//            'language' => 'ru-RU',
            'class' => 'yii\i18n\Formatter',
        ],
        'sync' => [
            'class' => 'app\components\Sync',
        ],

    ],

    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
//         'alfabank' => [
//             'class' => pantera\yii2\pay\alfabank\Module::class,
//             'components' => [
//                 'alfabank' => [
//                     'class' => pantera\yii2\pay\alfabank\components\Alfabank::class,

//                     // время жизни инвойса в секундах (по умолчанию 20 минут - см. документацию Альфабанка)
//                     // в этом примере мы ставим время 1 неделю, т.е. в течение этого времени покупатель может
//                     // произвести оплату по выданной ему ссылке
//                     'sessionTimeoutSecs' => 60 * 60 * 24 * 7,

//                     // логин api мерчанта
//                     'login' => 'altuka_kz-api',

//                     // пароль api мерчанта
//                     'password' => 'altuka_kz*?1',
//                 ],
//             ],

//             // страница вашего сайта с информацией об успешной оплате
//             'successUrl' => '/pay/success',

//             // страница вашего сайта с информацией о НЕуспешной оплате
//             'failUrl' => '/pay/fail',

//             // обработчик, вызываемый по факту успешной оплаты
//             'successCallback' => function($invoice){
//                 // какая-то ваша логика, например
//                 $order = \app\models\Orders::findOne($invoice->order_id);
//                 $client = $order->getUser();
// //                $client->sendEmail('Зачислена оплата по вашему заказу №' . $order->id);
//                 // .. и т.д.
//             }
//         ],
    ],


    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
