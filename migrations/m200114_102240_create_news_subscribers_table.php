<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%news_subscribers}}`.
 */
class m200114_102240_create_news_subscribers_table extends Migration
{
    public $table                   = "news_subscribers";
    public $news                    = "news";
    public $subscribers             = "subscribe";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'news_id'       => $this->integer()->notNull(),
            'subscriber_id' => $this->integer()->notNull(),
            'status'        => $this->smallInteger()->notNull()->defaultValue(0),
        ], $tableOptions);


        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->news}", "{{{$this->table}}}", 'news_id', "{{{$this->news}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->subscribers}", "{{{$this->table}}}", 'subscriber_id', "{{{$this->subscribers}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->news}", "{{{$this->table}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->subscribers}", "{{{$this->table}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
