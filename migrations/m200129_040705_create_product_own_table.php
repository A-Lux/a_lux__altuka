<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_own}}`.
 */
class m200129_040705_create_product_own_table extends Migration
{
    public $table           = "product_own";
    public $product         = "products";
    public $user            = "user";
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'product_id'        => $this->integer()->notNull(),
            'user_id'           => $this->integer()->notNull(),
            'image'             => $this->string(255)->null(),
            'date'              => $this->date()->defaultValue(Yii::$app->formatter->asDate(time(), 'Y-MM-dd H:i'))->null(),
        ], $tableOptions);


        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->product}", "{{{$this->table}}}", 'product_id', "{{{$this->product}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->user}", "{{{$this->table}}}", 'user_id', "{{{$this->user}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->product}", "{{{$this->table}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->user}", "{{{$this->table}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
