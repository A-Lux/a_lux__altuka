$('body').on('click', '.category_filter', function (e) {

    var id = $(this).attr('data-id');
    localStorage.setItem('anchor', $(this).attr('href'));
    $.ajax({
        type: 'GET',
        url: "/catalog/category",
        data: {id:id},
        success: function(html){
            $('.category_id').val(id);
            $('.tablet-version-hide.items').html(html);

        }
    });
});

$('body').on('click', '.categoryBtn',function(e){
    var id = $(this).attr('data-id');
    e.preventDefault();
    $.ajax({
        url: '/catalog/category',
        data: {id:id},
        type: 'GET',
        success: function (html) {
            $('.tablet-version-hide.items').html(html);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.');
        }
    });
});
