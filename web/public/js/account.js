

$('input[name="UserProfile[name]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});

$('input[name="UserProfile[surname]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});


$('input[name="UserProfile[father]"]').keyup(function(e) {
    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
    if (regex.test(this.value) !== true)
        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
});



$("body").on('click', '.register-button', function (e) {
    e.preventDefault();
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    $.ajax({
        type: 'POST',
        url: '/account/register',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                window.location.href = "/account";
            }else{
                swal(response);
            }
        },
        error: function () {
            swal(error_1, error_2);
        }
    });
});

$('body').on('click', '.login-button', function (e) {
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    $.ajax({
        type: 'POST',
        url: '/account/login',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                window.location.href = "/account";
            }else{
                swal(response);
            }
        },
        error: function () {
            swal(error_1, error_2);
        }
    });
});


$('body').on('click', '.btn-subscribe', function (e) {
    var success = $(this).attr('data-message-success');
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    $.ajax({
        type: 'POST',
        url: '/site/subscribe',
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if(data == 1) {
                swal(success);
                setTimeout(function () {
                    window.location.href = "/";
                }, 2000);
            }else{
                swal(data);
            }
        },
        error: function () {
            swal(error_1, error_2);
        }
    });
});


$('body').on('click', '.update-profile-button', function (e) {
    var success_1 = $(this).attr('data-message-success-1');
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    var error_3 = $(this).attr('data-message-error-3');
    $.ajax({
        type: 'POST',
        url: '/account/update-account-data',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){

                swal(success_1);
                setTimeout(function () {
                    window.location.href = "/account";
                }, 3000);
            }else{
                swal(error_1, response);
            }
        },
        error: function () {
            swal(error_2, error_3);
        }
    });
});

$('body').on('click', '.update-password-button', function (e) {
    var success_1 = $(this).attr('data-message-success-1');
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    var error_3 = $(this).attr('data-message-error-3');
    $.ajax({
        type: 'GET',
        url: '/account/update-password',
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if(data == 1){
                swal(success_1);
                setTimeout(function () {
                    window.location.href = "/account";
                }, 2000);
            }else{
                swal(error_1, data);
            }
        },
        error: function () {
            swal(error_2, error_3);
        }
    });
});


$('body').on('click', ".sendForgotPassword", function () {
    var success_1 = $(this).attr('data-message-success-1');
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    var error_3 = $(this).attr('data-message-error-3');
    $.ajax({
        type: "GET",
        url: "/account/forgot-password",
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if (data == 1) {
                swal(success_1);
                setTimeout(function () {

                    window.location.href = "/";
                }, 2500);
            } else {
                swal(error_1, data);
            }
        },
        error: function () {
            swal(error_2, error_3);
        }
    });

});



$(".btn-add-favorite").click(function () {
    var product_id = $(this).attr('data-id');
    var user_id = $(this).attr('data-user-id');

    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    var error_3 = $(this).attr('data-message-error-3');
    var error_4 = $(this).attr('data-message-error-4');

    if(user_id == ""){
        swal(error_1, error_2);
    }else{
        $.ajax({
            url: "/account/add-to-favorite",
            dataType: "json",
            data: {product_id:product_id,user_id:user_id},
            type: "GET",
            success: function(data){
                swal(data.text);
            },
            error: function () {
                swal(error_3, error_4);
            }
        });
    }
});



$(".btn-delete-product-from-favorite").click(function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/account/delete-from-favorite",
        data: {id:id},
        type: "GET",
        success: function(data){
            if(data == 0){
                swal('Упс!', 'Что-то пошло не так.');
            }else{
                $('#v-pills-profileCabinet').html(data);
            }
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.');
        }
    });
});
