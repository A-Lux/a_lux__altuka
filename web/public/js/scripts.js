$('body').on('click', ".sendReviews", function (e) {

    $(".raiting input:radio").attr("checked", false);

    $('.raiting input').click(function () {
        $(".raiting span").removeClass('checked');
        $(this).parent().addClass('checked');
    });

    var success_1 = $(this).attr('data-message-success-1');

    $.ajax({
        type: "GET",
        url: "/site/comment",
        data: $(this).closest('form').serialize(),
        success: function (data) {
            if (data == 1) {
                swal(success_1);
                setTimeout(function () {
                    window.location.href = "/";
                }, 5000);
            } else {
                swal(data);
            }
        },
        error: function () {
            swal('Error', 'error');
        }
    });

});
