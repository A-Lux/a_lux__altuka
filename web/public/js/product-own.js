$('body').on('click', '.btn-to-basket-product-own', function () {
    var success_1 = $(this).attr('data-message-success-1');
    var success_2 = $(this).attr('data-message-success-2');
    var success_3 = $(this).attr('data-message-success-3');
    var success_4 = $(this).attr('data-message-success-4');
    var error_2 = $(this).attr('data-message-error-2');
    var error_3 = $(this).attr('data-message-error-3');

    var id = $(this).attr('data-id');
    $.ajax({
        url: '/product-own/add-product-own',
        type: 'GET',
        dataType: 'json',
        data: { id: id },
        success: function (data) {
            if (data.status == 1) {
                swal(success_1, {
                    buttons: {
                        cancel: success_2,
                        catch: {
                            text: success_3,
                            value: 'catch',
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {

                            case "catch":
                                window.location.href = "/card/";
                                break;

                        }
                    });
            } else if (data.status == 2) {
                swal(success_4, {
                    buttons: {
                        cancel: success_2,
                        catch: {
                            text: success_3,
                            value: "catch",
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {

                            case "catch":
                                window.location.href = "/card/";
                                break;

                        }
                    });
            }
        },
        error: function () {
            swal(error_2, error_3);
        }
    });
});

$('.plus-product-own').click(function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: '/product-own/up-clicked-own',
        type: 'GET',
        dataType: 'json',
        data: { id: id },
        success: function (data) {
            $('#countProduct' + id).val(data.countProduct);
            $('#sumBasket').html('KZT ' + data.sum);
            $('#sumPay').val(data.sum);

        },
        error: function () {

        }
    });
});

$('.minus-product-own').click(function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: '/product-own/down-clicked-own',
        type: 'GET',
        dataType: 'json',
        data: { id: id },
        success: function (data) {
            $('#countProduct' + id).val(data.countProduct);
            $('#sumBasket').html('KZT ' + data.sum);
            $('#sumPay').val(data.sum);
        },
        error: function () {

        }
    });
});

$('.quantity-own').on('keyup', function () {
    var id = $(this).attr('data-id');
    var v = $(this).val();
    $.ajax({
        url: '/product-own/count-changed-own',
        type: 'GET',
        dataType: 'json',
        data: { id: id, v: v },
        success: function (data) {
            $('#countProduct' + id).html(data.countProduct);
            $('#sumBasket').html('KZT ' + data.sum);
            $('#sumPay').val(data.sum);
        },
        error: function () {
        }
    });
});

$(".btn-delete-product-own-from-basket").click(function () {
    var id = $(this).attr('data-id');

    var success_1 = $(this).attr('data-message-success-1');
    var success_2 = $(this).attr('data-message-success-2');
    var success_3 = $(this).attr('data-message-success-3');

    swal({
        title: success_1,
        text: success_2,
        icon: "warning",
        buttons: [success_3, "OK"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = "/product-own/delete-product-own?id=" + id;
            } else {

            }
        });
});

document.querySelectorAll('.nav-link').forEach(link => {
    if (link.hash == window.location.hash) {
        link.click();
    }
})
