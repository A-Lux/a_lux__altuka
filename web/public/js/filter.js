$('body').on('change', '.filter', function () {

    var select = $(this).val();
    var subjects = $('.subjects').val();

    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(),
            select:select, subjects:subjects, category_id: $('.category_id').val()},
        success: function(html){
            $('.tablet-version-hide.items').html(html);
        }
    });
});

$('body').on('change', '.subjects', function () {

    var subjects = $(this).val();
    var select = $('.filter').val();

    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {subjects:subjects, select:select, category_id: $('.category_id').val()},
        success: function(html){
            $('.tablet-version-hide.items').html(html);
        }
    });
});

$('body').on('keyup', '.from_price', function () {

    var select = $('.filter').val();
    var subjects = $('.subjects').val();
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            subjects:subjects, select:select,},
        success: function(html){
            $('.tablet-version-hide.items').html(html);
        }
    });
});

$('body').on('keyup', '.to_price', function () {

    var select = $('.filter').val();
    var subjects = $('.subjects').val();
    $.ajax({
        type: 'GET',
        url: "/catalog/filter",
        data: {from: $('.from_price').val(), to: $('.to_price').val(), category_id: $('.category_id').val(),
            subjects:subjects, select:select,},
        success: function(html){
            $('.tablet-version-hide.items').html(html);
        }
    });
});

