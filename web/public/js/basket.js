$("body").on('click', '.btn-to-basket', function () {
    var success_1 = $(this).attr('data-message-success-1');
    var success_2 = $(this).attr('data-message-success-2');
    var success_3 = $(this).attr('data-message-success-3');
    var success_4 = $(this).attr('data-message-success-4');
    var error_2 = $(this).attr('data-message-error-2');
    var error_3 = $(this).attr('data-message-error-3');

    var id = $(this).attr('data-id');
    var amount = $('.count' + id).val();
    $.ajax({
        url: "/card/add-product",
        type: "GET",
        dataType: "json",
        data: { id: id, amount: amount },
        success: function (data) {
            if (data.status == 1) {
                $('.count').html(data.count);
                swal(success_1, {
                    buttons: {
                        cancel: success_2,
                        catch: {
                            text: success_3,
                            value: "catch",
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {

                            case "catch":
                                window.location.href = "/card/";
                                break;

                        }
                    });
            } else if (data.status == 2) {
                swal(success_4, {
                    buttons: {
                        cancel: success_2,
                        catch: {
                            text: success_3,
                            value: "catch",
                        },
                    },
                })
                    .then((value) => {
                        switch (value) {

                            case "catch":
                                window.location.href = "/card/";
                                break;

                        }
                    });
            }
        },
        error: function () {
            swal(error_2, error_3);
        }
    });
});

$(".order-basket").click(function () {
    var sum = $(this).attr('data-sum');

    if (sum > 5000) {
        //$('#order-basket').show();
        $('#order-basket').animate({ height: 'show' });
    }
    console.log(sum);
});


$(".btn-delete-product-from-basket").click(function () {
    var id = $(this).attr('data-id');

    var success_1 = $(this).attr('data-message-success-1');
    var success_2 = $(this).attr('data-message-success-2');
    var success_3 = $(this).attr('data-message-success-3');

    swal({
        title: success_1,
        text: success_2,
        icon: "warning",
        buttons: [success_3, "OK"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = "/card/delete-product?id=" + id;
            } else {

            }
        });
});

$(".btn-delete-all-from-basket").click(function () {
    var success_1 = $(this).attr('data-message-success-1');
    var success_2 = $(this).attr('data-message-success-2');
    var success_3 = $(this).attr('data-message-success-3');

    swal({
        title: success_1,
        text: success_2,
        icon: "warning",
        buttons: [success_3, "OK"],
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                window.location.href = "/card/delete-all";
            } else {

            }
        });
});


$(".minus-product").click(function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/card/down-clicked",
        type: "GET",
        dataType: "json",
        data: { id: id },
        success: function (data) {
            $('#countProduct' + id).val(data.countProduct);
            $('#sumBasket').html('KZT ' + data.sum);
            $('#sumPay').val(data.sum);
            $('#sumProduct' + id).html('KZT ' + data.sumProduct);
        },
        error: function () {
            // alert('FAILED');

        }
    });
});


$(".plus-product").click(function () {
    var id = $(this).attr('data-id');
    $.ajax({
        url: "/card/up-clicked",
        type: "GET",
        dataType: "json",
        data: { id: id },
        success: function (data) {
            $('#countProduct' + id).val(data.countProduct);
            $('#sumBasket').html('KZT ' + data.sum);
            $('#sumPay').val(data.sum);
            $('#sumProduct' + id).html('KZT ' + data.sumProduct);

        },
        error: function () {

        }
    });
});

$(".quantity").on("keyup", function () {
    var id = $(this).attr('data-id');
    var v = $(this).val();
    $.ajax({
        url: "/card/count-changed",
        type: "GET",
        dataType: "json",
        data: { id: id, v: v },
        success: function (data) {
            $('#countProduct' + id).html(data.countProduct);
            $('#sumBasket').html('KZT ' + data.sum);
            $('#sumPay').val(data.sum);
            $('#sumProduct' + id).html('KZT ' + data.sumProduct);
        },
        error: function () {
            // alert('FAILED');
        }
    });
});


$("#order-pay").click(function () {

    var sum = parseInt($('#sumPay').val());
    var minPrice = parseInt($('#min-price').val());

    var success_1 = $(this).attr('data-message-success-1');
    var success_2 = $(this).attr('data-message-success-2');
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    var error_guest = $(this).attr('data-message-error-guest');
    var checked_1 = $(this).attr('data-message-checked-1');
    var checked_2 = $(this).attr('data-message-checked-2');

    var user = $(this).attr('data-user');

    if (user == true) {
        if (sum > minPrice) {
            if ($('#agree').is(':checked')) {
                $.ajax({
                    type: 'POST',
                    url: '/card/order-pay',
                    dataType: 'JSON',
                    data: $(this).closest('form').serialize(),
                    success: function (data) {
                        if (data.status == 1) {
                            $('#paymentModalBtn').click();
                            $('.payment-text .order_id').html(data.order_id);
                        } else if (data.status == 0) {
                            swal(error_1, error_2);
                        }
                    },
                    error: function () {
                        swal(error_1, error_2);
                    }
                });
            } else {
                swal(checked_1, checked_2);
            }
        } else {
            swal(success_1, success_2);
        }
    } else {
        swal(error_guest);
    }

});


$('.online-btn').click(function () {

    var id = parseInt($('.online-payment-order-id').html());
    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');

    $.ajax({
        type: 'GET',
        url: '/card/online-payment',
        dataType: 'JSON',
        data: { id: id },
        success: function (data) {
            if (data.status == 1) {
                window.location.href = '/pay/pay?id=' + id;
            } else if (data.status == 0) {
                swal(error_1, error_2);
            }
        },
        error: function () {
            swal(error_1, error_2);
        }
    });
});

$('.transaction-btn').click(function () {
    var id = parseInt($('.online-payment-order-id').html());

    var error_1 = $(this).attr('data-message-error-1');
    var error_2 = $(this).attr('data-message-error-2');
    $.ajax({
        type: 'GET',
        url: '/card/transaction-payment',
        dataType: 'JSON',
        data: { id: id },
        success: function (data) {
            if (data.status == 1) {
                $('#paymentModalBtn').click();
                $('#transactionPaymentBtn').click();
                $('.transaction-payment .order_id').html('Номер заказа: ' + data.order_id);
                $('.transaction-payment .order_date').html('Дата заказа: ' + data.order_date);
                $('.transaction-payment .order_sum').html('Сумма заказа: ' + data.order_sum);
                $('.transaction-payment .order_fullname').html('Заказчик: ' + data.order_fullname);
                $('.transaction-payment-en .order_id_en').html('Order number: ' + data.order_id);
                $('.transaction-payment-en .order_date_en').html('Order date: ' + data.order_date);
                $('.transaction-payment-en .order_sum_en').html('Order price: ' + data.order_sum);
                $('.transaction-payment-en .order_fullname_en').html('Client: ' + data.order_fullname);
                var product = '';
                for (var item of data.product) {
                    product += '<p>Продукт: ' + item.name + '</p>';
                    product += '<p>Цена: ' + item.price + '</p>';
                    product += '<p>Количество: ' + item.count + '</p><br>';
                    // product += '<p><img src=" '+item.image+'" width="50px" height="50px">'+'</p><br><br>';
                }
                ($('.transaction-payment .products_transaction'))[0].innerHTML += product;
                var product_en = '';
                for (var item_en of data.product) {
                    product_en += '<p>Product: ' + item_en.name_en + '</p>';
                    product_en += '<p>Price: ' + item_en.price + '</p>';
                    product_en += '<p>Amount: ' + item_en.count + '</p><br>';
                    // product_en += '<p><img src=" '+item_en.image+'" width="50px" height="50px">'+'</p><br><br>';
                }
                ($('.transaction-payment-en .products_transaction_en'))[0].innerHTML += product_en;

            } else if (data.status == 0) {
                swal(error_1, error_2);
            }
        },
        error: function () {
            swal(error_1, error_2);
        }
    });
});

$('#props').click(function () {
    $('.fade').removeClass('show');
})

