

$(document).ready(function () {

    $('.product-card__small-img ul li img').click(function(){

        let imgAttr = $(this).attr('src');

        $('.product-card__main-img img').attr('src', imgAttr);
    });

    $('.select-product-heart form p i').click(function(e){
        e.preventDefault();
        $(this).toggleClass('active-select-product-heart');
    });

    $('.plus').click(function(){
        let valInput = $('.quantity_box-input').attr('value');
        var integer = parseInt(valInput, 10);
        let num = integer + 1;
        $('.quantity_box-input').attr('value', num);
    });

    $('.minus').click(function(){
        let valInput = $('.quantity_box-input').attr('value');
        var integer = parseInt(valInput, 10);
        let num = integer - 1;
        if(num > 0){
            $('.quantity_box-input').attr('value', num);
        }
    });

    $(window).scroll(function () {

        if ($(this).scrollTop() > 200) {
            $('.top-btn').fadeIn();
        } else {
            $('.top-btn').fadeOut();
        }
    });

    $('.changePass').click(function(e){
        e.preventDefault();
       
    });
    $('.catalog-tabs ul li').click(function(e){
        e.preventDefault();

        let attr = $(this).find('a').attr('href');
        if ($(attr).css('display') == 'none'){ 
			$(attr).animate({height: 'show'}, 500); 
		}else{     
			$(attr).animate({height: 'hide'}, 500); 
		}
        $(this).toggleClass('current');
    });
    $('.smoothscroll').on('click', function (e) {
        e.preventDefault();
        var target = this.hash,
            $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 1000, 'swing', function () {
            window.location.hash = target;
        });
    });
});


(function($) {
    $('.owl1').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:true
            },
            1000:{
                items:4,
                nav:true,
                loop:true
            }
        }
    });
    $('.owl-carousel').owlCarousel({
        loop: ( $('.owl-carousel .item').length >= 5 ),
        rewind: true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:true
            },
            1000:{
                items:5,
                nav:true,
                
            }
        }
    });

    // $(".catalog-menu-btn").click(function(){
    //     $('.hamburger').toggleClass('is-active');
    //     $('.sub-menu').slideToggle(500);
    // });
    
    $(".edit_img_profile_mob").click(function(){
       $('.modal-img-cut').show();
    });


    new WOW().init();
})(jQuery);

function showPass() {
    var x = document.getElementById("password-type");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function showPass2() {
    var x = document.getElementById("password-type3");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
$(document).ready(function(){
    $('.price-filter-btn').click(function(e){
        e.preventDefault();
        let btnPrice = $(this).attr('href');

        if($(btnPrice).css('display') == 'none'){
            $(btnPrice).animate({height : 'show'}, 500);
        }else{
            $(btnPrice).animate({height : 'hide'}, 500);
        }
    });
});

$(document).ready(function () {
    $('.questions-box-tab li a').click(function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        console.log(link);
        // remove color active tab
        if($(link).css('display') == 'none') {
            $(this).addClass('questions-item-tab-active');

            // show dropdown block
            $(link).addClass('questions-box-info-active');

            // rotate img
            $(this).find('img').addClass('img-active');

            $(link).animate({
                height: 'show'
            }, 500);
        }else{
            $('.questions-box-tab li a').removeClass('questions-item-tab-active');

            // show dropdown block
            $('.questions-box-info').removeClass('questions-box-info-active');

            // rotate img
            $('.questions-box-tab li a img').removeClass('img-active');

            $(link).animate({
                height: 'hide'
            }, 500);
        }
    });


    $('.more-btn').click(function (e) {
        e.preventDefault();

        let attr = $(this).attr('href');
        let name = $(this).attr('data-tab-new');

        if (name == '1') {
            $('.more-btn').html('Скрыть');
            $('.more-btn').attr('data-tab-new', '0');
        } else {
            $('.more-btn').html('Показать еще');
            $('.more-btn').attr('data-tab-new', '1');
        }

        if ($(attr).css('display') == 'none') {
            $(attr).animate({
                height: 'show'
            }, 500);
        } else {
            $(attr).animate({
                height: 'hide'
            }, 500);
        }

    });
});
$(document).ready(function () {
    $('.sidebar__tabs li .firts-tabs').click(function (e) {
        e.preventDefault();
        let firstBtn = $(this).attr('href');
        if ($(firstBtn).css('display') == 'none') {
            $(this).prev('img').attr('src','/public/images/active.png');
            $(firstBtn).animate({
                height: 'show'
            }, 500);
        } else {
            $(this).prev('img').attr('src','/public/images/no-active.png');
            $(firstBtn).animate({
                height: 'hide'
            }, 500);
        }
    });
    $('.sidebar__tabs li .second-tabs').click(function (e) {
        e.preventDefault();
        let firstBtn = $(this).attr('href');
        if ($(firstBtn).css('display') == 'none') {
            $(this).prev('img').attr('src','/public/images/active.png');
            $(firstBtn).animate({
                height: 'show'
            }, 500);
        } else {
            $(this).prev('img').attr('src','/public/images/no-active.png');
            $(firstBtn).animate({
                height: 'hide'
            }, 500);
        }
    });
    $('.sidebar__tabs li .third-city').click(function (e) {
        e.preventDefault();
        let firstBtn = $(this).attr('href');
        if ($(firstBtn).css('display') == 'none') {
            $(this).prev('img').attr('src','/public/images/active.png');
            $(firstBtn).animate({
                height: 'show'
            }, 500);
        } else {
            $(this).prev('img').attr('src','/public/images/no-active.png');
            $(firstBtn).animate({
                height: 'hide'
            }, 500);
        }
    });
    $('.sidebar__tabs li .more-tabs').click(function (e) {
        e.preventDefault();
        let firstBtn = $(this).attr('href');
        let btnStatus = $(this).attr('data-tab');
        if ($(firstBtn).css('display') == 'none') {
            $(this).prev('img').attr('src','/public/images/active.png');
            $(firstBtn).animate({
                height: 'show'
            }, 500);
        } else {
            $(this).prev('img').attr('src','/public/images/no-active.png');
            $(firstBtn).animate({
                height: 'hide'
            }, 500);
        }
        if (btnStatus == '1') {
            $('.sidebar__tabs li .more-tabs').html('Скрыть');
            $('.sidebar__tabs li .more-tabs').attr('data-tab', '0');
        } else {
            $('.sidebar__tabs li .more-tabs').html('Показать еще');
            $('.sidebar__tabs li .more-tabs').attr('data-tab', '1');
        }
    });
});

$(document).ready(function(){

    $('#sea-pass').click(function(e){
        e.preventDefault();
        var attr = $(this).attr('data-password');
        if(attr == '1'){
            $('#password-type').attr('type','text');
            $('#sea-pass').attr('data-password','0');
            $('#sea-pass').attr('src', '/public/images/show-icon-2.png');
        }else{
            $('#sea-pass').attr('data-password','1');
            $('#sea-pass').attr('src', '/public/images/show-icon.png');
            $('#password-type').attr('type','password');
        }
    });



    $('#sea-pass-1').click(function(){
        var attr = $(this).attr('data-password');

        if(attr == '2'){
            $('#password-type2').attr('type','text');
            $('#sea-pass-1').attr('data-password','0');
            $('#sea-pass-1').attr('src', '/public/images/show-icon-2.png');
        }else{
            $('#sea-pass-1').attr('data-password','2');
            $('#sea-pass-1').attr('src', '/public/images/show-icon.png');
            $('#password-type2').attr('type','password');
        }
    });
    $('#sea-pass-2').click(function(){
        var attr = $(this).attr('data-password');

        if(attr == '3'){
            $('#password-type3').attr('type','text');
            $('#sea-pass-2').attr('data-password','0');
            $('#sea-pass-2').attr('src', '/public/images/show-icon-2.png');
        }else{
            $('#sea-pass-2').attr('data-password','3');
            $('#sea-pass-2').attr('src', '/public/images/show-icon.png');
            $('#password-type3').attr('type','password');
        }
    });



});


var minus = document.querySelectorAll(".minus-product");
for(var i = 0; i < minus.length; i++) {
    minus[i].addEventListener('click', function(event){
        var counter = event.target.nextElementSibling.innerHTML;
        if(counter > 1) {
            counter--;
        }
        event.target.nextElementSibling.innerHTML = counter;
    })
}

var plus = document.querySelectorAll(".plus-product");
for(var i = 0; i < plus.length; i++) {
    plus[i].addEventListener('click', function(event){
        var counter = event.target.previousElementSibling.innerHTML;
            counter++;

        event.target.previousElementSibling.innerHTML = counter;
    })
}


var path = window.location.pathname;

// console.log(path);
if(path == '/catalog/products'){
    // alert(12321);
}

if(path !== '/catalog' || path !== '/catalog/index' || path !== '/catalog/products/'){
    // localStorage.removeItem('anchor');
}

let size = 180;
let cardText = $('.card-info');

cardText.each(function(){
    if($(this).text().length > size){
        $(this).text($(this).text().slice(0, size) + '...');
    }
})







