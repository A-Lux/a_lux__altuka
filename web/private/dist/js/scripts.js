$('body').on('keyup', '.recommend', function () {
    var recommend = [];
    $('input[name="recommend[]"]').each(function () {
        recommend.push($(this).val());
    });

    var id = $('.product_id').val();
    $.ajax({
        type: 'GET',
        url: "/admin/products/recommend",
        data: {name: $('.recommend').val(), id: id, recommend: recommend},
        success: function(html){
            $('.recommend_div').html(html);
        }
    });
});

$('body').on('change', '.recommend_input', function () {
    var label = $(this).next('label');
    if($(this).is(':checked')) {
        $(this).appendTo('.recommend_isset').attr('name', 'recommend[]');
        label.appendTo('.recommend_isset');
    }
});