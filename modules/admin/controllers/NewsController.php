<?php

namespace app\modules\admin\controllers;

use app\job\MailSendJob;
use app\models\ImageUpload;
use app\models\NewsSubscribers;
use app\models\Subscribe;
use Yii;
use app\models\News;
use app\models\search\NewsSearch;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post())) {

            $this->createImage($model);

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            $this->updateImage($model, $oldImage);

            if($model->save()){
                return $this->redirect(['view', 'id' => $id]);
            }

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        $this->deleteImage($model);

        if($model->save(false)){
            return $this->redirect(['update?id='.$id]);
        }
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSubscribe($id)
    {
        if (!$news = $this->activeNews($id)) {
            \Yii::$app->session->setFlash('error','Эта новость уже была рассылана'.PHP_EOL);
            return $this->redirect(['index']);
        }

        if (!$emails = $this->activeSubscribers()) {
            \Yii::$app->session->setFlash('error','Нет подписок к рассылке'.PHP_EOL);
            return $this->redirect(['index']);
        }

        $count = count($emails);

        while ($batch = array_splice($emails, 0, 10)) {
            /**
             * Batch on input: [[id]]
             * Batch on output: [[id],[news_id],[status:ON]]
             */
            $batch = array_map(function ($element) use ($news) {
                $element = array_values($element);
                array_push($element, $news->id, NewsSubscribers::STATUS_WAIT);

                return $element;
            }, $batch);

            \Yii::$app->db->createCommand()
                ->batchInsert(NewsSubscribers::tableName(), ['subscriber_id', 'news_id', 'status'], $batch)
                ->execute();
        }

        $news->updateAttributes(['status' => 1]);

        \Yii::$app->session->setFlash('success',sprintf('Рассылка будет успешно отправлено %d подпискам', $count).PHP_EOL);
        return $this->redirect(['index']);
    }

    protected function activeSubscribers()
    {
        return Subscribe::find()->select(['id'])->asArray()->all();
    }

    /**
     * Returns oldest active dispatch
     * @return array|null|News
     */
    protected function activeNews($id)
    {
        return News::find()->where(['status' => 0, 'id' => $id])->one();
    }
}


//        $subscribers    = Subscribe::getList();
//        $model          = $this->findModel($id);
//
//        foreach ($subscribers as $subscriber)
//        {
//            Yii::$app->queue->push(new MailSendJob([
//                'to'        => $subscriber,
//                'subject'   => $model->name,
//                'body'      => $model->content,
//            ]));
//        }
//
//        $model->status = 1;
//
//        if($model->save()){
//            return $this->redirect(['index']);
//        }