<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 12.03.2019
 * Time: 20:30
 */

namespace app\modules\admin\controllers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class LabelPay
{
    public static function statusList()
    {
        return [
            0 => 'Не оплачено',
            1 => 'Оплачено',
            2 => 'Оплата через реквизиты',
        ];
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case 0:
                $class = 'label label-danger';
                break;
            case 1:
                $class = 'label label-success';
                break;
            case 2:
                $class = 'label label-default';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

}