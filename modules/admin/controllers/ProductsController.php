<?php

namespace app\modules\admin\controllers;

use app\models\Recommend;
use Yii;

use app\models\Products;
use app\models\search\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for News model.
 */

class ProductsController extends BackendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post())) {
            $model->url = $model->generateCyrillicToLatin($model->name,'-');

            $this->createImage($model);

            if ($model->save()) {
                $recommend = $_POST['recommend'];

                if ($recommend) {
                    foreach ($recommend as $v) {
                        $recommend = new Recommend();
                        $recommend->product1 = $model->id;
                        $recommend->product2 = $v;
                        $recommend->save();
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            $model->url = $model->generateCyrillicToLatin($model->name,'-');

            $this->updateImage($model, $oldImage);

            if($model->save()){
                $recommend = $_POST['recommend'];

                if($recommend){
                    $old_recommend = Recommend::find()->where('product1 = '.$model->id.' OR product2 = '.$model->id)->all();
                    $array_recommend = [];
                    foreach ($old_recommend as $v){
                        if($model->id == $v->product1)
                            $array_recommend[$v->product2] = 1;
                        else
                            $array_recommend[$v->product1] = 1;
                    }
                    foreach ($recommend as $v) {
                        unset($array_recommend[$v]);
                        $old_recommend = Recommend::find()->where('(product1 = '.$v.' OR product2 = '.$v.') 
                                                    AND (product1 = '.$model->id.' OR product2 = '.$model->id.')')->one();
                        if(!$old_recommend) {
                            $recommend = new Recommend();
                            $recommend->product1 = $model->id;
                            $recommend->product2 = $v;
                            $recommend->save();
                        }
                    }

                    if($array_recommend){
                        foreach ($array_recommend as $k => $v)
                            Recommend::find()
                                ->where('(product1 = '.$k.' OR product2 = '.$k.') 
                                         AND (product1 = '.$model->id.' OR product2 = '.$model->id.')')->one()->delete();
                    }
                }else
                    Recommend::deleteAll('product1 = '.$model->id.' OR product2 = '.$model->id);


            }
            return $this->redirect(['view', 'id' => $id]);
        }

        $recommend = Recommend::find()->where('product1 = '.$model->id.' OR product2 = '.$model->id)->all();

        if($recommend){
            $in = [];
            foreach ($recommend as $v){
                if($model->id == $v->product1)
                    $in[] = $v->product2;
                else
                    $in[] = $v->product1;
            }

            $recommend = Products::find()->where('id in ('.implode(',', $in).')')->all();
        }

        return $this->render('update', [
            'model' => $model,
            'recommend' => $recommend,
        ]);
    }

    public function actionDelete($id)
    {
        $model = Products::findOne(['id' => $id]);
        $models = Products::find()->where('sort > '.$model->sort)->all();

        foreach($models as $v){
            $v->sort--;
            $v->save(false);
        }

        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionDeleteImage($id)
    {
        $model = $this->findModel($id);

        $this->deleteImage($model);

        if($model->save(false)){
            return $this->redirect(['update?id='.$id]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMoveUp($id)
    {
        $model = Products::findOne($id);
        if ($model->sort != 1) {
            $sort = $model->sort - 1;
            $model_down = Products::find()->where("sort = $sort")->one();
            $model_down->sort += 1;
            $model_down->save(false);

            $model->sort -= 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }

    public function actionMoveDown($id)
    {
        $model = Products::findOne($id);
        $model_max_sort = Products::find()->orderBy("sort DESC")->one();

        if ($model->id != $model_max_sort->id) {
            $sort = $model->sort + 1;
            $model_up = Products::find()->where("sort = $sort")->one();
            $model_up->sort -= 1;
            $model_up->save(false);

            $model->sort += 1;
            $model->save(false);
        }
        $this->redirect(['index']);
    }

    public function actionRecommend()
    {
        $sql = '';
        if($_GET['recommend']) {
            $in = [];
            foreach ($_GET['recommend'] as $v){
                $in[] = $v;
            }
            $sql = ' AND id NOT IN ('.implode(',', $in).')';
        }
        if($_GET['id'])
            $recommend = Products::find()->where('name LIKE "%'.$_GET['name'].'%" AND id != '.$_GET['id'].$sql)->all();
        else
            $recommend = Products::find()->where('name LIKE "%'.$_GET['name'].'%"'.$sql)->all();

        return $this->renderAjax('recommend',compact('recommend'));
    }
}
