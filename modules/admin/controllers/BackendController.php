<?php

namespace app\modules\admin\controllers;


use yii\imagine\Image;
use yii\web\Controller;
use yii\web\UploadedFile;

class BackendController extends Controller
{
    public function beforeAction($action){
        if(\Yii::$app->user->identity->role != 1 && $action->actionMethod != 'actionLogin'){
            \Yii::$app->user->logout();
            return $this->redirect('/auth/login');
        }

        return parent::beforeAction($action);

    }

    protected function createImage($model)
    {
        $image = UploadedFile::getInstance($model, 'image');
        if($image != null) {
            $time = time();
            $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
            $model->image = $time . '_' . $image->baseName . '.' . $image->extension;

            $imagine    = new \Imagine\Gd\Imagine();
            $img      = $imagine->open($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
            $images     = Image::resize($img, 350, null);
            $images->save($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
        }
    }

    protected function updateImage($model, $oldImage)
    {
        $image = UploadedFile::getInstance($model, 'image');

        if($image == null){
            $model->image = $oldImage;
        }else{
            $time = time();
            $image->saveAs($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
            $model->image = $time . '_' . $image->baseName . '.' . $image->extension;
            if(!($oldImage == null)){
                if(file_exists($model->path . $oldImage)) {
                    unlink($model->path . $oldImage);
                }
            }

            $imagine    = new \Imagine\Gd\Imagine();
            $img      = $imagine->open($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
            $images     = Image::resize($img, 350, null);
            $images->save($model->path . $time . '_' . $image->baseName . '.' . $image->extension);
        }
    }

    protected function deleteImage($model)
    {
        $oldImage = $model->image;

        if(file_exists($model->path . $oldImage)){
            unlink($model->path . $oldImage);
        }

        $model->image = null;
    }

    protected function createFile($model)
    {
        $file = UploadedFile::getInstance($model, 'file');

        if($file != null){
            $time = time();
            $file->saveAs($model->path . $time . '_' . $file->baseName . '.' . $file->extension);
            $model->file = $time . '_' . $file->baseName . '.' . $file->extension;
        }
    }

    protected function updateFile($model, $oldFile)
    {
        $file = UploadedFile::getInstance($model, 'file');

        if($file == null){
            $model->file = $oldFile;
        }else{
            $time = time();
            $file->saveAs($model->path . $time . '_' . $file->baseName . '.' . $file->extension);
            $model->file = $time . '_' . $file->baseName . '.' . $file->extension;
            if(!($oldFile == null)){
                if(file_exists($model->path . $oldFile)) {
                    unlink($model->path . $oldFile);
                }
            }
        }
    }

    protected function deleteFile($model)
    {
        $oldFile = $model->file;

        if(file_exists($model->path . $oldFile)){
            unlink($model->path . $oldFile);
        }

        $model->file = null;
    }

}