<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsImage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-image-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->dropDownList(\app\models\Products::getList()) ?>


    <div class="row">
        <div class="col-md-4 col-xs-3" style="text-align: center">
            <div style="margin-top:20px;">
                <img src="/<?=$model->path?><?=$model->image?>" alt="" style="width:200px;">
            </div>
        </div>
    </div>

    <?php
    echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => ['accept' => 'image/*'],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
