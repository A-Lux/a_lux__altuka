<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsImage */

$this->title = 'Добавить изображение к продукту';
$this->params['breadcrumbs'][] = ['label' => 'Доп. изображение продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
