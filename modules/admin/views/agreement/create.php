<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agreement */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Условия доставки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
