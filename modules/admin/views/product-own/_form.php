<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\ProductOwn */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-own-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->dropDownList(\app\models\Products::getList(), ['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'user_id')->dropDownList(\app\models\User::getList(), ['disabled' => 'disabled']) ?>

    <?php
    echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload'            => false,
            'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
            'initialPreviewAsData'  => true,
            'initialCaption'        => $model->isNewRecord ? '': $model->image,
            'showRemove'            => true ,
            'deleteUrl'             => \yii\helpers\Url::to(['/admin/product-own/delete-image', 'id'=> $model->id]),
        ] ,
        'options' => ['accept' => 'image/*'],
    ]);
    ?>

    <?= $form->field($model, 'date')->textInput(['disabled' => 'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
