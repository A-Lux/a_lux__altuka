<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductOwn */

$this->title = 'Create Product Own';
$this->params['breadcrumbs'][] = ['label' => 'Product Owns', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-own-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
