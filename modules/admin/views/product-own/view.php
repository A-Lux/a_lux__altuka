<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductOwn */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Индивидуальный заказ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-own-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/admin/products/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute'=>'user_id',
                'value' => function($model){
                    return $model->userProfile->name." ".$model->userProfile->surname.' '.$model->userProfile->father;
                }
            ],
            [
                'format' => 'html',
                'filter' => '',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>200]);
                }
            ],
            'date',
        ],
    ]) ?>

</div>
