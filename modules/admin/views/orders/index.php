<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">
    <? $type = [1=>'По курьеру', 2=>'По банковская карта'];?>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'order_id',
            ['attribute'=>'user_id', 'value'=>function($model){ return $model->user->name." ".$model->user->surname.' '.$model->user->father;}],

            [
                'attribute' => 'statusProgress',
                'filter' => \app\modules\admin\controllers\LabelProgress::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelProgress::statusLabel($model->statusProgress);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPay',
                'filter' => \app\modules\admin\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelPay::statusLabel($model->statusPay);
                },
                'format' => 'raw',
            ],

//            [
//                'attribute' => 'type',
//                'filter' => \app\modules\admin\controllers\LabelType::statusList(),
//                'value' => function ($model) {
//                    return app\modules\admin\controllers\LabelType::statusLabel($model->type);
//                },
//                'format' => 'raw',
//            ],
            'created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
