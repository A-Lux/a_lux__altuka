<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="orders-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'statusProgress')->dropDownList([  0 => 'Не доставлено', 1 => 'Доставлено']) ?>

    <?= $form->field($model, 'statusPay')->dropDownList([0 => 'Не оплачено', 1 => 'Оплачено', 2 => 'Оплата через реквизиты']) ?>


    <?= $form->field($model, 'type')->dropDownList([ 1 => 'По курьеру', 2 => 'По банковская карта']) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
