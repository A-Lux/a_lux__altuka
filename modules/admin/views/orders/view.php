<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = 'Заказ: '.$model->order_id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view" style="padding-bottom: 600px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'order_id',
            ['attribute'=>'user_id', 'value'=>function($model){ return $model->user->name." ".$model->user->surname.' '.$model->user->father;}],

            'fullname',
            'email',
            'phone',
            'address',
            'comment',
            [
                'attribute' => 'statusProgress',
                'filter' => \app\modules\admin\controllers\LabelProgress::statusList(),
                'value' => function ($model) {
                    return app\modules\admin\controllers\LabelProgress::statusLabel($model->statusProgress);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPay',
                'filter' => \app\modules\admin\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelPay::statusLabel($model->statusPay);
                },
                'format' => 'raw',
            ],

//            [
//                'attribute' => 'type',
//                'filter' => \app\modules\admin\controllers\LabelType::statusList(),
//                'value' => function ($model) {
//                    return app\modules\admin\controllers\LabelType::statusLabel($model->type);
//                },
//                'format' => 'raw',
//            ],
            [
                'format' => 'raw',
                'attribute' => 'sum',
                'value' => function($data){
                    return $data->sum.' тг';
                }
            ],
            'created',
        ],
    ]) ?>


    <h2>Продукты заказа:</h2>
    <br>
    <?php foreach ($model->products as $v) :?>
        <? if($v->own_id == null): ?>
            <div style="margin-right: 50px;">
                <p style="width: 200px"><?=$v->product->name;?><b>  X <?=$v->count?></b></p>
                <p style="width: 200px"><?=$v->product->articul;?></p>
                <a href="/admin/products/view?id=<?=$v->product->id?>">
                    <img src="<?=$v->product->getImage();?>"  width="100">
                </a>
            </div>
        <? endif;?>
    <?php endforeach;?>

    <h2>Индивидуальный заказ:</h2>
    <br>
    <?php foreach ($model->products as $v) :?>
        <? if($v->own_id !== null): ?>
            <div style="margin-right: 50px;">
                <p style="width: 200px"><?=$v->ownProduct->product->name;?><b>  X <?=$v->count?></b></p>
                <p style="width: 200px"><?=$v->ownProduct->product->articul;?></p>
                <a href="/admin/product-own/view?id=<?=$v->ownProduct->id?>">
                    <img src="<?=$v->ownProduct->getImage();?>"  width="100">
                </a>

            </div>
        <? endif;?>
    <?php endforeach;?>

</div>
