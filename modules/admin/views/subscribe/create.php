<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Subscribe */

$this->title = 'Создания подписчика';
$this->params['breadcrumbs'][] = ['label' => 'Subscribes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
