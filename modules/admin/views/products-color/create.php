<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsColor */

$this->title = 'Добавить цвет продукта: ';
$this->params['breadcrumbs'][] = ['label' => 'Цвет продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-color-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
