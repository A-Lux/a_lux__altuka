<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductsColor */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Цвет продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-color-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute' => 'product_id', 'value' => $model->products->name],
            'name',
            'name_en',
            'name_kz',
            [
                'format' => 'raw',
                'attribute' => 'color',
                'value' => !empty($model->color) ? '<div style="background-color:'.$model->color.'; width:70px; height:30px;"></div>' :null,
            ],
        ],
    ]) ?>

</div>
