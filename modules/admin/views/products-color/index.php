<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductsColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цвет продукта';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-color-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить цвет продукта', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['attribute' => 'product_id', 'value' => 'productsName', 'filter' => \app\models\Products::getList()],
            'name',
            [
                'format' => 'raw',
                'attribute' => 'color',
                'value' => function ($model) {
                    return '<div style="background-color:'.$model->color.'; width:70px; height:30px;"></div>';
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
