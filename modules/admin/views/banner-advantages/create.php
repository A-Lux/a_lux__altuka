<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BannerAdvantages */

$this->title = 'Добавить преимущества';
$this->params['breadcrumbs'][] = ['label' => 'Наши преимущества', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-advantages-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
