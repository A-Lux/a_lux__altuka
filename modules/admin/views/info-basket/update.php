<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InfoBasket */

$this->title = 'Редактировать информацию по заказу: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Информация по заказу', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="info-basket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
