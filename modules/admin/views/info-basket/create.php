<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\InfoBasket */

$this->title = 'Добавить информацию по заказу';
$this->params['breadcrumbs'][] = ['label' => 'Информация по заказу', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="info-basket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
