<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use kartik\select2\Select2;

/* @var $model app\models\Products */

?>
<div class="products-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="kaz-tab" data-toggle="tab" href="#kaz" role="tab" aria-controls="kaz" aria-selected="false" class="nav-link">На казахском</a>
            </li>
            <li class="nav-item">
                <a id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false" class="nav-link">На английском</a>
            </li>
            <li class="nav-item">
                <a id="dop-tab" data-toggle="tab" href="#dop" role="tab" aria-controls="dop" aria-selected="false" class="nav-link">Акции и новинка</a>
            </li>
            <li class="nav-item">
                <a id="recommend-tab" data-toggle="tab" href="#recommend" role="tab" aria-controls="recommend" aria-selected="false" class="nav-link">Рекомендации</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Мета теги</a>
            </li>
            <li class="nav-item">
                <a id="profile_en-tab" data-toggle="tab" href="#profile_en" role="tab" aria-controls="profile_en" aria-selected="false" class="nav-link">Мета теги на казахском</a>
            </li>
            <li class="nav-item">
                <a id="profile_kz-tab" data-toggle="tab" href="#profile_kz" role="tab" aria-controls="profile_kz" aria-selected="false" class="nav-link">Мета теги на английском</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="recommend" role="tabpanel" aria-labelledby="recommend-tab" class="tab-pane fade">

                <h3>Рекомендуемые товары</h3>
                <div class="recommend_isset">
                    <?if(!$model->isNewRecord):?>
                        <? foreach ($recommend as $v):?>
                            <input checked="checked" name="recommend[]" type="checkbox" value="<?=$v->id?>"><label><?=$v->name?></label>
                        <?endforeach;?>
                    <?endif;?>
                </div>

                <input type="text" class="recommend">

                <div class="recommend_div"></div>

                <?if(!$model->isNewRecord){?>
                    <input type="hidden" class="product_id" value="<?=$model->id?>">
                <?}?>
            </div>
            <div id="profile_en" role="tabpanel" aria-labelledby="profile_en-tab" class="tab-pane fade">

                <?= $form->field($model, 'metaName_en')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_en')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_en')->textarea(['rows' => 6]) ?>

            </div>
            <div id="profile_kz" role="tabpanel" aria-labelledby="profile_kz-tab" class="tab-pane fade">

                <?= $form->field($model, 'metaName_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_kz')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_kz')->textarea(['rows' => 6]) ?>

            </div>
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">


                <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>

            </div>
            <div id="dop" role="tabpanel" aria-labelledby="dop-tab" class="tab-pane fade">

                <?= $form->field($model, 'size')->textInput() ?>

                <?= $form->field($model, 'isNew')->dropDownList([0 => 'Старая', 1 => 'Новая'], ['prompt' => '']) ?>

                <?= $form->field($model, 'isDiscount')->textInput() ?>

                <?= $form->field($model, 'isHit')->dropDownList([0 => 'Нет', 1 => 'Да'], ['prompt' => '']) ?>

                <?= $form->field($model, 'newPrice')->textInput() ?>

            </div>
            <div id="en" role="tabpanel" aria-labelledby="en-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'content_en')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]);
                ?>

            </div>
            <div id="kaz" role="tabpanel" aria-labelledby="kaz-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'content_kz')->widget(CKEditor::className(),[
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ])
                ]);
                ?>

            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'articul')->textInput(['maxlength' => true]) ?>

                <?php
                echo $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload'            => false,
                        'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
                        'initialPreviewAsData'  => true,
                        'initialCaption'        => $model->isNewRecord ? '': $model->image,
                        'showRemove'            => true ,
                        'deleteUrl'             => \yii\helpers\Url::to(['/admin/products/delete-image', 'id'=> $model->id]),
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

                <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder',[
                        'options' => ['rows' => 6],
                        'allowedContent' => true,
                        'preset' => 'full',
                        'inline' => false
                    ]),
                ]) ?>

                <?= $form->field($model, 'price')->textInput() ?>

                <div class="form-group field-products-category_id required has-error">
                    <label class="control-label" for="products-category_id">Категория</label>

                    <select name="Products[category_id]" class="form-control">
                        <option></option>
                        <?
                        $catalog = \app\models\Catalog::find()
                            ->where("level = 1")->all();
                        $array = [];
                        foreach ($catalog as $v){
                            echo '<option value="'.$v->id.'" disabled="disabled">'.$v->name.'</option>';
                            $array[$v->id] = $v->name;
                            if(isset($v->childs)){
                                foreach($v->childs as $child1) {
                                    if(!empty($child1->childs))
                                        echo '<option value="'.$child1->id.'" disabled="disabled">--'.$child1->name.'</option>';
                                    else{
                                        $selected = '';
                                        if($child1->id == $model->category_id)
                                            $selected = 'selected="selected"';
                                        echo '<option '.$selected.' value="'.$child1->id.'">--'.$child1->name.'</option>';
                                    }
                                    $array[$child1->id] = '--'.$child1->name;
                                    if(isset($child1->childs)){
                                        foreach($child1->childs as $child2){
                                            if(!empty($child2->childs))
                                                echo '<option value="'.$child2->id.'">----'.$child2->name.'</option>';
                                            else {
                                                $selected = '';
                                                if ($child2->id == $model->category_id)
                                                    $selected = 'selected="selected"';
                                                echo '<option ' . $selected . ' value="' . $child2->id . '">----' . $child2->name . '</option>';
                                                $array[$child2->id] = '----' . $child2->name;
                                            }
                                            if(isset($child2->childs)){
                                                foreach($child2->childs as $child3) {
                                                    if (!empty($child3->childs))
                                                        echo '<option value="' . $child3->id . '">--------' . $child3->name . '</option>';
                                                    else {
                                                        $selected = '';
                                                        if ($child3->id == $model->category_id)
                                                            $selected = 'selected="selected"';
                                                        echo '<option ' . $selected . ' value="' . $child3->id . '">--------' . $child3->name . '</option>';
                                                        $array[$child3->id] = '----' . $child3->name;
                                                    }
                                                    if(isset($child3->childs)){
                                                        foreach($child3->childs as $child4) {
                                                            if (!empty($child4->childs))
                                                                echo '<option value="' . $child4->id . '">--------' . $child4->name . '</option>';
                                                            else {
                                                                $selected = '';
                                                                if ($child4->id == $model->category_id)
                                                                    $selected = 'selected="selected"';
                                                                echo '<option ' . $selected . ' value="' . $child4->id . '">--------' . $child4->name . '</option>';
                                                                $array[$child4->id] = '----' . $child4->name;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                    </select>

                </div>

                <?= $form->field($model, 'status')->dropDownList([0 => 'Скрыто', 1 => 'Доступно'], ['prompt' => '']) ?>
            </div>
        </div>
    </div>

    <div class="form-group" style="padding-top: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
