<?php


use app\modules\admin\controllers\LabelNew;
use app\modules\admin\controllers\LabelPopular;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                 'attribute' => 'category_id',
                 'value' => function ($model) {
                     return $model->category->name;
                 },
                 'format' => 'raw',
                 'contentOptions' => ['style' => 'text-align: center'],
            ],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            
            [
                 'value' => function ($model) {
                     return
                         Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $model->id]) .
                         Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $model->id]);
                 },
                 'format' => 'raw',
                 'contentOptions' => ['style' => 'text-align: center'],
            ],
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],
            'price',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
