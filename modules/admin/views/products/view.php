<?php

use app\modules\admin\controllers\LabelNew;
use app\modules\admin\controllers\LabelPopular;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            ['attribute'=>'category_id', 'value'=>function($model){ return $model->categoryName;}],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'name_en',
                'value' => $model->name_en,
                'format' => 'raw',
            ],
            [
                'attribute' => 'name_kz',
                'value' => $model->name_kz,
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'filter' => '',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            'url',
            'articul',
            'price',
            'content:ntext',
            'content_en:ntext',
            'content_kz:ntext',
            'isDiscount',
            'newPrice',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelNew::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelNew::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isHit',
                'filter' => LabelPopular::statusList(),
                'value' => function ($model) {
                    return LabelPopular::statusLabel($model->isHit);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isNew',
                'filter' => LabelNew::statusList(),
                'value' => function ($model) {
                    return LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],
            'metaName',
            'metaName_en',
            'metaName_kz',
            'metaDesc:ntext',
            'metaDesc_en:ntext',
            'metaDesc_kz:ntext',
            'metaKey:ntext',
            'metaKey_en:ntext',
            'metaKey_kz:ntext',
            
        ],
    ]) ?>

</div>
