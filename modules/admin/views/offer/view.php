<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Offer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Договор оферты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="offer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php $files = $model->getFile(); ?>
    <?php if($files != null) $file = ['attribute'=>'file','value' => Html::a($model->file,$files,['target'=>'_blank']),'format' => 'raw' ];
    else $file =  ['attribute'=>'file','value' => '']?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'name_en',
            'name_kz',
            $file,
        ],
    ]) ?>

</div>
