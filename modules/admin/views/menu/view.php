<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            'text_en',
            'text_kz',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return \app\modules\admin\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'metaName',
            'metaName_en',
            'metaName_kz',
            'metaDesc:ntext',
            'metaDesc_en:ntext',
            'metaDesc_kz:ntext',
            'metaKey:ntext',
            'metaKey_en:ntext',
            'metaKey_kz:ntext',
        ],
    ]) ?>

</div>
