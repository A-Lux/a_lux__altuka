<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

<div class="contact-form" style="padding-bottom: 900px;">

    <?php $form = ActiveForm::begin(); ?>


    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Cоциальные сети</a>
            </li>
            <li class="nav-item">
                <a id="kaz-tab" data-toggle="tab" href="#kaz" role="tab" aria-controls="kaz" aria-selected="false" class="nav-link">На казахском</a>
            </li>
            <li class="nav-item">
                <a id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false" class="nav-link">На английском</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="en" role="tabpanel" aria-labelledby="en-tab" class="tab-pane fade">

                <?= $form->field($model, 'address_en')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'work_time_en')->textInput(['maxlength' => true]) ?>

            </div>
            <div id="kaz" role="tabpanel" aria-labelledby="kaz-tab" class="tab-pane fade">

                <?= $form->field($model, 'address_kz')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'work_time_kz')->textInput(['maxlength' => true]) ?>

            </div>
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">

                <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'whatsap')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'vk')->textInput(['maxlength' => true]) ?>

            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'work_time')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'iframe')->textInput(['maxlength' => true]) ?>

            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
