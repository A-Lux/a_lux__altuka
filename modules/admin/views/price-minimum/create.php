<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PriceMinimum */

$this->title = 'Добавить мин. цену заказа';
$this->params['breadcrumbs'][] = ['label' => 'Минимальная цена для заказа', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-minimum-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
