<?php

namespace app\components;

use app\models\Orders;
use Yii;
use yii\base\Component;

class Sync extends Component
{
    public function orderExport($order_id)
    {
        $file = Yii::$app->basePath . "/sync/orders/order_{$order_id}.xml";

        if (file_exists($file)) {
            unlink($file);
        }

        $order = Orders::find()->where(['order_id' => $order_id])->one();

        $xml  = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $xml .= "<order>\n";
        $xml .= "    <order_id>{$order->order_id}</order_id>\n";
        $xml .= "    <date>{$order->created}</date>\n";
        $xml .= "    <sum>{$order->sum}</sum>\n";
        $xml .= "    <user_id>{$order->user_id}</user_id>\n";
        $xml .= "    <fullname>{$order->fullname}</fullname>\n";
        $xml .= "    <email>{$order->email}</email>\n";
        $xml .= "    <phone>{$order->phone}</phone>\n";
        $xml .= "    <address>{$order->address}</address>\n";
        $xml .= "    <comment>{$order->comment}</comment>\n";
        $xml .= "    <status>{$order->statusPay}</status>\n";
        $xml .= "    <items>\n";

        foreach ($order->products as $product) {
            $xml .= "        <item>\n";
            $xml .= "               <articul>{$product->product->articul}</articul>\n";
            $xml .= "               <count>{$product->count}</count>\n";
            $xml .= "       </item>\n";
        }

        $xml .= "   </items>\n";
        $xml .= "</order>";

        $sync = file_put_contents(Yii::$app->basePath . "/sync/orders/order_{$order_id}.xml", $xml);

        if($sync){
            return true;
        }else{
            return false;
        }
    }
}