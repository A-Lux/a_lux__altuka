<?php

namespace app\job;

use yii\base\Object;
use yii\db\Exception;
use yii\queue\RetryableJob;

/**
 * Class MailSendJob
 * @property integer read-only $ttr
 * @package app\job
 * @property integer $ttr
 */
class MailSendJob extends Object implements RetryableJob
{
    public $to;
    public $subject;
    public $body;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        try {
            return \Yii::$app->mailer->compose()
                ->setFrom([\Yii::$app->params['adminEmail'] => '"AL2KA"'])
                ->setTo($this->to)->setSubject($this->subject)
                ->setHtmlBody($this->body)->setTextBody(\Yii::$app->formatter->asNtext($this->body))
                ->send();
        }catch (\Exception $e){
            throw new Exception('Очередь не смог обработать запрос!');
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 60;
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}


