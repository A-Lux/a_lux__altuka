<?php

namespace app\commands;

use app\models\Catalog;
use Imagine\Image\Box;
use Yii;
use app\models\Products;
use yii\console\ExitCode;
use yii\console\Controller;
use yii\imagine\Image;

/**
 * Sync controller
 */
class SyncController extends Controller
{
    public function catalog($catalog)
    {
        $explode = explode('/', $catalog);
        array_shift($explode);

        $result = [];
        foreach($explode as $item){
            $result[] = explode('. ', $item);
        }

        return $result;
    }

    public function actionSync()
    {
        $dir = Yii::$app->basePath.'/sync';

        $products = file_get_contents($dir . "/products.json");

        $products = json_decode($products, true);

        foreach($products as $_product){
            $categories = $this->catalog($_product['Родитель']);

            for($i = 0; $i < count($categories)-1; $i++ ){
                $first_catalog  = Catalog::find()->where(['name' => $categories[$i][0] ])->one();
                $second_catalog = Catalog::find()->where(['name' => $categories[$i+1][0] ])->one();

                if(!$first_catalog){
                    $first_catalog              = new Catalog();
                    $first_catalog->name        = $categories[$i][0];
                    $first_catalog->name_kz     = $categories[$i][1];
                    $first_catalog->name_en     = $categories[$i][2];
                    $first_catalog->status      = 1;
                    $first_catalog->level       = $i+1;
                    $first_catalog->save(false);
                }
                if(!$second_catalog){
                    $second_catalog             = new Catalog();
                    $second_catalog->name       = $categories[$i+1][0];
                    $second_catalog->name_kz    = $categories[$i+1][1];
                    $second_catalog->name_en    = $categories[$i+1][2];
                    $second_catalog->status     = 1;
                    $second_catalog->level      = $i+2;
                    $second_catalog->save(false);
                }
                $second_catalog->parent_id = $first_catalog->id;
                $second_catalog->save(false);
            }

            if(!($product = Products::find()->where(['articul' => $_product['Код']])->one())){
                $product = new Products();
            }

            if(!empty($_product['Артикул'])){
                $product->category_id   = $second_catalog->id;
                $product->articul       = $_product['Код'];
                $product->size          = $_product['Артикул'];
                $product->name          = $_product['НазваниеРус'];
                $product->name_en       = $_product['НазваниеАнгл'];
                $product->name_kz       = $_product['НазваниеКаз'];
                $product->content       = $_product['ОписаниеРус'];
                $product->content_en    = $_product['ОписаниеАнгл'];
                $product->content_kz    = $_product['ОписаниеКаз'];
                $product->price         = $_product['Цена'];
                $product->status        = $_product['Статус'];
                $product->isHit         = $_product['Популярное'];
                $product->isNew         = $_product['Новая'];
                $product->isDiscount    = $_product['Скидка'];
                $product->url           = $product->generateCyrillicToLatin($product->name.'-'.$_product['Код']);

                $image_str  = base64_decode($_product['ФайлКартинки']);
                $image_path = Yii::$app->basePath . '/web/' .$product->path;
                $image_name = $product->articul.'_'.$product->size.'.png';
                file_put_contents($image_path . $image_name, $image_str);

                $imagine    = new \Imagine\Gd\Imagine();
                $watermark  = $imagine->open(Yii::$app->basePath . '/web/public/images/watermark_kz.png');
                $image      = $imagine->open($image_path . $image_name);
                $images     = Image::resize($image, 600, null);
                $watermark  = Image::resize($watermark, 200, null);

                $size       = $images->getSize();
                $wSize      = $watermark->getSize();

                $width      = ($size->getWidth() - $wSize->getWidth())/2;
                $height     = ($size->getHeight() - $wSize->getHeight())/2;

                $bottomRight = new \Imagine\Image\Point($width,$height);

                $images->paste($watermark, $bottomRight)->save($image_path . $image_name);

                $product->image = $image_name;
                $product->save(false);
            }
        }
    }
}