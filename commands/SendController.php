<?php

namespace app\commands;

use app\models\NewsSubscribers;
use yii\console\Controller;

class SendController extends Controller
{
    public function actionMail()
    {
        if (!$pivots = $this->activeSubscribers()) {
            echo 'Нет активных рассылок'.PHP_EOL;
            exit;
        }

        $count = count($pivots);

        while ($pivot = array_shift($pivots)) {
            /* @var $pivot NewsSubscribers */

            sleep(45);
            $html = $pivot->news->content;
            $text = $pivot->news->content;

                \Yii::$app->mailer
                    ->compose()
                    ->setFrom([\Yii::$app->params['adminEmail'] => '"AL2KA"'])
                    ->setTo($pivot->subscriber->email)
                    ->setSubject($pivot->news->name)
                    ->setHtmlBody($html)
                    ->setTextBody($text)
                    ->send();

            $pivot->updateAttributes(['status' => NewsSubscribers::STATUS_SEND]);

            echo sprintf('Email to %s is sent', $pivot->subscriber->email).PHP_EOL;
        }

        echo '----------'.PHP_EOL;
        echo sprintf('%d emails sent', $count).PHP_EOL;
    }


    /**
     * Returns active pivot relations news-subscribe
     * @return array|\yii\db\ActiveRecord[]
     */
    protected function activeSubscribers()
    {
        return NewsSubscribers::find()->where(['status' => NewsSubscribers::STATUS_WAIT])
            ->orderBy(['id' => SORT_ASC])->with(['subscriber', 'news'])->all();
    }
}