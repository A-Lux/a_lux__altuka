<?php

namespace app\commands;

use Yii;
use app\models\Currency;
use yii\console\Controller;
use yii\console\ExitCode;

class ConversionController extends Controller
{
    public function actionConversion()
    {
        $target = $this->code();    // коды валюты

        if (!$target) {
            exit('model Currency not find'.PHP_EOL);
        }

        $currency = self::currency();   // вызов конвертера по api

        if (!$currency) {
            exit('api converter file is damaged'.PHP_EOL);
        }

        $result = [];

        foreach ($currency as $item) {
            if(in_array($item->title, $target)){
                $result[] = $item;
            }
        }

        $this->updateCurrency($result);   //  изменение значение валюты в бд

        return ExitCode::OK;
    }

    public static function currency()
    {
        $currency = simplexml_load_file('https://nationalbank.kz/rss/rates_all.xml');

        $array = [];
        foreach ($currency->channel as $value){
            foreach ($value->item as $item){
                $array[] = $item;
            }
        }

        return $array;
    }

    public function updateCurrency($result)
    {
        foreach ($result as $value){
            $model = Currency::find()->where(['code' => $value->title])->one();
            $model->value = $value->description;

            $model->save(false);
        }

        return 1;
    }

    public function code()
    {
        $model = Currency::find()->all();
        $code = [];
        foreach ($model as $item){
            $code[] = $item->code;
        }

        return $code;
    }
}