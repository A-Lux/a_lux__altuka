<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.02.2019
 * Time: 17:08
 */

namespace app\assets;

use yii\web\AssetBundle;


class publicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "public/css/bootstrap.css",
        "public/css/owl.carousel.min.css",
        "public/css/owl.theme.default.min.css",
        "public/css/animate.css",
        "public/css/main.css",
        "public/css/hamburgers.css",
        "https://use.fontawesome.com/releases/v5.7.1/css/all.css",
    ];
    public $js = [
        "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
        "public/js/bootstrap.min.js",
        "public/js/wow.min.js",
        "public/js/jquery.mask.min.js",
        "public/js/owl.carousel.min.js",
        "public/js/scripts.js",
        "public/js/catalog.js",
        "public/js/basket.js",
        "public/js/product-own.js",
        "public/js/account.js",
        "public/js/filter.js",
        "public/js/main.js",
    ];
    public $depends = [];
}
