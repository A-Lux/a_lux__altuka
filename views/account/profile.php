<?php

use yii\widgets\LinkPager;

?>
<section class="cabinet">
    <div class="container catalog">
        <!-- breadcrumbs -->
        <?  $text = "text".Yii::$app->session["lang"];?>
        <?  $name = "name".Yii::$app->session["lang"];?>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumbs">
                <li>
                        <a href="/"><?=Yii::$app->view->params['main'];?></a>
                    </li>
                    <li>
                        <a href=""> / </a>
                    </li>
                    <li>
                        <a href=""><?=$model->$text;?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end breadcrumbs -->
        <!-- contact title -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <p class="contact-title"><?=$model->$text;?></p>
            </div>
        </div>
        <!-- end contact title -->
        <!-- box contact -->
        <div class="row">
            <div class="col-sm-12">
                <div class="box-cabinet">
                    <div class="col-sm-3 p-0">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                            aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tabCabinet" data-toggle="pill" href="#v-pills-homeCabinet"
                                role="tab" aria-controls="v-pills-homeCabinet" aria-selected="true">
                                <?= Yii::t('main', 'Личные данные')?></a>
                            <a class="nav-link" id="v-pills-profile-tabCabinet" data-toggle="pill" href="#v-pills-profileCabinet"
                                role="tab" aria-controls="v-pills-profileCabinet" aria-selected="false">
                                <?= Yii::t('main', 'Избранное')?></a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settingsCabinet"
                                role="tab" aria-controls="v-pills-settingsCabinet" aria-selected="false">
                                <?= Yii::t('main', 'История заказов')?></a>
                            <a class="nav-link" id="v-pills-own-tab" data-toggle="pill" href="#v-pills-ownCabinet"
                               role="tab" aria-controls="v-pills-ownCabinet" aria-selected="false">
                                <?= Yii::t('main', 'Индивидуальный заказ')?></a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <div class="tab-content" id="v-pills-tabContentCabinet">
                            <div class="tab-pane fade show active" id="v-pills-homeCabinet" role="tabpanel"
                                aria-labelledby="v-pills-home-tabCabinet">
                                <form action="">
                                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                                    value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                                    <div class="cabinet-form-row">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <span><?= Yii::t('main', 'Мои данные')?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cabinet-form-row">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3">
                                                <p><?= Yii::t('main', 'Логин')?></p>
                                            </div>
                                            <div class="col-sm-12 col-md-9 col-lg-9">
                                                <div class="input-wrap">
                                                    <input type="text" value="<?=$user->username;?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cabinet-form-row">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3">
                                                <p><?= Yii::t('main', 'Имя')?></p>
                                            </div>
                                            <div class="col-sm-12 col-md-9 col-lg-9">
                                                <div class="input-wrap">
                                                    <input type="text" value="<?=$profile->name;?>" name="UserProfile[name]"
                                                    placeholder="<?= Yii::t('main', 'Имя')?>">
                                                    <a href="#" data-toggle="modal" data-target="#exampleModal"><?= Yii::t('main', 'Сменить пароль')?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cabinet-form-row">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3">
                                                <p><?= Yii::t('main', 'Ваша E-mail')?></p>
                                            </div>
                                            <div class="col-sm-12 col-md-9 col-lg-9">
                                                <div class="input-wrap">
                                                    <input type="text" value="<?=$user->email;?>" name="User[email]"
                                                    placeholder="<?= Yii::t('main', 'Почта')?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cabinet-form-row">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-3">
                                                <p><?= Yii::t('main', 'Телефон')?></p>
                                            </div>
                                            <div class="col-sm-12 col-md-9 col-lg-9">
                                                <div class="input-wrap">
                                                    <input type="text" value="<?=$profile->phone;?>" name="UserProfile[phone]"
                                                    placeholder="8(000) 000-0000">
                                                    <script>
                                                    $('input[name="UserProfile[phone]"]').inputmask("8(999) 999-9999");
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="cabinet-form-row">
                                        <div class="row">
                                                <div class="col-sm-12 col-md-3 col-lg-3">
                                                    <p><?= Yii::t('main', 'Адрес')?></p>
                                                </div>

                                                <div class="col-sm-12 col-md-9 col-lg-9">
                                                    <div class="input-wrap">
                                                        <input type="text" value="<?= $userAddress->address; ?>"
                                                        name="UserAddress[address][]" placeholder="<?= Yii::t('main', 'Адрес')?>">
                                                    </div>
                                                </div>
                                        </div>
                                    </div>

                                    <div class="cabinet-form-row">
                                        <div class="row">
                                            <div class="submit-btn">
                                                <button type="button" class="submit-btn__btn update-profile-button"
                                                        data-message-success-1="<?= Yii::t('main', 'Ваши изменения успешно сохранен.')?>"
                                                        data-message-error-1="<?= Yii::t('main', 'Ошибка!')?>"
                                                        data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                                        data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                                                    <?= Yii::t('main', 'Сохранить')?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profileCabinet" role="tabpanel"
                                    aria-labelledby="v-pills-profile-tabCabinet">
                                    <div class="favorites">
                                        <div class="row text-center">
                                            <? if ($fav == null): ?>
                                                <?= Yii::t('main', 'В избранном пока нет товаров.')?>
                                            <? endif; ?>

                                            <? if ($fav != null): ?>
                                                <div class="row" style="width: 100%;">
                                                    <? foreach ($fav as $v): ?>
                                                        <div class="col-sm-4">
                                                            <div class="card">
                                                                <div class="delete-icon btn-delete-product-from-favorite"
                                                                     data-id="<?= $v->id; ?>">
                                                                    &#10005
                                                                </div>
                                                                <div class="card-img">
                                                                <img src="<?=$v->getImage();?>" alt="" style="width: 237px; height: 205px">
                                                                </div>
                                                                <div class="card-content">
                                                                    <p class="card-title"><?=$v->$name;?></p>
                                                                    <p class="card-price"><?=$v->price?> kzt</p>
                                                                </div>
                                                                <div class="buttons">
                                                                    <?if($v->status !== 0):?>
                                                                        <button class="btn btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>"
                                                                                data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!')?>"
                                                                                data-message-success-2="<?= Yii::t('main', 'Продолжить покупки')?>"
                                                                                data-message-success-3="<?= Yii::t('main', 'Оформить заказ')?>"
                                                                                data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!')?>"
                                                                                data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                                                                data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                                                                            <?= Yii::t('main', 'В корзину')?></button>
                                                                    <?else:?>
                                                                        <button class="btn btn-not-basket" type="button" name="button">
                                                                            <?= Yii::t('main', 'Товара нет в наличии')?></button>
                                                                    <?endif;?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <? endforeach; ?>
                                                </div>
                                            <? endif; ?>

                                        </div>
                                    </div>
                                </div>
                            <div class="tab-pane fade" id="v-pills-ownCabinet" role="tabpanel"
                                 aria-labelledby="v-pills-own-tabCabinet">
                                <div class="favorites">
                                    <div class="row text-center">
                                        <? if ($ownProduct == null): ?>
                                            <?= Yii::t('main', 'Вы еще не оформили индивидуальный заказ.')?>
                                        <? endif; ?>

                                        <? if ($ownProduct != null): ?>
                                            <div class="row" style="width: 100%;">
                                                <? foreach ($ownProduct as $product): ?>
                                                    <div class="col-12 col-sm-6 col-lg-4 pb-4">
                                                        <div class="card">
                                                            <div class="delete-icon">
                                                                <a href="/product-own/delete-own?id=<?= $product->id;?>">
                                                                    &#10005
                                                                </a>
                                                            </div>
                                                            <div class="card-img">
                                                                <img src="<?= $product->getImage();?>" alt="">
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="card-title"><?= $product->product->$name;?></p>
                                                                <p class="card-title"><?= $product->product->size; ?></p>
                                                                <p class="card-price"><?= $product->product->price?> kzt</p>
                                                            </div>
                                                            <div class="buttons">
                                                                <button class="btn btn-to-basket-product-own" type="button" name="button" data-id="<?= $product->id;?>"
                                                                        data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!')?>"
                                                                        data-message-success-2="<?= Yii::t('main', 'Продолжить покупки')?>"
                                                                        data-message-success-3="<?= Yii::t('main', 'Оформить заказ')?>"
                                                                        data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!')?>"
                                                                        data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                                                        data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                                                                    <?= Yii::t('main', 'В корзину')?>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach; ?>
                                            </div>
                                        <? endif; ?>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-settingsCabinet" role="tabpanel"
                                aria-labelledby="v-pills-settings-tabCabinet">
                                <table class="table">

                                    <tr>
                                        <th scope="col"><?= Yii::t('main', 'Номер заказа')?></th>
                                        <th scope="col"><?= Yii::t('main', 'Дата оформление')?></th>
                                        <th scope="col"><?= Yii::t('main', 'Статус')?></th>
                                        <th scope="col"><?= Yii::t('main', 'Сумма')?></th>
                                    </tr>

                                    <? foreach ($orders['data'] as $v): ?>
                                        <tr>
                                            <th scope="row"><?= $v->order_id; ?></th>
                                            <td><?= $v->date; ?></td>
                                            <td><?= $v->statusProgress; ?></td>
                                            <td><?= $v->sum; ?> тг</td>
                                        </tr>
                                    <? endforeach; ?>
                                </table>
                                <div class="pagination">
                                    <ul>
                                        <?= LinkPager::widget([
                                            'pagination' => $orders['pagination'],
                                            'activePageCssClass' => 'active',
                                            'hideOnSinglePage' => true,
                                            'prevPageLabel' => '<i class="arrow left"></i>',
                                            'nextPageLabel' => '<i class="arrow right"></i>',
                                        ]);
                                        ?>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end box contact -->
        </div>
</section>


                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content modal-login modal-personal">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('main', 'Сменить пароль')?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="">

                            <div class="modal-body">
                                <div class="modal-text">
                                    <p></p>
                                </div>
                                <div class="authorization-input">
                                    <input type="password" id="password-type" placeholder="<?= Yii::t('main', 'Старый пароль')?>"
                                           name="PasswordUpdate[old_password]">
                                    <img src="/public/images/show-icon.png" id="sea-pass" alt="" data-password="1">
                                </div>
                                <div class="authorization-input">
                                    <input type="password" id="password-type2" placeholder="<?= Yii::t('main', 'Новый пароль')?>"
                                           name="PasswordUpdate[password]">
                                    <img src="/public/images/show-icon.png" id="sea-pass-1" alt="" data-password="2">
                                </div>
                                <div class="authorization-input">
                                    <input type="password" id="password-type3" placeholder="<?= Yii::t('main', 'Подтвердите новый пароль')?>"
                                           name="PasswordUpdate[password_verify]">
                                    <img src="/public/images/show-icon.png" id="sea-pass-2" alt="" data-password="3">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn update-password-button"
                                        data-message-success-1="<?= Yii::t('main', 'Ваши изменения успешно сохранен.')?>"
                                        data-message-error-1="<?= Yii::t('main', 'Ошибка!')?>"
                                        data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                        data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                                    <?= Yii::t('main', 'Сохранить')?></button>
                                <a href="#" class="changePass" data-toggle="modal" data-target="#updatePassword">
                                    <?= Yii::t('main', 'Забыли пароль?')?>
                                </a>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="updatePassword" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content modal-personal">
                            <div class="modal-header">
                                <h5 class="modal-title" id="updatePassword"><?= Yii::t('main', 'Забыли пароль?')?></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="">

                            <div class="modal-body">
                                <div class="modal-text">
                                    <p></p>
                                </div>
                                <div class="authorization-input">
                                    <input type="text" placeholder="<?= Yii::t('main', 'Ваша E-mail')?>"
                                           name="ForgotYourPassword[email]">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn sendForgotPassword"
                                        data-message-success-1="<?= Yii::t('main', 'Новый пароль отправлен на Вашу электронную почту')?>"
                                        data-message-error-1="<?= Yii::t('main', 'Ошибка!')?>"
                                        data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                        data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                                    <?= Yii::t('main', 'Отправить')?></button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
