<div class="container">
    <div class="row">
        <?  $name = "name".Yii::$app->session["lang"];?>
        <div class="offset-lg-4 col-lg-4 col-sm-12">
            <div class="authorization text-center">
                <h3><?= Yii::t('main', 'Авторизация')?></h3>
                <form class="">
                    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                           value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                    <div class="authorization-input">
                        <input type="text" placeholder="<?= Yii::t('main', 'Логин')?>"
                               name="LoginUser[username]">
                    </div>
                    <div class="authorization-input">
                        <input type="password" placeholder="<?= Yii::t('main', 'Пароль')?>"
                               id="password-type" name="LoginUser[password]">
                        <img src="/public/images/show-icon.png" id="sea-pass" alt="" data-password="1">

                    </div>
                    <div class="authorization-button">
                        <button type="button" class="login-button btn"><?= Yii::t('main', 'Войти')?></button>
                    </div>
                </form>
                <div class="authorization-button">
                    <a href="/account/sign-up">
                        <button class="btn btn-light"><?= Yii::t('main', 'Регистрация')?></button>
                    </a>
                </div>
                <div class="authorization-button">
                    <a href="#" data-toggle="modal" data-target="#updatePassword"><?= Yii::t('main', 'Забыли пароль?')?></a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updatePassword" tabindex="-1" role="dialog" aria-labelledby="updatePassword" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-personal">
                <div class="modal-header">
                    <h5 class="modal-title" id="updatePassword"><?= Yii::t('main', 'Забыли пароль?')?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="">

                    <div class="modal-body">
                        <div class="modal-text">
                            <p></p>
                        </div>
                        <div class="authorization-input">
                            <input type="text" placeholder="<?= Yii::t('main', 'Ваша E-mail')?>"
                                   name="ForgotYourPassword[email]">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn sendForgotPassword"
                                data-message-success-1="<?= Yii::t('main', 'Новый пароль отправлен на Вашу электронную почту')?>"
                                data-message-error-1="<?= Yii::t('main', 'Ошибка!')?>"
                                data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                            <?= Yii::t('main', 'Отправить')?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
