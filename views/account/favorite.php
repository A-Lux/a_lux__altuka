<div class="favorites">
    <?  $name = "name".Yii::$app->session["lang"];?>
    <div class="row text-center">
        <? if ($fav == null): ?>
            <?= Yii::t('main', 'В избранном пока нет товаров.')?>
        <? endif; ?>
        <? if ($fav != null): ?>
            <div class="row" style="width: 100%;">
                <? foreach ($fav as $v): ?>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="delete-icon btn-delete-product-from-favorite"
                                 data-id="<?= $v->id; ?>">
                                &#10005
                            </div>
                            <div class="card-img">
                                <img src="<?=$v->getImage();?>" alt="" style="width: 237px; height: 205px">
                            </div>
                            <div class="card-content">
                                <p class="card-title"><?=$v->$name;?></p>
                                <p class="card-price"><?=$v->price?> kzt</p>
                            </div>
                            <div class="buttons">
                                <?if($v->status !== 0):?>
                                    <button class="btn btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>">
                                        <?= Yii::t('main', 'В корзину')?>
                                    </button>
                                <?else:?>
                                    <button class="btn btn-not-basket" type="button" name="button">
                                        <?= Yii::t('main', 'Товара нет в наличии')?>
                                    </button>
                                <?endif;?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endif; ?>

    </div>
</div>