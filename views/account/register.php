<?php

use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>
<?  $name = "name".Yii::$app->session["lang"];?>
<div class="container">
    <div class="authorization">
        <div class="text-center">
            <h3><?= Yii::t('main', 'Регистрация')?></h3>
        </div>

        <?php $form = ActiveForm::begin(); ?>
        <div class="row">

            <div class="offset-lg-2 col-lg-4 col-sm-12">

                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="authorization-input">
                    <img src="/public/images/user.png" alt="">
                    <input type="text" placeholder="<?= Yii::t('main', 'Фамилия')?>"
                           name="UserProfile[surname]">
                </div>
                <div class="authorization-input">
                    <img src="/public/images/user.png" alt="">
                    <input type="text" placeholder="<?= Yii::t('main', 'Имя')?>"
                           name="UserProfile[name]">
                </div>
                <div class="authorization-input">
                    <img src="/public/images/user.png" alt="">
                    <input type="text" placeholder="<?= Yii::t('main', 'Отчество')?>"
                           name="UserProfile[father]">
                </div>
                <div class="authorization-input">
                    <input type="text" placeholder="<?= Yii::t('main', 'Почта')?>"
                           name="SignupForm[email]">
                </div>

            </div>
            <div class="col-lg-4 col-sm-12 col-lg-offset-right-2">
                <div class="authorization-input">

                    <img src="/public/images/user.png" alt="">
                    <input type="text" placeholder="<?= Yii::t('main', 'Логин')?>"
                           name="SignupForm[username]">

                </div>
                <div class="authorization-input">

                    <input type="text" placeholder="<?= Yii::t('main', 'Номер телефона')?>"
                           name="UserProfile[phone]">
                    <script>
                        $('input[name="UserProfile[phone]"]').inputmask("8(999) 999-9999");
                    </script>
                </div>
                <div class="authorization-input">
                    <input type="password" placeholder="<?= Yii::t('main', 'Пароль')?>"
                           id="password-type2" name="SignupForm[password]">
                    <img src="/public/images/show-icon.png" id="sea-pass-1" alt="" data-password="2">

                </div>
                <div class="authorization-input">
                    <input type="password" placeholder="<?= Yii::t('main', 'Повторите пароль')?>"
                           id="password-type3" name="SignupForm[password_verify]">
                    <img src="/public/images/show-icon.png" id="sea-pass-2" alt="" data-password="3">
                </div>

            </div>

        </div>
        <div class="regist-btn text-center">
            <button type="button" class="register-button btn"
                    data-message-error-1="<?= Yii::t('main', 'Упс!')?>"
                    data-message-error-2="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                <?= Yii::t('main', 'Регистрация')?>
            </button>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
