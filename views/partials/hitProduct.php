<?php

?>
<? $name = "name".Yii::$app->session["lang"];?>
<div class="owl1 owl-carousel owl-theme">
    <?foreach ($hitProduct as $value):?>
        <div class="item">
            <a href="<?=\yii\helpers\Url::to(['/catalog/product', 'url' => $value->url]) ?>">
                <img src="<?=$value->getImage();?>" alt="">
            </a>
            <p><?=$value->$name;?></p>
            <div class="price">
                <p><?=$value->price;?> KZT</p>
            </div>
            <?if($value->status !== 0):?>
                <div class="basket-btn">
                    <a class="btn-to-basket" name="button" data-id="<?=$value->id;?>"
                       data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!')?>"
                       data-message-success-2="<?= Yii::t('main', 'Продолжить покупки')?>"
                       data-message-success-3="<?= Yii::t('main', 'Оформить заказ')?>"
                       data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!')?>"
                       data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                       data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                        <?= Yii::t('main', 'В корзину')?></a>
                </div>
            <?else:?>
                <div class="no-product">
                    <a><?= Yii::t('main', 'В корзину')?></a>
                </div>
            <?endif;?>
        </div>
    <?endforeach;?>
</div>
