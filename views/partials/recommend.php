<?php

?>
<?  $name = "name".Yii::$app->session["lang"];?>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="product-recom__title">
        <?if($recommendProduct):?>
            <p><?=$mainSub[12]->$name;?></p>
        <?endif;?>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="owl1 owl-carousel owl-theme">
        <?foreach ($recommendProduct as $value):?>
            <div class="item">
                <div class="select-product-heart">
                    <form>
                        <input type="hidden" name="">
                        <p class="btn-add-favorite" data-id="<?=$value->id;?>" data-user-id="<?=Yii::$app->user->id?>"
                           data-message-error-1="<?= Yii::t('main', 'Ошибка!')?>"
                           data-message-error-2="<?= Yii::t('main', 'Чтобы добавить товар в избранное нужно войти!')?>"
                           data-message-error-3="<?= Yii::t('main', 'Упс!')?>"
                           data-message-error-4="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                            <i class="fas fa-heart"></i>
                        </p>
                    </form>
                </div>
                <a href="<?=\yii\helpers\Url::to(['/catalog/product', 'url' => $value->url]) ?>">
                    <img src="<?=$value->getImage();?>" alt="">
                </a>
                <p><?=$value->$name;?></p>
                <div class="price">
                    <p><?=$value->price;?> KZT</p>
                </div>
                <?if($value->status !== 0):?>
                    <div class="basket-btn">
                        <a class="btn-to-basket" name="button" data-id="<?=$value->id;?>"
                           data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!')?>"
                           data-message-success-2="<?= Yii::t('main', 'Продолжить покупки')?>"
                           data-message-success-3="<?= Yii::t('main', 'Оформить заказ')?>"
                           data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!')?>"
                           data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                           data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                            <?= Yii::t('main', 'В корзину')?></a>
                    </div>
                <?else:?>
                    <div class="no-product">
                        <a><?= Yii::t('main', 'В корзину')?></a>
                    </div>
                <?endif;?>
            </div>
        <?endforeach;?>
    </div>
</div>
