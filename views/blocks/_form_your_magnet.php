<?php

    /* @var $this yii\web\View */
    /* @var $form yii\widgets\ActiveForm */
    /* @var $product_id integer */

    use yii\helpers\Html;
    use yii\web\View;
    use yii\widgets\ActiveForm;
    use app\models\ProductOwn;

    $model = new ProductOwn();
    $product    = $product_id;

?>

<? $form = ActiveForm::begin([
    'id'        => 'magnet-form',
    'action'    => ['/form/upload'],
]) ?>

    <?= $form->field($model, 'product_id')->textInput([
            'hidden'    => true,
            'value'     => $product,
    ])->label(false); ?>

    <?= $form->field($model, 'image')->fileInput(
        [
            'required'      => 'required',
        ]
    )->label(false);
    ?>

    <?= $form->field($model, 'agreement')->checkbox([
        'required'      => 'required',
        'class'         => '',
        'label'         => Yii::t('main', 'Настоящим Заказчик подтверждает, что является автором изображения и/или имеет все права на использование в личных и коммерческих целях.'),
    ]) ?>

    <?= $form->field($model, 'agreement_2')->checkbox([
        'required'      => 'required',
        'label'         => Yii::t('main', 'Настоящим Заказчик соглашается с тем, что Исполнитель может отказать в исполнении заказа в случаях обнаружения нарушения авторских прав, низкого качества предоставленного изображения и/или в случаях, когда изображение противоречит этическим нормам и/или применимому законодательству. В таких случаях деньги будут возвращены Плательщику.'),
    ]) ?>

    <?= Html::submitButton(Yii::t('main', 'Далее'), ['class' => 'pb-2 pt-2 mb-5']) ?>

<? ActiveForm::end() ?>
