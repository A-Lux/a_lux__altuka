<?php

use yii\helpers\Url;

?>
<section class="questions-page">
    <div class="container">
        <!-- ХЛЕБНЫЕ КРОШКИ -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><?=$menu[0]->text;?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href=""><?=$menu[8]->text;?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END ХЛЕБНЫЕ КРОШКИ -->



            <div class="catalog">
                <?php
                if($count){?>
                    <? $text = '<span style="font-weight: bold;">'.$search .'</span>' ?>
                    <div class="" style="font-size: 22px;margin-bottom: 30px;margin-right: 500px;">
                        <?= Yii::t('main', 'Результаты поиска по запросу {text} .', [
                            'text' => $text,
                        ]);?>
                   </div>
                <?}else{?>
                    <? $text = '<span style="font-weight: bold;">'.$search .'</span>' ?>
                    <div class="" style="font-size: 22px;margin-bottom: 300px;">
                        <?= Yii::t('main', 'По запросу ничего {text} не найдено',[
                            'text' => $text,
                        ]);?>
                    </div>
                <?}?>
                <?  $name = "name".Yii::$app->session["lang"];?>
                <? if(count($product) != 0):?>

                    <div class="col-lg-9">
                        <div class="tablet-version-hide row items">
                                <?foreach($product as $v):?>
                                    <div class="col-12 col-sm-6 col-lg-4 pb-4">
                                        <div class="card">
                                            <div class="card-img">
                                                <a href="<?= \yii\helpers\Url::to(['/catalog/product', 'url' => $v->url]) ?>">
                                                    <img src="<?= $v->getImage(); ?>" alt="">
                                                </a>
                                            </div>
                                            <div class="card-content">
                                                <p class="card-title"><?= $v->$name; ?></p>
                                                <p class="card-title"><?= $v->size; ?></p>
                                                <p class="card-price">KZT <?= $v->price ?></p>
                                            </div>
                                            <div class="buttons">
                                                <? if ($v->status !== 0): ?>
                                                    <button class="btn btn-to-basket" type="button" name="button"
                                                            data-id="<?= $v->id; ?>"
                                                            data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!')?>"
                                                            data-message-success-2="<?= Yii::t('main', 'Продолжить покупки')?>"
                                                            data-message-success-3="<?= Yii::t('main', 'Оформить заказ')?>"
                                                            data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!')?>"
                                                            data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                                            data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                                                        <?= Yii::t('main', 'В корзину')?>
                                                    </button>
                                                <? else: ?>
                                                    <button class="btn btn-not-basket" type="button" name="button">
                                                        <?= Yii::t('main', 'Товара нет в наличии')?>
                                                    </button>
                                                <? endif; ?>
                                            </div>

                                        </div>
                                    </div>
                                <? endforeach;?>
                        </div>
                    </div>
                <? endif;?>

            </div>


    </div>
</section>
</div>

