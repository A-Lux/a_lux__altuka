<?php

use yii\helpers\Url;
?>
<? $name = "name" . Yii::$app->session["lang"]; ?>
<div class="slider wow bounce">
    <div class="container-flid">
        <? $name = "name" . Yii::$app->session["lang"]; ?>
        <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <? $m = 0; ?>
                <? foreach($banner as $item): ?>
                    <? $m++;?>
                    <? if($m == 1): ?>
                        <div class="carousel-item active" data-interval="10000" style="background-image: url('<?= $item->getImage(); ?>')">
                            <div class="carousel-caption">
                                <h1><?= $item->$name; ?></h1>
                            </div>
                        </div>
                    <? elseif ($m == 2): ?>
                        <div class="carousel-item" data-interval="2000" style="background-image: url('<?= $item->getImage(); ?>')">
                            <div class="carousel-caption">
                                <h1><?= $item->$name; ?></h1>
                            </div>
                        </div>
                    <? else: ?>
                        <div class="carousel-item" style="background-image: url('<?= $item->getImage(); ?>')">
                            <div class="carousel-caption">
                                <h1><?= $item->$name; ?></h1>
                            </div>
                        </div>
                    <? endif; ?>
                <? endforeach;?>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                <img src="/public/images/main-left.png" alt="">
            </a>
            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                <img src="/public/images/main-right.png" alt="">
            </a>
        </div>
    </div>
</div>
<div class="main wow bounceInLeft">
    <div class="blue-bg">
        <div class="container">
            <div class="light-title">
                <h3>
                    <?= $mainSub[0]->$name ?>
                </h3>
            </div>
            <div class="row">
                <? foreach ($bannerAdvantages as $value) : ?>
                    <div class="col-6 col-md-4 col-lg-2 p-3 p-lg-0">
                        <div class="icon-item">
                            <img src="<?= $value->getImage(); ?>" alt="">
                            <p><?= $value->$name; ?></p>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <div class="bg-white">
        <div class="container">
            <? if (!empty($hitProduct)) : ?>
                <div class="blue-title">
                    <h3><?= $mainSub[1]->$name ?></h3>
                </div>
                <?= $this->render('../partials/hitProduct', compact('hitProduct')) ?>
                <div class="catalog-btn">
                    <a href="/catalog"><?= Yii::t('main', 'В каталог') ?>
                    </a>
                </div>
            <? endif; ?>
            <div class="row wow bounceInDown">
                <div class="col-sm-7">
                    <div class="rel-img">
                        <? $m = 0; ?>
                        <? foreach($aboutImages as $item): ?>
                            <? $m++;?>
                            <? if($m == 1): ?>
                                <div class="first-img">
                                    <img src="<?= $item->getImage(); ?>" class="img-fluid" alt="">
                                </div>
                            <? elseif ($m == 2): ?>
                                <div class="second-img">
                                    <img src="<?= $item->getImage(); ?>" class="img-fluid" alt="">
                                </div>
                            <? elseif($m == 3): ?>
                                <div class="third-img">
                                    <img src="<?= $item->getImage(); ?>" class="img-fluid" alt="">
                                </div>
                            <? else: ?>
                                <div class="first-img">
                                    <img src="<?= $item->getImage(); ?>" class="img-fluid" alt="">
                                </div>
                            <? endif; ?>
                        <? endforeach;?>

                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="blue-title">
                        <? $col = "title" . Yii::$app->session["lang"];
                            $title = $about->$col;
                            $col = "description" . Yii::$app->session["lang"];
                            $description = $about->$col; ?>
                        <h4><?= $title ?></h4>
                        <p><?= $description; ?></p>
                    </div>
                    <div class="more-more-btn">
                        <a href="<?= Url::to(['/content/about']) ?>">
                            <?= Yii::t('main', 'Подробнее') ?></a>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="blue-title">
                        <h3><?= $mainSub[3]->$name ?></h3>
                    </div>
                    <div class="owl owl-carousel owl-theme">
                        <? foreach ($partners as $value) : ?>
                            <div class="item">
                                <img src="<?= $value->getImage(); ?>" class="img-fluid" alt="">
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <div class="col-sm-6 wow rotateInUpLeft">
                    <div class="blue-title">
                        <h4><?= $mainSub[5]->$name ?></h4>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contact-icon">
                                <img src="/public/images/phone.png" alt="">
                                <p>
                                    <? foreach ($phone as $value) : ?>
                                        <a href="tel:<?= $value->name ?>"><?= $value->name ?></a> <br>
                                    <? endforeach; ?>
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="contact-icon">
                                <img src="/public/images/clock.png" alt="">
                                <p><?= $mainSub[5]->$name; ?></p>
                                <? $col = "work_time" . Yii::$app->session["lang"];
                                    $work_time = $contact->$col; ?>
                                <span><?= $work_time; ?></span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="contact-icon">
                                <img src="/public/images/location.png" alt="">
                                <? $col = "address" . Yii::$app->session["lang"];
                                    $address = $contact->$col; ?>
                                <p><?= $address; ?></p>
                            </div>
                            <div class="contact-icon">
                                <img src="/public/images/mail.png" alt="">
                                <p><a href="mailto:<?= $contact->email; ?>"><?= $contact->email; ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 wow rotateInUpLeft">
                    <div class="map">
                        <?= $contact->iframe; ?>
                    </div>
                </div>
                <div class="col-sm-12 wow fadeInDownBig">
                    <div class="blue-title">
                        <h5><?= $mainSub[6]->$name; ?></h5>
                        <p class="text-center">
                            <?= Yii::t('main', 'Подпишитесь на рассылку и узнавайте первым о новых акциях и спецпредложениях магазина!') ?>
                        </p>
                    </div>
                </div>
                <div class="offset-lg-2 col-sm-8 offset-md-2 wow wobble">
                    <form action="">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                        <div class="mail-input">
                            <input type="email" name="Subscribe[email]" placeholder="<?= Yii::t('main', 'Ваш E-mail') ?>">

                            <button type="button" class="btn-subscribe"
                                    data-message-success="<?= Yii::t('main', 'Вы успешно подписались на рассылку') ?>"
                                    data-message-error-1="<?= Yii::t('main', 'Упс!') ?>"
                                    data-message-error-2="<?= Yii::t('main', 'Что-то пошло не так') ?>">
                                <i class="fab fa-telegram-plane"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>