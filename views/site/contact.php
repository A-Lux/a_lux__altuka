<? $name = "name" . Yii::$app->session["lang"]; ?>
<!-- contact -->
<section class="contact-page wow lightSpeedIn">
    <div class="container">
        <!-- breadcrumbs -->
        <? $text = "text" . Yii::$app->session["lang"]; ?>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><?= Yii::$app->view->params['main']; ?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href=""><?= $model->$text; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end breadcrumbs -->
        <!-- contact title -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <p class="contact-title"><?= $model->$text; ?></p>
            </div>
        </div>
        <!-- end contact title -->
        <!-- box contact -->
        <? $col = "address" . Yii::$app->session["lang"];
        $address = $contact->$col;
        $col = "work_time" . Yii::$app->session["lang"];
        $work_time = $contact->$col; ?>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="box-contact">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="contact-list-info">
                            <div class="col-sm-12 col-md-12 offset-lg-1 col-lg-11">
                                <ul class="contact-location">
                                    <li class="contact-location-title">
                                        <?= Yii::t('main', 'Адрес') ?>:
                                    </li>
                                    <li class="contact-location-info">
                                        <?= $address; ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-6 offset-lg-1 col-lg-5">
                                <ul class="contact-phone">
                                    <li class="contact-phone-title">
                                        <?= Yii::t('main', 'Телефон') ?>:
                                    </li>
                                    <? foreach ($phone as $value) : ?>
                                        <li class="contact-phone-info">
                                            <?= $value->name; ?>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-6 offset-lg-1 col-lg-5">
                                <ul class="contact-clock">
                                    <li class="contact-clock-title">
                                        <?= Yii::t('main', 'График работы') ?>:
                                    </li>
                                    <li class="contact-clock-info">
                                        <?= $work_time; ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-12 offset-lg-1 col-lg-5">
                                <ul class="contact-mail">
                                    <li class="contact-mail-title">
                                        <?= Yii::t('main', 'Почта') ?>:
                                    </li>
                                    <li class="contact-mail-info">
                                        <a href="mailto:<?= $contact->email; ?>"><?= $contact->email; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="map">
                            <?= $contact->iframe; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end box contact -->
    </div>
</section>