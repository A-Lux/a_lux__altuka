
<!-- contact -->
<section class="questions-page wow bounceInDown">
    <div class="container">
        <!-- breadcrumbs -->
        <?  $text = "text".Yii::$app->session["lang"];?>
        <?  $name = "name".Yii::$app->session["lang"];?>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><?=Yii::$app->view->params['main'];?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href=""><?=$model->$text;?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end breadcrumbs -->
        <!-- contact title -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <p class="questions-title"><?=$mainSub[7]->$name?></p>
            </div>
        </div>
        <!-- end contact title -->
        <!-- box question content -->
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="questions-box-content">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <ul class="questions-box-tab px-0">
                            <!-- tab box text tab -->
                            <? foreach($faq as $value):?>
                                <? $col = "name".Yii::$app->session["lang"];$name = $value->$col;?>
                                <? $col = "content".Yii::$app->session["lang"];$content = $value->$col;?>
                                <li>
                                    <a href="#box-answer<?=$value->id?>"><?=$name?><img
                                        src="/public/images/left-questions.png"></a> 
                                </li>
                                <!-- end tab box text tab -->
                                <!-- box text dropdown -->
                                <div class="questions-box-info" id="box-answer<?=$value->id?>" style="display: none;">
                                    <p><?=$content;?></p>
                                </div>
                            <!-- end box text dropdown -->
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end box question content -->
    </div>
</section>
<?  $col = "name".Yii::$app->session["lang"];?>
<a class="top-btn smoothscroll" href="#top"><?= Yii::t('main', 'Вверх')?> <img src="images/top-btn-up.png"></a>

