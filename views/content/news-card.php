
    <section class="news-card-page">
        <div class="container">
            <div class="row">
                <?  $text = "text".Yii::$app->session["lang"];?>
                <? $col = "name".Yii::$app->session["lang"];$name = $news->$col;?>
                <? $col = "content".Yii::$app->session["lang"];$content = $news->$col;?>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/"><?=Yii::$app->view->params['main'];?></a>
                        </li>
                        <li>
                            <a href="#">/</a>
                        </li>
                        <li>
                            <a href=""><?=$name?></a>
                        </li>
                    </ul>
                </div>
            </div> <!-- end row-->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p class="news-card-page__title"><?=$name?></p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p class="text-muted news-date"><?=$news->getDate();?></p>
                </div>
            </div> <!-- end row -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="news-card__box">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <img src="<?=$news->getImage();?>">
                            <p><?=$content;?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
