<?php

use yii\widgets\LinkPager;
use yii\widgets\Pjax;

?>
<? $name = "name".Yii::$app->session["lang"];?>
    <div class="feedback wow fadeInDownBig">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title__h2">
                        <?  $text = "text".Yii::$app->session["lang"];?>
                        <?=$model->$text?>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="feedback-title">
                        <?= Yii::t('main', 'Оставить комментарий на сайте')?>
                    </h3>
                </div>
            </div>
            <form action="/site/comment">
                <div class="row">
                    <div class="col-sm-12 col-lg-3 px-0">
                        <div class="feedback-form">
                            <p><?= Yii::t('main', 'Имя')?></p>
                            <input type="text" name="Feedback[name]" value="<?=$profile->name;?>" required>
                            <script>
                                $('input[name="Feedback[name]"]').keyup(function(e) {
                                    var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
                                    if (regex.test(this.value) !== true)
                                        this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, '');
                                });
                            </script>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-3 px-0">
                        <div class="feedback-form">
                            <p><?= Yii::t('main', 'Телефон')?></p>
                            <input type="text" name="Feedback[phone]" value="<?=$profile->phone;?>" required>
                            <script>
                                $('input[name="Feedback[phone]"]').inputmask("8(999) 999-9999");
                            </script>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-3 px-0">
                        <div class="feedback-form">
                            <p><?= Yii::t('main', 'Почта')?></p>
                            <input type="text" name="Feedback[email]" value="<?=Yii::$app->user->identity->email;?>" required>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-3 px-0">
                        <div class="feedback-form">
                            <p><?= Yii::t('main', 'Поставьте свою оценку')?></p>
                            <div class="stars-rate raiting-r rating">
                            <input id="star5" name="Feedback[rating]" type="radio" value="5" class="radio-btn hide"/>
                            <label for="star5" style="font-size: 20px;">&#x2605</label>
                            <input id="star4" name="Feedback[rating]" type="radio" value="4" class="radio-btn hide"/>
                            <label for="star4" style="font-size: 20px;">&#x2605</label>
                            <input id="star3" name="Feedback[rating]" type="radio" value="3" class="radio-btn hide"/>
                            <label for="star3" style="font-size: 20px;">&#x2605</label>
                            <input id="star2" name="Feedback[rating]" type="radio" value="2" class="radio-btn hide"/>
                            <label for="star2" style="font-size: 20px;">&#x2605</label>
                            <input id="star1" name="Feedback[rating]" type="radio" value="1" class="radio-btn hide"/>
                            <label for="star1" style="font-size: 20px;">&#x2605</label>
                            <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="feedback-form">
                        <h2><?= Yii::t('main', 'Ваш комментарий')?></h2>
                        <textarea name="Feedback[comment]" id="" 
                                      placeholder="<?= Yii::t('main', 'Введите текст сообщения')?>"
                                  required></textarea>
                    </div>
                </div>
                <div class="row">
                    <button type="button" class="submit-button sendReviews"
                            data-message-success-1="<?= Yii::t('main', 'Благодарим за ваше обращение')?>">
                        <?= Yii::t('main', 'Отправить')?></button>
                </div>
            </form>
            <?foreach($feedback['data'] as $value):?>
                <div class="row">
                    <div class="col-sm-9">
                        <p class="comment-data">
                            <?=$value->getDate();?>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-9">
                        <div class="comment">
                            <h3><?=$value->name;?></h3>
                            <div class="stars-display">
                                <? for ($i = 1; $i <= $value->rating; $i++): ?>
                                    <label for="star5" style="color: #00aab7; font-size: 20px;">&#x2605</label>
                                <? endfor; ?>
                                <? for ($i = $value->rating+1; $i <= 5; $i++): ?>
                                    <label for="star5" style="font-size: 20px;">&#x2605</label>
                                <? endfor; ?>
                            </div>
                            <p class="comment-text">
                                <?=$value->comment;?>
                            </p>
                        </div>
                    </div>
                </div>
            <?endforeach;?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="pagination-feedback">
                        <ul>
                            <?=LinkPager::widget([
                                'pagination' => $feedback['pagination'],
                                'activePageCssClass' => 'active',
                                'hideOnSinglePage' => true,
                                'prevPageLabel' => '',
                                'prevPageCssClass' => ['class' => 'prev-page'],
                                'nextPageLabel' => '&raquo;',
                                'nextPageCssClass' => ['class' => 'next-page'],
                                // Настройки контейнера пагинации
                                'options' => [
                                    'tag' => 'div',
                                    'class' => 'pagination',
                                ],

                                // Настройки классов css для ссылок

                            ]);
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



