<?php

use app\controllers\ContentController;
?>
    <section id="top"></section>

    <section class="news">
        <div class="container">
            <!-- breadcrumbs -->
            <? $text = "text" . Yii::$app->session["lang"]; ?>
            <div class="row wow bounceInLeft">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumbs">
                        <li>
                            <a href="/"><?= Yii::$app->view->params['main']; ?></a>
                        </li>
                        <li>
                            <a href="">/</a>
                        </li>
                        <li>
                            <a href=""><?= $model->$text; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end breadcrumbs -->
            <!-- contact title -->
            <div class="row wow flash">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <p class="news-title"><?= $model->$text; ?></p>
                </div>
            </div>
            <!-- end contact title -->
            <!-- news content -->
            <div class="row wow fadeInDownBig">
                <div class="news-contact-box">
                    <? $count = 0; ?>
                    <? foreach ($news as $value) : ?>
                        <? $col = "name" . Yii::$app->session["lang"];
                            $name = $value->$col; ?>
                        <? $col = "content" . Yii::$app->session["lang"];
                            $content = $value->$col; ?>
                        <? $count++; ?>
                        <? if ($count < 7) : ?>
                            <? if ($count % 4 == 1 || $count % 4 == 2) : ?>
                                <div class="col-lg-3 col-md-6 col-sm-12 px-0">
                                    <div class="card">
                                        <img src="<?= $value->getImage(); ?>">
                                    </div>
                                </div> <!-- end card new img -->
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-muted news-date"><?=$value->getDate();?></p>
                                            <p class="card-text-title"><?=$name;?></p>
                                            <p class="card-text"><?= ContentController::cutStr($content, 200);?></p>
                                            <a class="card-btn-more" href="<?=\yii\helpers\Url::to(['/content/news-card', 'id' => $value->id])?>">
                                                <?= Yii::t('main', 'Читать далее')?></a>
                                        </div>
                                    </div>
                                </div> <!-- end card new text -->
                            <? endif; ?>
                            <? if ($count % 4 == 3 || $count % 4 == 0) : ?>
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <p class="text-muted news-date"><?=$value->getDate();?></p>
                                            <p class="card-text-title"><?=$name;?></p>
                                            <p class="card-text"><?= ContentController::cutStr($content, 200);?></p>
                                            <a class="card-btn-more" href="<?=\yii\helpers\Url::to(['/content/news-card', 'id' => $value->id])?>">
                                                <?= Yii::t('main', 'Читать далее')?></a>
                                        </div>
                                    </div>

                                </div> <!-- end card new text -->
                                <div class="col-lg-3 col-md-6 col-sm-12 px-0">
                                    <div class="card">
                                        <img src="<?= $value->getImage(); ?>">
                                    </div>
                                </div> <!-- end card new img -->
                            <? endif; ?>
                        <? endif; ?>
                    <? endforeach; ?>



                    <div class="box-more-card" id="morecard-news" style="display: none;">
                        <!-- start box more card -->
                        <? $count = 0; ?>
                        <? foreach ($news as $value) : ?>
                            <? $col = "name" . Yii::$app->session["lang"];
                                $name = $value->$col; ?>
                            <? $col = "content" . Yii::$app->session["lang"];
                                $content = $value->$col; ?>
                            <? $count++; ?>
                            <? if ($count > 6) : ?>
                                <? if ($count % 4 == 1 || $count % 4 == 2) : ?>
                                    <div class="col-lg-3 col-md-6 col-sm-12 px-0">
                                        <div class="card">
                                            <img src="<?= $value->getImage(); ?>">
                                        </div>
                                    </div> <!-- end card new img -->
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <p class="text-muted news-date"><?=$value->getDate();?></p>
                                                <p class="card-text-title"><?=$name;?></p>
                                                <p class="card-text"><?= ContentController::cutStr($content, 200);?></p>
                                                <a class="card-btn-more" href="<?=\yii\helpers\Url::to(['/content/news-card', 'id' => $value->id])?>">
                                                    <?= Yii::t('main', 'Читать далее')?></a>
                                            </div>
                                        </div>
                                    </div> <!-- end card new text -->
                                <? endif; ?>
                                <? if ($count % 4 == 3 || $count % 4 == 0) : ?>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <p class="text-muted news-date"><?=$value->getDate();?></p>
                                                <p class="card-text-title"><?=$name;?></p>
                                                <p class="card-text"><?= ContentController::cutStr($content, 200);?></p>
                                                <a class="card-btn-more" href="<?=\yii\helpers\Url::to(['/content/news-card', 'id' => $value->id])?>">
                                                    <?= Yii::t('main', 'Читать далее')?></a>
                                            </div>
                                        </div>

                                    </div> <!-- end card new text -->
                                    <div class="col-lg-3 col-md-6 col-sm-12 px-0">
                                        <div class="card">
                                            <img src="<?= $value->getImage(); ?>">
                                        </div>
                                    </div> <!-- end card new img -->
                                <? endif; ?>
                            <? endif; ?>
                        <? endforeach; ?>

                    </div> <!-- end box more card -->
                    <? $count = 0; ?>
                    <? foreach ($news as $value) : ?>
                        <? $count++; ?>
                        <? if ($count > 6) : ?>
                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="box-btn-more">
                                    <a class="more-btn" href="#morecard-news" data-tab-new="1">
                                        <?= Yii::t('main', 'Показать еще') ?></a>
                                </div>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
            <!-- end news content -->
            <!-- search box -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p class="search-box-title"><?= Yii::t('main', 'Подпишитесь на рассылку и узнавайте о новостях магазина') ?></p>
                </div>
                <div class="offset-lg-2 col-sm-8 offset-md-2">
                    <form action="">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                        <div class="mail-input">
                            <input type="email" name="Subscribe[email]" placeholder="<?= Yii::t('main', 'Ваш E-mail') ?>">
                            <button type="button" class="btn-subscribe"
                                    data-message-success="<?= Yii::t('main', 'Вы успешно подписались на рассылку') ?>"
                                    data-message-error-1="<?= Yii::t('main', 'Упс!') ?>"
                                    data-message-error-2="<?= Yii::t('main', 'Что-то пошло не так') ?>">
                                <i class="fab fa-telegram-plane"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end serch box -->
        </div>
    </section>

    <a class="top-btn smoothscroll" href="#top"><?= Yii::t('main', 'Вверх') ?> <img src="/public/images/top-btn-up.png"></a>
    <!-- JS
   ================================================== -->