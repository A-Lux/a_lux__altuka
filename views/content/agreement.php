<?php

use yii\helpers\Url;
?>

<section class="agreement-page">
    <div class="container">
        <!-- breadcrumbs -->
        <?  $text = "text".Yii::$app->session["lang"];?>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><?=Yii::$app->view->params['main'];?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href=""><?=$model->$text;?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end breadcrumbs -->
        <!-- contact title -->
        <? $col = "title".Yii::$app->session["lang"];$title = $agreement->$col;?>
        <? $col = "content".Yii::$app->session["lang"];$content = $agreement->$col;?>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <p class="agreement-title"><?=$title?></p>
            </div>
        </div>
        <!-- end contact title -->
        <!-- agreement contact -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="agreement__box">
                    <?=$content;?>
                </div>
            </div>
        </div>
        <!-- end agreement contact -->
    </div>
</section>
