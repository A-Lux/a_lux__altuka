<?php

use yii\helpers\Url;

?>
<? $name = "name".Yii::$app->session["lang"];?>
<? $text = "text".Yii::$app->session["lang"];?>

<div class="content">
    <header class="header">
        <div class="light-menu">
            <div class="container">
               <div class="row">
                   <div class="col-sm-4">
                       <div class="menu">
                           <ul>
                            <?foreach(Yii::$app->view->params['menu'] as $value):?>
                                <? $col = "text".Yii::$app->session["lang"];?>
                               <li><a href="<?= Url::to([$value->url])?>"><?=$value->$col;?></a></li>
                            <?endforeach;?>
                           </ul>
                       </div>
                   </div>
                   <div class="col-sm-8">

                       <div class="phone">
                           <a href="tel:<?=Yii::$app->view->params['phone'][0]->name;?>"><?=Yii::$app->view->params['phone'][0]->name;?></a>
                       </div>
                       <div class="">
<!--                       <ul class="lang_menu">-->
<!--                           <li><a href="/lang/?url=kz" style="color:--><?//=Yii::$app->session["lang"] == '_kz'?'#ccb04d':'##003d60';?><!--">Kz</a></li>-->
<!--                           <li><a href="/lang/?url=ru" style="color:--><?//=Yii::$app->session["lang"] == ''?'#ccb04d':'##003d60';?><!--">Ru</a></li>-->
<!--                           <li><a href="/lang/?url=en" style="color:--><?//=Yii::$app->session["lang"] == '_en'?'#ccb04d':'##003d60';?><!--">En</a></li>-->
<!--                       </ul>-->
                       </div>

                   </div>
                  
               </div>
            </div>
        </div>
        <div class="blue-menu">
            <div class="container">
                <div class="row">
                    <div class="col-lg-1 px-xl-3 pr-lg-0 pl-lg-2 col-md-2 col-5 margin__logo">
                        <div class="logo">
                            <a href="/"><img src="<?=Yii::$app->view->params['logo'][0]->getImage();?>" alt=""></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-5">
                        <a href="/catalog">
                            <div class="catalog-menu-btn">
                                <div class="hamburger hamburger--spring">
                                    <div class="hamburger-box">
                                        <div class="hamburger-inner"></div>
                                    </div>
                                </div>
                                <p><?=Yii::$app->view->params['menuAll'][5]->$text;?></p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-5 col-md-3">
                        <form action="/search/index">
                            <div class="search">
                                <input type="text" placeholder="<?= Yii::t('main','Что вы ищете?')?>"
                                       name="text">
                                <button><img src="/public/images/search.png" alt=""></button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-2 col-md-2 col-6">
                        <div class="basket">
                            <a href="/card"><img src="/public/images/basket.png" alt=""><?=Yii::$app->view->params['menuAll'][6]->$text;?></a>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-3 col-6">
                        <div class="dropdown">
                            <div class=" dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="/public/images/user.png" alt=""><?= Yii::$app->user->isGuest ? 'Профиль' :  Yii::$app->user->identity->username ;?>
                            </div>
                            <? if(Yii::$app->user->isGuest):?>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/account/sign-in">
                                        <?= Yii::t('main', 'Войти')?>
                                       </a>
                                    <a class="dropdown-item" href="/account/sign-up">
                                        <?= Yii::t('main', 'Регистрация')?>
                                        </a>
                                </div>
                            <?else:?>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="/account">
                                        <?= Yii::t('main', 'Профиль')?>
                                        </a>
                                    <a class="dropdown-item" href="/account/logout">
                                        <?= Yii::t('main', 'Выйти')?>
                                        </a>
                                </div>
                            <?endif;?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

</header>

