<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;

PublicAsset::register($this);



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script src="/public/js/jquery-3.2.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
</head>
<body>
<?php $this->beginBody() ?>

<!-- HEADER -->
<?=$this->render('_header')?>
<!-- END HEADER -->
<?php if(Yii::$app->session->getFlash('paySuccess')):?>
    <div id="success" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        Вы успешно оплатили заказ!<br> Скоро с Вами свяжется менеджер!
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('payError0')):?>
    <div id="error" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        Ошибка!<br> Вы не оплатили заказ!
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('payError3')):?>
    <div id="error" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        Ошибка!<br> Не получилось оплатить заказ. Попробуйте позднее!
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('error')):?>
    <div id="error" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        Внутреннее ошибка сервера!
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('successUpload')):?>
    <div id="success" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        <?= Yii::$app->session->get('successUpload'); ?>
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('errorUpload')):?>
    <div id="error" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('error').style.display = 'none';">&times;</a>
        <?= Yii::$app->session->get('errorUpload'); ?>
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('successDelete')):?>
    <div id="success" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('success').setAttribute('style','display: none;');">&times;</a>
        <?= Yii::$app->session->get('successDelete'); ?>
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('errorDelete')):?>
    <div id="error" class="message">
        <a id="close" title="Закрыть"  href="#" onClick="document.getElementById('error').style.display = 'none';">&times;</a>
        <?= Yii::$app->session->get('errorDelete'); ?>
    </div>
<?php endif;?>

<?=$content?>

<!-- FOOTER -->
<?=$this->render('_footer')?>
<!-- END FOOTER -->

<?php $this->endBody() ?>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    if (!String.prototype.includes) {
        String.prototype.includes = function (search, start) {
            'use strict';
            if (typeof start !== 'number') {
                start = 0;
            }

            if (start + search.length > this.length) {
                return false;
            } else {
                return this.indexOf(search, start) !== -1;
            }
        };
    }

    var path = window.location.pathname;

    if(!path.includes('/catalog')){
        localStorage.removeItem('anchor');
    }
</script>

</html>

<?php $this->endPage() ?>
