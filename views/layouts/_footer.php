<?php

use yii\helpers\Url;
?>

</body>
<footer class="main-footer wow bounceInUp">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-sm-3">
                            <div class="fot-logo">
                                <a href="/">
                                    <img src="<?=Yii::$app->view->params['logo'][1]->getImage();?>" alt="">
                                </a>
                                <div class="socials">
                                    <a href="<?=Yii::$app->view->params['contact']->facebook;?>"><i class="fab fa-facebook-f"></i></a>
                                    <a href="<?=Yii::$app->view->params['contact']->instagram;?>"><i class="fab fa-instagram"></i></a>
                                    <a href="<?=Yii::$app->view->params['contact']->whatsap;?>"><i class="fab fa-whatsapp"></i></a>
                                    <a href="<?=Yii::$app->view->params['contact']->youtube;?>"><i class="fab fa-youtube"></i></a>
                                    <a href="<?=Yii::$app->view->params['contact']->vk;?>"><i class="fab fa-vk"></i></a>
                                </div>
                                <div class="rights">
                                    <?  $copyright = "copyright".Yii::$app->session["lang"];?>
                                    <p><?=Yii::$app->view->params['logo'][0]->$copyright;?></p>
                                </div>
                            </div>
                            <p>
                                <a href="https://a-lux.kz">
                                Создание сайта:
                                    <img src="/public/images/a-lux.png" alt="">
                                </a>
                            </p>
                        </div>
                        <div class="offset-lg-1 col-lg-2 col-sm-3">
                            <div class="fot-text">
                                <?  $text = "text".Yii::$app->session["lang"];?>
                                <?  $address = "address".Yii::$app->session["lang"];?>
                                <p><?=Yii::$app->view->params['menuAll'][4]->$text;?></p>
                                <p><?=Yii::$app->view->params['contact']->$address;?></p>
                                <p><a href="tel:<?=Yii::$app->view->params['phone'][1]->name;?>"><?=Yii::$app->view->params['phone'][1]->name;?></a>
                                    <a href="mailto:<?=Yii::$app->view->params['contact']->email;?>"><?=Yii::$app->view->params['contact']->email;?></a></p>
                            </div>
                        </div>
                        <div class="offset-lg-2 col-lg-2 col-sm-3">
                            <div class="fot-list">
                                <ul>
                                    <?foreach(Yii::$app->view->params['catalog'] as $value):?>
                                        <?  $col = "name".Yii::$app->session["lang"];$name = $value->$col;?>
                                        <li><a href="/catalog" class="category_filter" data-id="<?=$value->id;?>"><?=$name?></a></li>
                                    <?endforeach;?>
                                   
                                </ul>
                            </div>
                        </div>
                        <div class="offset-lg-1 col-lg-2 col-sm-3">
                            <div class="fot-list">
                                <ul>
                                    <li><a href="/content/about"><?= Yii::$app->view->params['menuAll'][9]->$text?></a></li>
                                    <li><a href="/site/contact"><?= Yii::$app->view->params['menuAll'][4]->$text?></a></li>
                                    <li><a href="/content/agreement"><?= Yii::$app->view->params['menuAll'][10]->$text?></a></li>
                                    <li><a href="/catalog"><?= Yii::$app->view->params['menuAll'][5]->$text?></a></li>
                                    <li><a href="<?= Yii::$app->view->params['offer']->getFile();?>" target="_blank">
                                            <?= Yii::$app->view->params['offer']->getName()?></a></li>
                                </ul>
                            </div>
                            <div class="payment">
                                <img src="/public/images/alfa.png" alt="" class="alfa">
                                <img src="/public/images/master.png" alt="">
                                <img src="/public/images/visa.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>