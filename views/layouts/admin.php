<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/index'])?>" class="logo">
            <span class="logo-mini"></span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
<!--        --><?//= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                            <!-- The user image in the navbar-->
                            <img src="/private/dist/img/user.png" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs"><?=Yii::$app->user->identity->username?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">

                                <p>
                                    <?=Yii::$app->user->identity->username?> - Administrator
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/admin/admin-profile/index" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Выйти</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/private/dist/img/user.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?=Yii::$app->user->identity->username?></p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
                </div>
            </form>
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
            </ul>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [
                        ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/admin/menu/index'],'active' => $this->context->id == 'menu'],

                        [
                            'label' => 'Главная страница',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Логотип', 'icon' => 'th', 'url' => ['/admin/logo/index'],'active' => $this->context->id == 'logo'],
                                ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/admin/banner/index'],'active' => $this->context->id == 'banner'],
                                ['label' => 'Преимущества', 'icon' => 'suitcase', 'url' => ['/admin/banner-advantages/index'],'active' => $this->context->id == 'banner-advantages'],
                                ['label' => 'Заголовки контента', 'icon' => 'fa fa-user', 'url' => ['/admin/mainsub/index'],  'active' => $this->context->id == 'mainsub'],
                                ['label' => 'Наши партнеры', 'icon' => 'fa fa-user', 'url' => ['/admin/partners/index'], 'active' => $this->context->id == 'partners'],
                                ['label' => 'Договор оферты', 'icon' => 'fa fa-user', 'url' => ['/admin/offer/index'], 'active' => $this->context->id == 'offer'],

                            ],
                        ],
                        [
                            'label' => 'Страницы',
                            'icon' => 'book',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Новости', 'icon' => 'calendar', 'url' => ['/admin/news/index'],'active' => $this->context->id == 'news'],
                                ['label' => 'Отзывы', 'icon' => 'envelope-open', 'url' => ['/admin/feedback/index'],'active' => $this->context->id == 'feedback'],
                                ['label' => 'Условия доставки', 'icon' => 'fa fa-user', 'url' => ['/admin/agreement/index'], 'active' => $this->context->id == 'agreement'],
                                ['label' => 'FAQ', 'icon' => 'gavel', 'url' => ['/admin/faq/index'],'active' => $this->context->id == 'faq'],
                                ['label' => 'О компании', 'icon' => 'info', 'url' => ['/admin/about/index'], 'active' => $this->context->id == 'about'],
                                ['label' => 'Изображение о компании', 'icon' => 'fa fa-user', 'url' => ['/admin/about-images/index'], 'active' => $this->context->id == 'about-images'],

                            ],
                        ],

                        [
                            'label' => 'Продукция',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Каталог ', 'icon' => 'fa fa-user', 'url' => ['/admin/catalog/index'],'active' => $this->context->id == 'catalog'],
                                ['label' => 'Продукты', 'icon' => 'fa fa-user', 'url' => ['/admin/products/index'], 'active' => $this->context->id == 'products'],
//                                ['label' => 'Доп. изображение продуктов', 'icon' => 'fa fa-user', 'url' => ['/admin/products-image/index'], 'active' => $this->context->id == 'products-image'],
//                                ['label' => 'Цвет продукта', 'icon' => 'fa fa-user', 'url' => ['/admin/products-color/index'], 'active' => $this->context->id == 'products-color'],
                                //['label' => 'Тематика', 'icon' => 'fa fa-user', 'url' => ['/admin/subjects/index'], 'active' => $this->context->id == 'subjects'],
                            ],
                        ],
                        [
                            'label' => 'Контакты',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contact/index'],  'active' => $this->context->id == 'contact'],
                                ['label' => 'Телефоны', 'icon' => 'fa fa-user', 'url' => ['/admin/telephone/index'],  'active' => $this->context->id == 'telephone'],
                                ['label' => 'Эл. почта для связи', 'icon' => 'fa fa-user', 'url' => ['/admin/emailforrequest/index'],'active' => $this->context->id == 'emailforrequest'],
                            ],
                        ],
                        ['label' => 'Курсы валют', 'icon' => 'money', 'url' => ['/admin/currency/index'],'active' => $this->context->id == 'currency'],
                        [
                            'label' => 'Заказы',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Заказы', 'icon' => 'fa fa-user', 'url' => ['/admin/orders/index'],'active' => $this->context->id == 'orders'],
                                ['label' => 'Цена заказа', 'icon' => 'fa fa-user', 'url' => ['/admin/price-minimum/index'], 'active' => $this->context->id == 'price-minimum'],
                                ['label' => 'Скидка', 'icon' => 'fa fa-user', 'url' => ['/admin/discount/index'], 'active' => $this->context->id == 'discount'],
                                ['label' => 'Информация по заказу', 'icon' => 'fa fa-user', 'url' => ['/admin/info-basket/index'], 'active' => $this->context->id == 'info-basket'],
                                ['label' => 'Реквизиты', 'icon' => 'fa fa-user', 'url' => ['/admin/requisites/index'], 'active' => $this->context->id == 'requisites'],
                            ],
                        ],
                        ['label' => 'Рассылка', 'icon' => 'envelope', 'url' => ['/admin/subscribe/index'],'active' => $this->context->id == 'subscribe'],
                        ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/admin/source-message/index'],'active' => $this->context->id == 'source-message'],
                    ],
                ]
            ) ?>
        </section>
    </aside>



    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?=$content?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2007-2019 <a href="https://a-lux.kz">A-lux</a>.</strong>
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor( editor );
    })
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
