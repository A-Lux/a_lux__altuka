

<div style="padding:0.3rem 1%;">
    <div>
        <div style="width: 48%;float: left;font-size: 12px;">
            <div>
                <h3><b>ТОО «AL2KA.kz»</b></h3>
                <p>Республика Казахстан, <br> г. Алматы, Бостандыкский район,  <br>С/Т «Свежесть», д. 32 <br>  +7 701 508 00 76
                    <br><a href="https://al2ka.kz/" style="text-decoration: underline">www.al2ka.kz</a></p>

            </div>
            <div>
                <p>
                    <b>Счёт на оплату* № <?=$order->order_id;?></b> <br>
                    <b>Дата: <?=$order->created;?></b><br>
                    <b>Договор: Публичная оферта (см. веб-сайт)</b>
                </p>

            </div>
            <div>
                <h6><b>Детали:</b></h6>
            </div>
        </div>
        <div  style="width: 48%;float: right;font-size: 12px;" >
            <div class="col-xl-6">
                <h3><b>AL2KA.kz LLP</b></h3>
                <p>Republic of Kazakhstan, <br> Almaty, Bostandyk region, <br>32, Svezhest <br>+7 701 508 00 76
                    <br><a href="https://al2ka.kz/" style="text-decoration: underline">www.al2ka.kz</a></p>

            </div>
            <div>
                <p>
                    <b>Proforma Invoice* # <?=$order->order_id;?></b><br>
                    <b>Date: <?=$order->created;?></b><br>
                    <b>Contract: Public offer (refer to web-site)</b>
                </p>
            </div>
            <div>
                <h6><b>Details:</b></h6>
            </div>
        </div>
    </div>


    <div >
        <table class="table table-hover table-altuka table-responsive">
            <thead  style="font-size: 12px;">
                <tr>
                    <th >№</th>
                    <th >Код товара</th>
                    <th >Название</th>
                    <th>Размер</th>
                    <th>Кол-во</th>
                    <th>Цена</th>
                    <th>Сумма</th>
                </tr>
            </thead>
            <tbody  style="font-size: 12px;">
            <tr>
                <th scope="row">#</th>
                <th>Code</th>
                <th>Description</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Amount</th>
            </tr>
            <? $m=0;?>
            <? foreach ($products as $v):?>
                <? $m++;?>
                <tr>
                    <th scope="row"><?=$m;?></th>
                    <td><?=$v->product->articul;?></td>
                    <td><?=$v->product->name;?></td>
                    <td><?=$v->product->size;?></td>
                    <td><?=$v->count;?></td>
                    <td>KZT<?=$v->product->price;?> </td>
                    <td>KZT<?=$v->product->price*$v->count;?></td>
                </tr>
            <? endforeach;?>
            </tbody>
        </table>
        <br>

        <div style="font-size: 12px;">
            <div style="width: 45%;float: left;">
                <p>
                    <b>Итого к оплате: </b>
                    <b>KZT <?=$order->sum;?></b><br>
                </p>
            </div>
            <div  style="width: 45%;float: right;">
                <p>
                    <b>Total for wire transfer: </b>
                    <b>KZT <?=$order->sum;?></b><br>
                </p>
            </div>
        </div>
        <div style="font-size: 12px;">
            <h6><b>
                    Equivalent amounts: <?=$order->getConversion();?>
            </b></h6>
        </div>
        <br>
        <div style="font-size: 12px;">
            <div style="width: 45%;float: left;"><p>*Данный счёт действителен 2 рабочих дня</p></div>
            <div  style="width: 45%;float: right;"><p>*This document is valid for 2 business days</p></div>
        </div>
        <br><br><br><br>

        <div style="font-size: 12px;">
            <p>
                <b>Детали для банковского перевода:</b><br><br>
                <b>KZT:</b> ТОО «AL2KA.kz», БИН 190440010137, счёт KZ78 9470 3989 2038 0414, Кбе 17, КНП 710, АО ДБ
                «Альфа-Банк»,
                БИК ALFAKZKA, БИН Банка 941240000341, в назначении платежа обязательно укажите номер счёта на оплату
            </p>
        </div>
        <br><br>

        <div style="font-size: 12px;">
            <p>
                <b>Wire transfer details:</b><br><br>
                <b>RUR:</b> ТОО «AL2KA.kz», БИН 190440010137, счёт KZ85 9470 6430 0353 7852, АО ДБ «Альфа-Банк», БИК
                ALFAKZKA, в назначении платежа обязательно укажите номер счёта на оплату <br><br>
                <b>EUR:</b> AL2KA.kz LLP, IBAN: KZ48 9470 9780 7283 5762, Bank: Alfa-Bank Branch JSC, Almaty,<br>
                Kazakhstan, SWIFT: ALFAKZKA, please insert proforma invoice number in wire transfer details <br><br>
                <b>USD:</b>AL2KA.kz LLP, IBAN: KZ62 9470 8409 1331 1640, Bank: Alfa-Bank Branch JSC, Almaty, <br> Kazakhstan,
                SWIFT: ALFAKZKA, please insert proforma invoice number in wire transfer details <br><br>
                <b>CNY:</b> AL2KA.kz LLP, IBAN: KZ42 9470 1560 0222 6672, Bank: Alfa-Bank Branch JSC, Almaty, <br> Kazakhstan,
                SWIFT: ALFAKZKA, please insert proforma invoice number in wire transfer details <br><br>
            </p>

        </div>

    </div>

</div>


