<div class="cart wow bounceInRight">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <? $title = "title" . Yii::$app->session["lang"]; ?>
                <? $text = "text" . Yii::$app->session["lang"]; ?>
                <? $name = "name" . Yii::$app->session["lang"]; ?>
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><?= Yii::$app->view->params['main']; ?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href=""><?= $model->$text; ?></a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <p class="contact-title"><?= $model->$text; ?></p>
            </div>
        </div>

        <? if ($info->status == 1) : ?>
            <div class="row mt-3">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <div class="white-background">
                        <div class="vertical-column-2">
                            <p><?= $info->$title; ?></p>
                            <p><?= $info->$text; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="white-background">
                    <?php if (count($_SESSION['basket']) == 0 && count($_SESSION['basket-product-own']) == 0) : ?>
                        <div class="white-background__column--flex">
                            <div class="vertical-column-2">
                                <p><?= Yii::t('main', 'Ваша корзина пуста!') ?></p>
                            </div>
                        </div>
                    <? else : ?>

                        <div class="row border-basket-title">
                            <div class="white-background__column--flex-title col-xl-2">

                            </div>
                            <div class="white-background__column--flex-title col-xl-3">
                                <div class="vertical-column-1">
                                    <p><?= Yii::t('main', 'Наименование') ?></p>
                                </div>
                            </div>
                            <div class="white-background__column--flex-title col-xl-1">
                                <div class="vertical-column-1">
                                    <p><?= Yii::t('main', 'Размер') ?></p>
                                </div>
                            </div>
                            <div class="white-background__column--flex-title col-xl-2">
                                <div class="vertical-column-1">
                                    <p><?= Yii::t('main', 'Цена') ?></p>
                                </div>
                            </div>
                            <div class="white-background__column--flex-title col-xl-2">
                                <div class="vertical-column-1">
                                    <p><?= Yii::t('main', 'Количество') ?></p>
                                </div>
                            </div>

                        </div>
                        <? foreach ($_SESSION['basket-product-own'] as $v) : ?>
                            <div class="row">
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="vertical-column-1">
                                        <div class="img-container">
                                            <img src="<?= $v->getImage(); ?>" alt="cart-img">
                                        </div>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-3">
                                    <div class="vertical-column-2 custom-class-text-center">
                                        <p><?= $v->product->lastCatalog($v->product->category); ?></p>
                                        <p><?= $v->product->$name; ?></p>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-1">
                                    <div class="vertical-column-2 custom-class-text-center">
                                        <p><?= $v->product->size; ?></p>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="vertical-column-3 verical-column-money">
                                        <p><b>KZT <?= $v->product->price; ?></b></p><br>
                                        <?= $v->product->getConversionBasket($v->product->price) ?>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="basket-quantity">
                                        <div class="vertical-column-3">
                                            <span class="minus-product-own" data-id="<?= $v->id ?>"> - </span>
                                            <input type="number" style="width: 80px;" class="quantity-own" data-id="<?= $v->id ?>"  id="countProduct<?= $v->id ?>" value="<?= $v->count; ?>">
                                            <span class="plus-product-own" data-id="<?= $v->id ?>"> + </span>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="vertical-column-4">
                                        <div class="btn-delete-product-own-from-basket" data-id="<?= $v->id ?>" data-message-success-1="<?= Yii::t('main', 'Вы уверены?') ?>" data-message-success-2="<?= Yii::t('main', 'что хотите удалить этот товар!') ?>" data-message-success-3="<?= Yii::t('main', 'Отмена') ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="svg-bin" height="20px" viewBox="-40 0 427 427.00131" width="20px">
                                                <path d="m232.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                                <path d="m114.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                                <path d="m28.398438 127.121094v246.378906c0 14.5625 5.339843 28.238281 14.667968 38.050781 9.285156 9.839844 22.207032 15.425781 35.730469 15.449219h189.203125c13.527344-.023438 26.449219-5.609375 35.730469-15.449219 9.328125-9.8125 14.667969-23.488281 14.667969-38.050781v-246.378906c18.542968-4.921875 30.558593-22.835938 28.078124-41.863282-2.484374-19.023437-18.691406-33.253906-37.878906-33.257812h-51.199218v-12.5c.058593-10.511719-4.097657-20.605469-11.539063-28.03125-7.441406-7.421875-17.550781-11.5546875-28.0625-11.46875h-88.796875c-10.511719-.0859375-20.621094 4.046875-28.0625 11.46875-7.441406 7.425781-11.597656 17.519531-11.539062 28.03125v12.5h-51.199219c-19.1875.003906-35.394531 14.234375-37.878907 33.257812-2.480468 19.027344 9.535157 36.941407 28.078126 41.863282zm239.601562 279.878906h-189.203125c-17.097656 0-30.398437-14.6875-30.398437-33.5v-245.5h250v245.5c0 18.8125-13.300782 33.5-30.398438 33.5zm-158.601562-367.5c-.066407-5.207031 1.980468-10.21875 5.675781-13.894531 3.691406-3.675781 8.714843-5.695313 13.925781-5.605469h88.796875c5.210937-.089844 10.234375 1.929688 13.925781 5.605469 3.695313 3.671875 5.742188 8.6875 5.675782 13.894531v12.5h-128zm-71.199219 32.5h270.398437c9.941406 0 18 8.058594 18 18s-8.058594 18-18 18h-270.398437c-9.941407 0-18-8.058594-18-18s8.058593-18 18-18zm0 0" />
                                                <path d="m173.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                        <? foreach ($_SESSION['basket'] as $v) : ?>
                            <div class="row">
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="vertical-column-1">
                                        <div class="img-container">
                                            <img src="<?= $v->getImage(); ?>" alt="cart-img">
                                        </div>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-3">
                                    <div class="vertical-column-2 custom-class-text-center">
                                        <p><?= $v->lastCatalog($v->category); ?></p>
                                        <p><?= $v->$name; ?></p>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-1">
                                    <div class="vertical-column-2 custom-class-text-center">
                                        <p><?= $v->size; ?></p>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="vertical-column-3 verical-column-money">
                                        <p><b>KZT <?= $v->price; ?></b></p><br>
                                        <?= $v->getConversionBasket($v->price) ?>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="basket-quantity">
                                        <div class="vertical-column-3">
                                            <span class="minus-product" data-id="<?= $v->id ?>"> - </span>
                                            <input type="number" style="width: 80px;" class="quantity" data-id="<?= $v->id ?>"  id="countProduct<?= $v->id ?>" value="<?= $v->count; ?>">
                                            <span class="plus-product" data-id="<?= $v->id ?>"> + </span>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <div class="white-background__column--flex col-xl-2">
                                    <div class="vertical-column-4">
                                        <div class="btn-delete-product-from-basket" data-id="<?= $v->id ?>" data-message-success-1="<?= Yii::t('main', 'Вы уверены?') ?>" data-message-success-2="<?= Yii::t('main', 'что хотите удалить этот товар!') ?>" data-message-success-3="<?= Yii::t('main', 'Отмена') ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="svg-bin" height="20px" viewBox="-40 0 427 427.00131" width="20px">
                                                <path d="m232.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                                <path d="m114.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                                <path d="m28.398438 127.121094v246.378906c0 14.5625 5.339843 28.238281 14.667968 38.050781 9.285156 9.839844 22.207032 15.425781 35.730469 15.449219h189.203125c13.527344-.023438 26.449219-5.609375 35.730469-15.449219 9.328125-9.8125 14.667969-23.488281 14.667969-38.050781v-246.378906c18.542968-4.921875 30.558593-22.835938 28.078124-41.863282-2.484374-19.023437-18.691406-33.253906-37.878906-33.257812h-51.199218v-12.5c.058593-10.511719-4.097657-20.605469-11.539063-28.03125-7.441406-7.421875-17.550781-11.5546875-28.0625-11.46875h-88.796875c-10.511719-.0859375-20.621094 4.046875-28.0625 11.46875-7.441406 7.425781-11.597656 17.519531-11.539062 28.03125v12.5h-51.199219c-19.1875.003906-35.394531 14.234375-37.878907 33.257812-2.480468 19.027344 9.535157 36.941407 28.078126 41.863282zm239.601562 279.878906h-189.203125c-17.097656 0-30.398437-14.6875-30.398437-33.5v-245.5h250v245.5c0 18.8125-13.300782 33.5-30.398438 33.5zm-158.601562-367.5c-.066407-5.207031 1.980468-10.21875 5.675781-13.894531 3.691406-3.675781 8.714843-5.695313 13.925781-5.605469h88.796875c5.210937-.089844 10.234375 1.929688 13.925781 5.605469 3.695313 3.671875 5.742188 8.6875 5.675782 13.894531v12.5h-128zm-71.199219 32.5h270.398437c9.941406 0 18 8.058594 18 18s-8.058594 18-18 18h-270.398437c-9.941407 0-18-8.058594-18-18s8.058593-18 18-18zm0 0" />
                                                <path d="m173.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                        <div class="row">
                            <div class="white-background__column--flex-endTitle">
                                <div class="vertical-column-1">
                                    <p class="total">
                                    <div class="btn-delete-all-from-basket" data-message-success-1="<?= Yii::t('main', 'Вы уверены?') ?>" data-message-success-2="<?= Yii::t('main', 'что хотите очистить корзину!') ?>" data-message-success-3="<?= Yii::t('main', 'Отмена') ?>">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="svg-bin" height="20px" viewBox="-40 0 427 427.00131" width="20px">
                                            <path d="m232.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                            <path d="m114.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                            <path d="m28.398438 127.121094v246.378906c0 14.5625 5.339843 28.238281 14.667968 38.050781 9.285156 9.839844 22.207032 15.425781 35.730469 15.449219h189.203125c13.527344-.023438 26.449219-5.609375 35.730469-15.449219 9.328125-9.8125 14.667969-23.488281 14.667969-38.050781v-246.378906c18.542968-4.921875 30.558593-22.835938 28.078124-41.863282-2.484374-19.023437-18.691406-33.253906-37.878906-33.257812h-51.199218v-12.5c.058593-10.511719-4.097657-20.605469-11.539063-28.03125-7.441406-7.421875-17.550781-11.5546875-28.0625-11.46875h-88.796875c-10.511719-.0859375-20.621094 4.046875-28.0625 11.46875-7.441406 7.425781-11.597656 17.519531-11.539062 28.03125v12.5h-51.199219c-19.1875.003906-35.394531 14.234375-37.878907 33.257812-2.480468 19.027344 9.535157 36.941407 28.078126 41.863282zm239.601562 279.878906h-189.203125c-17.097656 0-30.398437-14.6875-30.398437-33.5v-245.5h250v245.5c0 18.8125-13.300782 33.5-30.398438 33.5zm-158.601562-367.5c-.066407-5.207031 1.980468-10.21875 5.675781-13.894531 3.691406-3.675781 8.714843-5.695313 13.925781-5.605469h88.796875c5.210937-.089844 10.234375 1.929688 13.925781 5.605469 3.695313 3.671875 5.742188 8.6875 5.675782 13.894531v12.5h-128zm-71.199219 32.5h270.398437c9.941406 0 18 8.058594 18 18s-8.058594 18-18 18h-270.398437c-9.941407 0-18-8.058594-18-18s8.058593-18 18-18zm0 0" />
                                            <path d="m173.398438 154.703125c-5.523438 0-10 4.476563-10 10v189c0 5.519531 4.476562 10 10 10 5.523437 0 10-4.480469 10-10v-189c0-5.523437-4.476563-10-10-10zm0 0" />
                                        </svg>
                                        <span>
                                                <?= Yii::t('main', 'Очистить корзину') ?>
                                            </span>
                                    </div>
                                    </p>
                                </div>
                            </div>
                            <div class="white-background__column--flex-endTitle">
                                <div class="vertical-column-1">

                                </div>
                            </div>
                            <div class="white-background__column--flex-endTitle">
                                <div class="vertical-column-2">

                                </div>
                            </div>
                            <div class="white-background__column--flex-endTitle">
                                <div class="vertical-column-3">

                                </div>
                            </div>
                            <div class="white-background__column--flex-endTitle">
                                <div class="vertical-column-4">
                                    <!--                                            <p class="total" id="sumBasket" data-price = '--><? //=$sum
                                    ?>
                                    <!--'>Всего к оплате: <span>--><? //=$sum
                                    ?>
                                    <!-- KZT</span></p>-->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="submit-btn">
                                    <!--                                            <a href="#" class="submit-btn__btn order-basket" data-sum="--><? //=$sum
                                    ?>
                                    <!--">-->
                                    <!--                                                Оформить заказ-->
                                    <!--                                            </a>-->
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
        <? if($_SESSION['basket'] != null || $_SESSION['basket-product-own'] != null):?>
        <div id="order-basket" class="basket-contacts-and-paying">
            <form action="/card/order-pay">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                <? $isGuest = Yii::$app->user->isGuest; ?>
                <div class="left-side">
                    <span><?= Yii::t('main', 'ФИО') ?>*</span>
                    <input type="text" placeholder="<?= Yii::t('main', 'Фамилия, имя, отчество') ?>" name="Orders[fullname]" value="<?= $isGuest ? "" : $profile->fio; ?>">
                    <div class="email-tel">
                        <div>
                            <span><?= Yii::t('main', 'Эл. почта') ?>*</span>
                            <input type="email" placeholder="yourmail@mail.ru" name="Orders[email]" value="<?= $isGuest ? "" : $user->email; ?>">
                        </div>
                        <div>
                            <span><?= Yii::t('main', 'Мобильный телефон') ?>*</span>
                            <input class="phone_us" type="text" placeholder="8 (___) ___-__-__" name="Orders[phone]" value="<?= $isGuest ? "" : substr($profile->phone, 1); ?>">
                            <script>
                                $('input[name="Orders[phone]"]').inputmask("8(999) 999-9999");
                            </script>
                        </div>
                    </div>
                    <span><?= Yii::t('main', 'Адрес доставки') ?></span>

                    <input type="text" placeholder="<?= Yii::t('main', 'Например: г.Алматы, ул. Московская') ?>" name="Orders[address]" value="<?= $isGuest ? "" : $address->address; ?>">
                    <div class="comment">
                        <span><?= Yii::t('main', 'Коментарии') ?></span>
                        <textarea id="comment" placeholder="<?= Yii::t('main', 'Сообщение') ?>" name="Orders[comment]"></textarea>
                    </div>
                </div>
                <div class="right-side">
                    <div class="pay-price">
                        <div class="total">
                            <p><?= Yii::t('main', 'Всего к оплате') ?>:</p>
                            <p id="sumBasket">KZT <?= $sum ?></p>
                        </div>
                        <div class="total-payment">
                            <div class="next-del">
                                <a href="/catalog"><?= Yii::t('main', 'Продолжить покупки') ?></a>
                                <a href="<?= \yii\helpers\Url::toRoute(['/card/delete-all']) ?>">
                                    <?= Yii::t('main', 'Очистить корзину') ?></a>
                            </div>
                            <input type="hidden" id="sumPay" value="<?= $sum ?>">
                            <input type="hidden" id="min-price" value="<?= $minPrice->price ?>">
                            <a style="cursor: pointer" class="btn-pay" id="order-pay" data-sum="<?= $sum ?>" data-user="<?= !Yii::$app->user->isGuest; ?>" data-message-success-1="<?= Yii::t('main', 'Обратите внимание!') ?>" data-message-success-2="<?= Yii::t('main', 'Оформление заказа свыше суммы 5000 тг или позвоните на поддержку!') ?>" data-message-error-1="<?= Yii::t('main', 'Ошибка!') ?>" data-message-error-2="<?= Yii::t('main', 'Что-то пошло не так.') ?>" data-message-error-guest="<?= Yii::t('main', 'Чтобы совершить покупку зарегистрируйтесь пожалуйста!') ?>" data-message-checked-1="<?= Yii::t('main', 'Внимание') ?>" data-message-checked-2="<?= Yii::t('main', 'Чтобы продолжить покупку, нужно принять договор оферты') ?>">
                                <?= Yii::t('main', 'Перейти к оплате') ?>
                            </a>

                        </div>
                        <div class="col-md-12 ">
                            <br><br><br>
                            <div class="d-flex">

                                <input id="agree" name="check" type="checkbox" required class="w-auto mr-2">
                                <label for="agree" class="w-auto">
                                    <? $text = '<a href="' . $offer->getFile() . '" target="_blank">' . $offer->getName() . '</a>' ?>
                                    <?= Yii::t('main', 'Я принимаю условия Публичного Договора ({text})', [
                                        'text' => $text,
                                    ]) ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <? endif;?>
    </div>

    <div class="modal fade paymentModal" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-login modal-personal">
                <div class="modal-header">
                    <h5 class="modal-title" id="paymentModalLabel"><?= Yii::t('main', 'Выберите способ оплаты') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="payment-text">
                        <p class="order_id online-payment-order-id" style="display: none;"></p>
                    </div>
                    <div class="payment-choice">
                        <button type="button" class="online-btn" data-message-error-1="<?= Yii::t('main', 'Ошибка!') ?>" data-message-error-2="<?= Yii::t('main', 'Что-то пошло не так.') ?>">
                            <?= Yii::t('main', 'Онлайн оплата'); ?>
                        </button>
                    </div>
                    <div class="payment-choice">
                        <button class="transaction-btn" type="button" data-message-error-1="<?= Yii::t('main', 'Ошибка!') ?>" data-message-error-2="<?= Yii::t('main', 'Что-то пошло не так.') ?>">
                            <?= Yii::t('main', 'Перевод на банковский счет') ?></button>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- Modal toggle -->
                    <button type="button" id="paymentModalBtn" class="btn btn-primary d-none" data-toggle="modal" data-target="#paymentModal">
                        Launch demo modal
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="transactionPayment" tabindex="-1" role="dialog" aria-labelledby="transactionPaymentLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content modal-login modal-personal">
                <div class="modal-header">
                    <h5 class="modal-title" id="transactionPaymentLabel"><?= Yii::t('main', 'Информация о заказе') ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="payment-choice transaction-payment">
                                <p class="order_id"></p>
                                <p class="order_date"></p>
                                <p class="order_sum"></p>
                                <p class="order_fullname"></p><br>
                                <hr>

                                <div class="products_transaction">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="payment-choice transaction-payment-en">
                                <p class="order_id_en"></p>
                                <p class="order_date_en"></p>
                                <p class="order_sum_en"></p>
                                <p class="order_fullname_en"></p><br>
                                <hr>

                                <div class="products_transaction_en">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <h6 class="modal-title">
                        <a href="/card/pdf" id="props" onClick="window.location.reload()" target="_blank"><?= Yii::t('main', 'Счет на оплату') ?></a>
                    </h6>
                    <!-- Modal toggle -->
                    <button type="button" id="transactionPaymentBtn" class="btn btn-primary d-none" data-toggle="modal" data-target="#transactionPayment">
                        Open Modal Transaction
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
