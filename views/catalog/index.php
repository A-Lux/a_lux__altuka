<main>
    <div class="container catalog wow bounce" id="catalog_result">
        <div class="col-sm-12 col-lg-12">
            <? $text = "text" . Yii::$app->session["lang"]; ?>
            <? $name = "name" . Yii::$app->session["lang"]; ?>
            <div class="link"><span><?= Yii::$app->view->params['main']; ?>/</span><span><?= $model->$text; ?></span>
            </div>
            <div class="title title-items"><h1><?= $model->$text; ?></h1></div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-lg-3 col-sm-3 right-filter">
                        <ul class="sidebar__tabs">
                            <?= $tree; ?>
                        </ul>

                    </div>
                    <div class="col-sm-9 col-md-9 col-sm-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 px-0">
                                <div class="filter-header">
                                    <input class="category_id" type="hidden" value="">
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="filter-header__select">
                                            <select name="filter" id="popular" class="filter">
                                                <option value="isNew"
                                                        class="isNew"><?= Yii::t('main', 'Сначала новые') ?></option>
                                                <option value="available"
                                                        class="available"><?= Yii::t('main', 'Есть в наличии') ?></option>
                                                <option value="isDiscount"
                                                        class="isDiscount"><?= Yii::t('main', 'Акционные товары') ?></option>
                                                <option value="isHit"
                                                        class="isHit"><?= Yii::t('main', 'Популярные') ?></option>
                                                <option value="top_month"
                                                        class="top_month"><?= Yii::t('main', 'Товар месяца') ?></option>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12">
                                        <div class="filter-header__select">
                                            <div class="filter-select__center">
                                                <a href="#price-filter"
                                                   class="price-filter-btn"><?= Yii::t('main', 'По цене') ?></a>
                                                <div style="display: none;" class="filter-select__center-input_wrapper"
                                                     id="price-filter">
                                                    <div class="filter-select__center-input">
                                                        <span><?= Yii::t('main', 'От') ?></span>
                                                        <input type="number" class="from_price">
                                                        <span>тг</span>
                                                    </div>
                                                    <div class="filter-select__center-input">
                                                        <span><?= Yii::t('main', 'До') ?></span>
                                                        <input type="number" class="to_price">
                                                        <span>тг</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
<!--                            <div class="col-lg-12">-->
<!--                                <div class="filter-txt">-->
<!--                                    <p>--><?//= Yii::t('main', 'Пожалуйста, выберите размер магнитика') ?><!--</p>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>

                        <div class="row mt-5">
                            <div class="col-lg-12">
                                <div class="tablet-version-hide row items">
                                    <? foreach ($catalog as $value): ?>
                                        <? $products = $value->allProducts; ?>
                                        <? foreach ($products as $v): ?>
                                            <div class="col-12 col-sm-6 col-lg-4 pb-4">
                                                <div class="card">
                                                    <div class="card-img">
                                                        <a href="<?= \yii\helpers\Url::to(['/catalog/product', 'url' => $v->url]) ?>">
                                                            <img src="<?= $v->getImage(); ?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="card-content">
                                                        <p class="card-title"><?= $v->$name; ?></p>
                                                        <p class="card-title"><?= $v->size; ?></p>
                                                        <p class="card-price">KZT <?= $v->price ?></p>
                                                    </div>
                                                    <div class="buttons">
                                                        <? if ($v->catalog->name !== Yii::$app->view->params['yourMagnet']): ?>
                                                            <? if ($v->status !== 0): ?>
                                                                <button class="btn btn-to-basket" type="button"
                                                                        name="button"
                                                                        data-id="<?= $v->id; ?>"
                                                                        data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!') ?>"
                                                                        data-message-success-2="<?= Yii::t('main', 'Продолжить покупки') ?>"
                                                                        data-message-success-3="<?= Yii::t('main', 'Оформить заказ') ?>"
                                                                        data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!') ?>"
                                                                        data-message-error-2="<?= Yii::t('main', 'Упс!') ?>"
                                                                        data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.') ?>">
                                                                    <?= Yii::t('main', 'В корзину') ?>
                                                                </button>
                                                            <? else: ?>
                                                                <button class="btn btn-not-basket" type="button"
                                                                        name="button">
                                                                    <?= Yii::t('main', 'Товара нет в наличии') ?>
                                                                </button>
                                                            <? endif; ?>
                                                        <? endif; ?>
                                                    </div>

                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        window.onload = function () {

            var link = localStorage.getItem('anchor');

            a = link.split('#');

            b = a[1].split('-');
            var temp = '';
            for (var i = 0; i < b.length; i++) {
                $('[href="#' + temp + '"').trigger('click');
                temp = temp == '' ? b[i] : temp + '-' + b[i];
            }

            var id = b.slice(-1)[0];

            $.ajax({
                type: 'GET',
                url: "/catalog/category",
                data: {id: id},
                success: function (html) {
                    $('.category_id').val(id);
                    $('.tablet-version-hide.items').html(html);

                }
            });

        };

    </script>

</main>

