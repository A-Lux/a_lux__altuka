<?  $name = "name".Yii::$app->session["lang"];?>
<?foreach($products as $v):?>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-img">
                <a href="<?=\yii\helpers\Url::to(['/catalog/product', 'url' => $v->url]) ?>">
                    <img src="<?=$v->getImage();?>" alt="">
                </a>
            </div>
            <div class="card-content">
                <p class="card-title"><?=$v->$name;?></p>
                <p class="card-title"><?= $v->size; ?></p>
                <p class="card-price">KZT <?=$v->price?></p>
            </div>
            <div class="buttons">
                <? if($v->catalog->name !== Yii::$app->view->params['yourMagnet']): ?>
                    <?if($v->status !== 0):?>
                        <button class="btn btn-to-basket" type="button" name="button" data-id="<?=$v->id;?>"
                                data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!')?>"
                                data-message-success-2="<?= Yii::t('main', 'Продолжить покупки')?>"
                                data-message-success-3="<?= Yii::t('main', 'Оформить заказ')?>"
                                data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!')?>"
                                data-message-error-2="<?= Yii::t('main', 'Упс!')?>"
                                data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.')?>">
                            <?= Yii::t('main', 'В корзину')?></button>
                    <?else:?>
                        <button class="btn btn-not-basket" type="button" name="button">
                            <?= Yii::t('main', 'Товара нет в наличии')?></button>
                    <?endif;?>
                <? endif; ?>
            </div>

        </div>
    </div>
<? endforeach;?>