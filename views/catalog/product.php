<?php

?>

<section class="product-card wow bounce">
    <div class="container">
        <!-- breadcrumbs -->
        <? $text = "text" . Yii::$app->session["lang"]; ?>
        <? $name = "name" . Yii::$app->session["lang"]; ?>
        <? $content = "content" . Yii::$app->session["lang"]; ?>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-5">
                <ul class="breadcrumbs">
                    <li>
                        <a href="/"><?= Yii::$app->view->params['main']; ?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href="/catalog"><?= $product->category->$name; ?></a>
                    </li>
                    <li>
                        <a href="">/</a>
                    </li>
                    <li>
                        <a href=""><?= $product->$name; ?></a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-7">
                <div class="product-comment">
                    <? if(!Yii::$app->user->isGuest && $product->catalog->name == \Yii::$app->view->params['yourMagnet']) :?>
                    <h1><?= Yii::t('main', 'Пожалуйста, загрузите собственную картинку') ?></h1>
                    <? endif;?>
                </div>
            </div>
        </div>
        <!-- end breadcrumbs -->
        <div class="product-card__info">
            <div class="row">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 px-0">
                        <div class="product-card__main-img">
                            <img src="<?= $product->getImage(); ?>">
                        </div>
                    </div>

                    <? if(!Yii::$app->user->isGuest && $product->catalog->name == \Yii::$app->view->params['yourMagnet']) :?>
                        <div class="col-lg-12 col-md-12 col-sm-12 px-0">
                            <div class="product-card__small-img">
                                <?= $this->render('/blocks/_form_your_magnet', [
                                        'product_id'    => $product->id,
                                ]); ?>
                            </div>
                        </div>
                    <? endif;?>

                </div>
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="product-card__info-main">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <p class="product-card__info-main_title"><?= $product->$name; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <ul class="product-card__info-main_link1">
                                    <li>
                                        <p><?= Yii::t('main', 'Размер') ?>: </p>
                                    </li>
                                    <li></li>
                                    <li>
                                        <p><?= $product->size; ?></p>
                                    </li>
                                </ul>
                                <ul class="product-card__info-main_link2">
                                    <li>
                                        <p><?= Yii::t('main', 'Код товара') ?>: </p>
                                    </li>
                                    <li></li>
                                    <li>
                                        <p><?= $product->articul ?></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-text">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="product-text__main">
                                        <p>
                                            <?= $product->$content; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="product-card__info-price">
                                    <ul class="product-card__info-price_main">
                                        <li>
                                            <p><?= Yii::t('main', 'Цена') ?>: </p>
                                        </li>
                                        <li></li>
                                        <li>
                                            <p>KZT <?= $product->price; ?></p>
                                        </li>
                                    </ul>
                                    <div class="product-card__our-price_wrapper">
                                        <p class="product-card__our-price">
                                            <span>
                                                <?= $product->getConversion($product->price) ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="product-card__info-buy">
                                    <div class="col-lg-6 col-md-6 col-sm-12 px-0">
                                        <div class="product-card__info-buy-wrapper">
                                            <? if($product->catalog->name !== Yii::$app->view->params['yourMagnet']): ?>
                                                <div class="col-lg-6 p-0">
                                                    <ul>
                                                        <li class="minus">
                                                            <span>-</span>
                                                        </li>
                                                        <li>
                                                            <input type="number" class="quantity_box-input count<?= $product->id; ?>" value="1">
                                                        </li>
                                                        <li class="plus">
                                                            <span>+</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-6 px-0">
                                                    <input type="submit" class="btn <?= $product->status == 1 ? 'btn-to-basket' : 'btn-not-basket' ?>"
                                                           data-id="<?= $product->id; ?>" data-message-success-1="<?= Yii::t('main', 'Товар добавлен в корзину!') ?>"
                                                           data-message-success-2="<?= Yii::t('main', 'Продолжить покупки') ?>"
                                                           data-message-success-3="<?= Yii::t('main', 'Оформить заказ') ?>"
                                                           data-message-success-4="<?= Yii::t('main', 'Товар уже добавлен в корзину!') ?>"
                                                           data-message-error-2="<?= Yii::t('main', 'Упс!') ?>"
                                                           data-message-error-3="<?= Yii::t('main', 'Что-то пошло не так.') ?>"
                                                           value="<?= Yii::t('main', 'В корзину') ?>">
                                                </div>
                                            <? endif;?>
                                            <br>
                                        </div>
                                        <!-- <input type="text" placeholder="Количество" class="quantity"> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="product-card__info-footer_text">
                                    <p><?= Yii::t('main', 'Доставка в пределах города Алматы бесплатно, за пределами по ценам курьера') ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="product-recom mt-5">
            <div class="row">
                <?= $this->render('../partials/recommend', compact('recommendProduct', 'mainSub')) ?>
            </div>
        </div>
    </div>
</section>